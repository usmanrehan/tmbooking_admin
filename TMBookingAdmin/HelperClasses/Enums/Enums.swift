//
//  Enums.swift
//  AdminTMBooking
//
//  Created by Akber Sayni on 30/07/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

enum AppStoryboard : String {
    
    //Add all the storyboard names you wanted to use in your project
    case Main
	case Booking
    
    var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type, function : String = #function, line : Int = #line, file : String = #file) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardId
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard.\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")
        }
        
        return scene
    }
    
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
    
    
}
