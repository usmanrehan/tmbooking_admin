//
//  UIImageView+Extension.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 01/03/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    func loadImage(withURL url: String?) {
        guard let url = url, !url.isEmpty else { return }
        self.sd_setImage(with: URL(string: url), completed: nil)
    }
	
	func loadImage(withURL url: String?, placeholderImage: UIImage?) {
		guard let url = url, !url.isEmpty else {
			return
		}
		
		self.sd_setImage(with: URL(string: url), placeholderImage:placeholderImage,  completed: nil)
	}
    
    public func loadImage(_ path: String?, loadingView: UIActivityIndicatorView? = nil, callback: @escaping (UIImage?) -> Void) {
        guard path != nil else { return }
        if let url = URL(string: path!) {
            startActivityIndicatorView(loadingView)
            self.sd_setImage(with: url) { (image, error, cacheType, url) in
                self.stopActivityIndicatorView(loadingView)
                callback(image)
            }
        }
    }
    
    private func startActivityIndicatorView(_ activityIndicatorView: UIActivityIndicatorView?) {
        guard activityIndicatorView != nil else {
            return
        }
        
        activityIndicatorView!.startAnimating()
    }
    
    private func stopActivityIndicatorView(_ activityIndicatorView: UIActivityIndicatorView?) {
        guard activityIndicatorView != nil else {
            return
        }
        
        activityIndicatorView!.stopAnimating()
    }

}
