//
//  Data+Extension.swift
//  PNL
//
//  Created by Syed Arsalan Shah on 3/6/18.
//  Copyright © 2018 Muzamil Hassan. All rights reserved.
//

import Foundation

extension Data
{
    func toString() -> String {
        return String(data: self, encoding: .utf8)!
    }
    
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    func toDictionary() -> [String: Any] {
        return (try? JSONSerialization.jsonObject(with: self)) as? [String: Any] ?? [:]
    }
}
