//
//  String+Extension.swift
//  Template
//
//  Created by Akber Sayni on 15/04/2018.
//  Copyright © 2018 Muzamil Hassan. All rights reserved.
//

import UIKit

extension String {
    
    func isValidEmail() -> Bool {
        let emalRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let result = NSPredicate(format:"SELF MATCHES %@", emalRegex)
        
        return result.evaluate(with: self)
    }
    
    func isValidString() -> Bool {
        let trimmedString = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        return !trimmedString.isEmpty
    }
    
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }

    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
        
    func toDouble() -> Double {
        return (NumberFormatter().number(from: self)?.doubleValue) ?? 0.0
    }
    
    func toFloat() -> Float {
        return (NumberFormatter().number(from: self)?.floatValue) ?? 0.0
    }
    
    func toInt() -> Int {
        return (NumberFormatter().number(from: self)?.intValue) ?? 0
    }
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss")-> Date? {        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)
    }
}


extension StringProtocol {
    var firstUppercased: String {
        guard let first = first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
}
