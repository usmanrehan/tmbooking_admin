//
//  DefaultsKeys+Extension.swift
//  PNL
//
//  Created by Syed Arsalan Shah on 2/14/18.
//  Copyright © 2018 Muzamil Hassan. All rights reserved.
//

import Foundation
import SwiftyUserDefaults
 
extension DefaultsKeys {
    static let loginData = DefaultsKey<Data?>("loginData")
    static let loginStatus = DefaultsKey<Bool>("loginStatus")
    static let loginUserId = DefaultsKey<String>("loginUserId")
    static let token = DefaultsKey<String>("tokenGenerated")
    static let deviceToken = DefaultsKey<String>("deviceTokenAPNS")
}
