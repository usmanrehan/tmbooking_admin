//
//  Constants.swift
//  AdminTMBooking
//
//  Created by Akber Sayni on 30/07/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

struct Global {
    static let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
    static let APP_COLOR = UIColor(red:0.82, green:0.11, blue:0.00, alpha:1.0)
}

struct Constants {
    static let APP_DELEGATE                 = UIApplication.shared.delegate as! AppDelegate
    static let UIWINDOW                     = UIApplication.shared.delegate!.window!
    
    static let USER_DEFAULTS               = UserDefaults.standard
    
    static let DEFAULTS_USER_KEY            = "User"
    
    static var DeviceToken                  = "123456789"

    static let BaseURL                      = "https://tmbooking.com/"
    
    static let WpBaseURL                    = "https://tmbooking.com/wp-json/"
    
    static let serverDateFormat             = "yyyy-MM-dd HH:mm:ss"
    
    static let PAGINATION_PAGE_SIZE         = 100
    
    static let CUSTOMER_KEY                 = "ck_cd06ea655997307389b1d6c38b66cd8d008d44d5"
    
    static let CUSTOMER_SECERT              = "cs_7e868bd4f5a7e3041141acde01723508232a4e6d"
    
//    internal static let PASS_PHRASE = "WLW\\^{68J?*9wk{]"
//    internal static let ADMIN_USERNAME = "akber"
//    internal static let ADMIN_PASSWORD = "admin123"

}

extension Notification.Name {
    static let visaUpdateStatus = Notification.Name("VisaUpdateStatus")
}
