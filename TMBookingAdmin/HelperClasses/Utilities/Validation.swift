//
//  Validation.swift
//  Auditix
//
//  Created by Ahmed Shahid on 1/4/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import UIKit
class Validation {
    
    static func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static  func isValidPhone(value: String) -> Bool {
        var result = false
        if value.count >= 12 {
            result = true
        }
        return result
    }
    
    static func isValidePassword(value: String) -> Bool {
        var result = false
        if value.count >= 6 {
            result = true
        }
        return result
    }
    
    static func isConfirmPasswordIsEqualToPassword(password: String, confirm: String) -> Bool {
        if(password == confirm)
        {
            return true
        }
        
        return false
    }
    
    static func validateStringLength(_ text: String) -> Bool {
        let trimmed = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return !trimmed.isEmpty
    }
    
    static func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    static func isDoubleParsable(value: String)-> Bool{
        if value == ""{return false}
        if let _ = Double(value){
            return true
        }
        return false
    }
}
