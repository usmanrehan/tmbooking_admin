//
//  Utility.swift
//  AdminTMBooking
//
//  Created by Akber Sayni on 03/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class Utility: NSObject {

    func topViewController(base: UIViewController? = (Global.APP_DELEGATE).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }

    static func showAlert(title:String?, message:String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
        Utility().topViewController()!.present(alert, animated: true){}
    }
    
    static func showAlert(title:String?, message:String?, closure: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { onClick in
            closure(onClick)
        })
        Utility().topViewController()!.present(alert, animated: true){}
    }
    
    static func showAlertWithYesNo(title:String?, message:String?, closure: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "No", style: .default) { onClick in
            alert.dismiss(animated: true, completion: {
                //ignore
            })
        })
        alert.addAction(UIAlertAction(title: "Yes", style: .default) { onClick in
            closure(onClick)
        })
        
        Utility().topViewController()!.present(alert, animated: true){}
    }

}
// MARK:- INDICATOR
extension Utility{
    
    static func showLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let size = CGSize(width: 50, height: 50)
        let bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let activityData = ActivityData(size: size, message: "", messageFont: UIFont.systemFont(ofSize: 12), type: .ballClipRotate, color: Global.APP_COLOR, padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 1, backgroundColor: bgColor, textColor: UIColor.black)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
    }
    
    static func hideLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
}
// MARK:- Helper Methods
extension Utility{
    static func stringToUrl(string: String)-> URL{
        if Validation.verifyUrl(urlString: string){
            return URL(string: string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!
        }
        else{
            let placeHolder = "http://tmbooking.com/tmbooking/public/img/placeholder.png"
            return URL(string: placeHolder.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)!
        }
    }
    
    static func stringDateFormatter(dateStr: String , dateFormat : String , formatteddate : String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = dateFormat
        let date = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = formatteddate
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: date ?? Date())
    }
    
    static func returnVisaApplicationStatusStringFromCode(code: String)-> String{
        switch code {
        case "0":
            return "Pending"
        case "1":
            return "Approved"
        default:
            return "Rejected"
        }
    }
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
