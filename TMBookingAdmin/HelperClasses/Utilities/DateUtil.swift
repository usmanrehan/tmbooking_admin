//
//  DateUtil.swift
//  PNL
//
//  Created by Syed Arsalan Shah on 2/21/18.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import Foundation

class DateUtil {
    static let FORMAT_WOOCOMMERCE = "yyyy-MM-dd'T'HH:mm:ss"
    static let DATE_FORMAT = "yyyy-MM-dd"
    static let TIME_FORMAT = "HH:mm:ss"
    static let DATE_TIME_FORMAT = "\(DATE_FORMAT) \(TIME_FORMAT)"
    static let DATA_FORMAT_NOTIFICATIONS = "HH:mm a"
    
    /// make string date object to date object
    ///
    /// - Parameter date: String
    /// - Returns: Date
    static func stringToDate(with date: String?, format: String) -> Date? {
        if date != nil && !(date?.isEmpty)! {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = format
            dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
            if let date = dateFormatter.date(from: date!){
                return date
            }
            return nil
        }
        return nil
    }
    
    /// get elapsed interval with date
    ///
    /// - Parameter date: Date
    /// - Returns: String
    static func getElapsedInterval(date: Date?) -> String {
        guard let date = date else {
            return ""
        }

        var isJustNow = false
        var calendar = Calendar.current
        calendar.locale = Locale(identifier: Bundle.main.preferredLocalizations[0]) //--> IF THE USER HAVE THE PHONE IN SPANISH BUT YOUR APP ONLY SUPPORTS I.E. ENGLISH AND GERMAN WE SHOULD CHANGE THE LOCALE OF THE FORMATTER TO THE PREFERRED ONE (IS THE LOCALE THAT THE USER IS SEEING THE APP), IF NOT, THIS ELAPSED TIME IS GOING TO APPEAR IN SPANISH
        
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.maximumUnitCount = 1
        formatter.calendar = calendar
        
        var dateString: String?
        
        let interval = calendar.dateComponents([.year, .month, .weekOfYear, .day,
                                                .hour, .minute, .second], from: date, to: Date())
        
        if let year = interval.year, year > 0 {
            formatter.allowedUnits = [.year] //2 years
        } else if let month = interval.month, month > 0 {
            formatter.allowedUnits = [.month] //1 month
        } else if let week = interval.weekOfYear, week > 0 {
            formatter.allowedUnits = [.weekOfMonth] //3 weeks
        } else if let day = interval.day, day > 0 {
            formatter.allowedUnits = [.day] // 6 days
        }else if let hour = interval.hour, hour > 0 {
            formatter.allowedUnits = [.hour] // 2 hours
        } else if let minute = interval.minute, minute > 0 {
            formatter.allowedUnits = [.minute] // 6 minutes
        } else if let second = interval.second, second > 3 {
            formatter.allowedUnits = [.second] // 6 seconds
        } else {
            isJustNow = true
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: Bundle.main.preferredLocalizations[0]) //--> IF THE USER HAVE THE PHONE IN SPANISH BUT YOUR APP ONLY SUPPORTS I.E. ENGLISH AND GERMAN WE SHOULD CHANGE THE LOCALE OF THE FORMATTER TO THE PREFERRED ONE (IS THE LOCALE THAT THE USER IS SEEING THE APP), IF NOT, THIS ELAPSED TIME IS GOING TO APPEAR IN SPANISH
//            dateFormatter.dateStyle = .short
            dateFormatter.timeStyle = .short
            dateFormatter.doesRelativeDateFormatting = false //set this to true if required Today as text
            
            dateString = dateFormatter.string(from: date) // IS GOING TO SHOW 'TODAY'
        }
        
        if dateString == nil {
            if isJustNow {
                dateString = "Just now"//formatter.string(from: date, to: Date())
            }else {
                dateString = formatter.string(from: date, to: Date())! + " ago"
            }
        }
        
        return dateString!
    }

    static func timeAgoSinceDate(_ date:Date, numericDates:Bool = false) -> String {
        var calendar = NSCalendar.current
        calendar.locale = Locale(identifier: Bundle.main.preferredLocalizations[0])
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date()
        let earliest = now < date ? now : date
        let latest = (earliest == now) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
//            if (numericDates){
//                return "1 day ago"
//            } else {
                return "Yesterday"
//            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
//            if (numericDates){
                return "1 hour ago"
//            }
//            else {
//                return "An hour ago"
//            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
//            if (numericDates){
                return "1 minute ago"
//            } else {
//                return "A minute ago"
//            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
    
    /// get date from String date in GMT
    ///
    /// - Parameters:
    ///   - stringDate: String
    ///   - returnFormate: String
    /// - Returns: String
    static func getDateFormatGMTWith(stringDate: String?, initialFormate: String, returnFormate: String) -> String{
        if stringDate != nil {
            let dateFormatter = DateFormatter()
//            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = initialFormate
            if let date = dateFormatter.date(from: stringDate!) {
                dateFormatter.dateFormat = returnFormate
                return dateFormatter.string(from: date)
            }
        }
        return ""
    }
    
    /// get date from String date in UTC
    ///
    /// - Parameters:
    ///   - stringDate: String date
    ///   - returnFormate: Formate Required in return
    /// - Returns: String
    static func getDateFormatUTCWith(stringDate: String?, initialFormate: String, returnFormate: String?) -> String {
        if stringDate != nil {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = initialFormate
            if let date = dateFormatter.date(from: stringDate!) {
                dateFormatter.dateFormat = returnFormate
                dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
                return dateFormatter.string(from: date)
            }
        }
        return ""
    }
    
    static func CustomDateFormatter(stringDate: String , initialFormate: String , returnFormate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = initialFormate //Your date format
        //dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        let date = dateFormatter.date(from: stringDate) //according to date format your date string
        print(date ?? "") //Convert String to Date
        //dateFormatter.dateFormat = "dd-MM-yy '|' HH:mm"
        dateFormatter.dateFormat = returnFormate //"MMM dd YYYY"
        return dateFormatter.string(from: date!)//""//"  \(date) | \(time)"
    }
    
    private static func prepareFormat(with defaultDate: String){
    
    }
    
    ///Input: "23/02/2017" Output: 2017-02-23 18:30:00 +0000
    static func GetDateFromString(DateStr: String)-> Date {
        let calendar = NSCalendar(identifier: NSCalendar.Identifier.gregorian)
        let DateArray = DateStr.components(separatedBy: "/")
        let components = NSDateComponents()
        components.year = Int(DateArray[2])!
        components.month = Int(DateArray[1])!
        components.day = Int(DateArray[0])!
        components.timeZone = TimeZone(abbreviation: "GMT+0:00")
        let date = calendar?.date(from: components as DateComponents)
        return date!
    }
}
