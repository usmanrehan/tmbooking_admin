//
//  RoundedCorners.swift
//  Fan Direct
//
//  Created by Usman Bin Rehan on 2/26/18.
//  Copyright © 2018 Usman Bin Rehan. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        let height = self.frame.height
        self.layer.cornerRadius = CGFloat(height / 2.0)
        self.clipsToBounds = true
        
    }
}
class SelectUnSelectBookingButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        if self.isSelected{
            self.backgroundColor = Global.APP_COLOR
            self.setTitleColor(.white, for: .selected)
            self.borderColor = Global.APP_COLOR
            self.borderWidth = 1.0
        }
        else{
            self.backgroundColor = .white
            self.setTitleColor(Global.APP_COLOR, for: .normal)
            self.borderColor = Global.APP_COLOR
            self.borderWidth = 1.0
        }
    }
}
class SelectUnSelectFilterButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        if self.isSelected{
            self.backgroundColor = UIColor(red:0.82, green:0.11, blue:0.00, alpha:0.35)
            self.borderColor = Global.APP_COLOR
            self.borderWidth = 1.0
        }
        else{
            self.backgroundColor = .clear
            self.borderColor = Global.APP_COLOR
            self.borderWidth = 1.0
        }
    }
}
class CurrentStateButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        if self.isSelected{
            self.backgroundColor = Global.APP_COLOR
        }
        else{
            self.backgroundColor = .white
        }
    }
}
class RoundedLabel: UILabel {
    override func layoutSubviews() {
        super.layoutSubviews()
        let height = self.frame.height
        self.layer.cornerRadius = CGFloat(height / 2.0)
        self.clipsToBounds = true
        
    }
}

class RoundedImage : UIImageView{
    override func layoutSubviews() {
        super.layoutSubviews()
        let height = self.frame.height
        self.layer.cornerRadius = CGFloat(height / 2.0)
        self.clipsToBounds = true
    }
}

class RoundedView : UIView{
    override func layoutSubviews() {
        super.layoutSubviews()
        let height = self.frame.height
        self.layer.cornerRadius = CGFloat(height / 2.0)
        self.clipsToBounds = true
    }
}
class RoundedSwitch : UISwitch{
    override func layoutSubviews() {
        super.layoutSubviews()
        let height = self.frame.height
        self.layer.cornerRadius = CGFloat(height / 2.0)
        self.clipsToBounds = true
    }
}
