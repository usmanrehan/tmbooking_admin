//
//  VisaMemberDetail.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 23/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import SDWebImage

enum ImagePickerType{
    case profile
    case passport
}

class VisaMemberDetail: BaseViewController {
    
    var memberDetailModel = MemberDetailModel()
    
    var imagePicker: UIImagePickerController!
    var imagePickerType = ImagePickerType.profile
    
    @IBOutlet weak var tfNationality: UITextField!
    @IBOutlet weak var tfResidency: UITextField!
    @IBOutlet weak var tfFullName: UITextField!
    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var imgPassport: UIImageView!
    @IBOutlet weak var imgProfilePhoto: UIImageView!
    @IBOutlet weak var btnPassportImage: UIButton!
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var btnEditApplication: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnEditApplication(_ sender: UIButton) {
        if !sender.isSelected{
            sender.isSelected = true
            self.enableUserInteraction()
        }
        else{
            self.updateVisaMemberDetail()
        }
    }
    @IBAction func onBtnChangeStatus(_ sender: UIButton) {
        self.navigateToEditVisaStatus()
    }
    @IBAction func onBtnEmailAsCopy(_ sender: UIButton) {
        self.showEmailTextFieldAlertPopup()
    }
    @IBAction func onBtnImagePicker(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            self.imagePickerType = .passport
        default:
            self.imagePickerType = .profile
        }
        self.uploadImage()
    }
}
extension VisaMemberDetail{
    private func setData(){
        let data = self.memberDetailModel
        self.tfNationality.text = data.nationality ?? ""
        self.tfResidency.text = data.livingin ?? ""
        self.tfFullName.text = data.fullname ?? ""
        self.tfMobile.text = data.mobile ?? ""
        self.tfEmail.text = data.email ?? ""
        
        if let profileImageURLString = data.photo{
            if let profileImageURL = URL(string:profileImageURLString){
                self.imgProfilePhoto.sd_setImage(with: profileImageURL, completed: nil)
            }
        }
        if !data.passport.isEmpty{
            if let passportImageURLString = data.passport.first{
                if let passportImageURL = URL(string:passportImageURLString){
                    self.imgPassport.sd_setImage(with: passportImageURL, completed: nil)
                }
            }
        }
    }
    private func navigateToEditVisaStatus() {
        let storyboard = UIStoryboard(name: "VisaSection", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "EditVisaStatus") as? EditVisaStatus{
            controller.memberDetailModel = self.memberDetailModel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    private func enableUserInteraction(){
        self.tfNationality.isUserInteractionEnabled = true
        self.tfResidency.isUserInteractionEnabled = true
        self.tfFullName.isUserInteractionEnabled = true
        self.tfMobile.isUserInteractionEnabled = true
        self.tfEmail.isUserInteractionEnabled = true
        self.btnPassportImage.isUserInteractionEnabled = true
        self.btnProfileImage.isUserInteractionEnabled = true
    }
    private func disableUserInteraction(){
        self.tfNationality.isUserInteractionEnabled = false
        self.tfResidency.isUserInteractionEnabled = false
        self.tfFullName.isUserInteractionEnabled = false
        self.tfMobile.isUserInteractionEnabled = false
        self.tfEmail.isUserInteractionEnabled = false
        self.btnPassportImage.isUserInteractionEnabled = false
        self.btnProfileImage.isUserInteractionEnabled = false
    }
    private func showEmailTextFieldAlertPopup() {
    }
}
//MARK:- Image picker
extension VisaMemberDetail{
    func uploadFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
    }
    func uploadFromGallery(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
        
    }
    func uploadImage(){
        let alert = UIAlertController(title: "Upload Photo" , message: "How do you want to set your photo?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
            self.uploadFromCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
            self.uploadFromGallery()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
//MARK: - UIImagePickerControllerDelegate
extension VisaMemberDetail: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            switch self.imagePickerType{
            case .profile:
                self.imgProfilePhoto.image = pickedImage
            case .passport:
                self.imgPassport.image = pickedImage
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- Service
extension VisaMemberDetail{
    private func updateVisaMemberDetail(){
        var params = [String:Any]()
        params["memberid"] = self.memberDetailModel.id ?? "0"
        params["userid"] = self.memberDetailModel.userid ?? "0"
        if !(self.tfNationality.text ?? "").isEmpty{
            params["nationality"] = self.tfNationality.text ?? ""
        }
        if !(self.tfResidency.text ?? "").isEmpty{
            params["livingin"] = self.tfResidency.text ?? ""
        }
        if !(self.tfFullName.text ?? "").isEmpty{
            params["fullname"] = self.tfFullName.text ?? ""
        }
        if !(self.tfMobile.text ?? "").isEmpty{
            params["mobile"] = self.tfMobile.text ?? ""
        }
        if !(self.tfEmail.text ?? "").isEmpty{
            params["email"] = self.tfEmail.text ?? ""
        }
        if self.imgPassport.image != nil{
            params["passport[0]"] = UIImageJPEGRepresentation(self.imgPassport.image!, 0.08) ?? Data()
        }
        if self.imgProfilePhoto.image != nil{
            params["photo"] = UIImageJPEGRepresentation(self.imgProfilePhoto.image!, 0.08) ?? Data()
        }
        print(params)
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.UpdateVisaMemberDetail(params: params, success: { (responseObject) in
            Utility.hideLoader()
            self.btnEditApplication.isSelected = false
            self.disableUserInteraction()
            print(responseObject)
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
        
        
    }
}
