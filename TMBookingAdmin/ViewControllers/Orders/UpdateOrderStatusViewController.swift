//
//  UpdateOrderStatusViewController.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 16/07/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import SwiftDate
import DropDown
import SimpleImageViewer

class UpdateOrderStatusViewController: BaseViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var orderStatusTextField: UITextField!
    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var orderDateLabel: UILabel!
    @IBOutlet weak var orderPriceLabel: UILabel!
    @IBOutlet weak var orderPaymentSlipsView: UIView!
    
    private var orderStatusDropDown: DropDown!
    var selectedOrderStatus: OrderStatusModel?
    var arrOrderStatus = Array<OrderStatusModel>()
    
    private var arrPaymentSlips = Array<String>()
    
    private let dispatchGroup = DispatchGroup()

    var orderDetails: Order?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setData()
        self.setOrderStatusDropDown(orderStatusTextField)
        //self.getAllOrderStatus()
        self.getOrderPaymentSlips()
        self.setDispatchGroup()
    }
    
    private func setDispatchGroup() {
        Utility.showLoader()
        
        self.dispatchGroup.notify(queue: .main) {
            Utility.hideLoader()
        }
    }
    
    private func setData() {
        if let data = self.orderDetails {
            self.orderNumberLabel.text = String(format: "Order #%d", data.id ?? 0)
            self.orderPriceLabel.text = String(format: "Total: SAR %@", data.total ?? 0.00)
         
            if let createdDate = data.createdAt {
                let date = DateInRegion(createdDate, format: DateUtil.FORMAT_WOOCOMMERCE)
                self.orderDateLabel.text = date?.toString(.custom("MMM dd, yyyy"))
            }
            
            let orderStatus = self.arrOrderStatus.filter {$0.key == data.status}.first
            self.selectedOrderStatus = orderStatus
            self.orderStatusTextField.text = orderStatus?.value ?? ""
        }
    }
    
    private func setOrderStatusDropDown(_ textField: UITextField) {
        textField.delegate = self
        
        if let image = UIImage(named: "ic_dropdown") {
            textField.addPaddingRightIcon(image, padding: 8.0)
        }
        
        let dropdown = DropDown()
        dropdown.anchorView = textField
        dropdown.dataSource = self.arrOrderStatus.map {$0.value ?? ""}
        dropdown.bottomOffset = CGPoint(x: 0, y: (dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.selectionAction = { (index: Int, item: String) in
            textField.text = item
            self.selectedOrderStatus = self.arrOrderStatus[index]
        }
        
        self.orderStatusDropDown = dropdown
    }
    
    private func showFullScreenImageView(_ imageView: UIImageView) {
        // Show full screen image view
        let configuration = ImageViewerConfiguration { config in
            config.imageView = imageView
        }
        
        let imageViewerController = ImageViewerController(configuration: configuration)
        
        self.present(imageViewerController, animated: true)
    }

    @IBAction func onBtnSubmit(_ sender: AnyObject) {
        guard self.selectedOrderStatus != nil else {
            Utility.showAlert(title: "Message", message: "Order status not provided")
            return
        }
        
        self.updateOrderStatus()
    }
    
    private func getAllOrderStatus() {
        self.dispatchGroup.enter()
        APIManager.sharedInstance.apiManagerServicesWordPress.getAllOrderStatus(params: [:], success: { (response) in
            self.dispatchGroup.leave()
            if let array = response as? [AnyObject] {
                self.arrOrderStatus.removeAll()
                for item in array {
                    if let model = OrderStatusModel(JSON: item as! [String: Any]) {
                        self.arrOrderStatus.append(model)
                    }
                }
                self.orderStatusDropDown.dataSource = self.arrOrderStatus.map {$0.value ?? ""}
            }
        }) { (error) in
            self.dispatchGroup.leave()
            print(error.localizedDescription)
        }
    }

    private func getOrderPaymentSlips() {
        self.dispatchGroup.enter()
        let params: [String: Any] = [
            "order_id": self.orderDetails?.id ?? 0
        ]
        APIManager.sharedInstance.apiManagerServicesWordPress.getOrderPaymentSlips(params: params, success: { (response) in
            self.dispatchGroup.leave()
            if let array = response as? [String] {
                self.arrPaymentSlips = array
                self.collectionView.reloadData()
            }
        }) { (error) in
            self.dispatchGroup.leave()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }

    private func updateOrderStatus() {
        Utility.showLoader()
        let params: [String: Any] = [
            "order_id": self.orderDetails?.id ?? 0,
            "status": self.selectedOrderStatus?.key ?? ""
        ]
        APIManager.sharedInstance.apiManagerServicesWordPress.updateOrderStatus(params: params, success: { (response) in
            Utility.hideLoader()
            Utility.showAlert(title: "Message", message: "Order status updated", closure: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UpdateOrderStatusViewController: UITextFieldDelegate {
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.orderStatusTextField {
            self.orderStatusDropDown.show()
            return false
        }
        
        return true
    }
}

extension UpdateOrderStatusViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.orderPaymentSlipsView.isHidden = (self.arrPaymentSlips.count == 0) ? true : false
        
        return self.arrPaymentSlips.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PaySlipCellIdentifier", for: indexPath) as! OrderPaymentSlipViewCell
		cell.imageView.loadImage(withURL: self.arrPaymentSlips[indexPath.row], placeholderImage: UIImage(named: "placeholder"))
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! OrderPaymentSlipViewCell
        self.showFullScreenImageView(cell.imageView)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.height, height: collectionView.frame.size.height)
    }
}

