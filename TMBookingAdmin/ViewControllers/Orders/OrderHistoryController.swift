//
//  ViewController.swift
//  TMBookingTemp
//
//  Created by Ahmed Shahid on 4/16/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import UIKit
import OAuthSwift

class OrderHistoryController: BaseViewController {
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var lblNoOrderAvailable: UILabel!
    
    var oauthSwift: OAuth1Swift?
    private var dataSource = [Order]()
    private var currentPage = 1
    private var isDataLoading = false
    
    private var arrOrderStatus = Array<OrderStatusModel>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Order History"
        
        // Do any additional setup after loading the view, typically from a nib.
        self.getAllOrderStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        currentPage = 1
        self.getMyOrders(refreshData: true)
    }
    
    private func showUpdateOrderStatus(_ indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "UpdateOrderStatusViewController")
            as! UpdateOrderStatusViewController
        
        controller.orderDetails = self.dataSource[indexPath.row]
        controller.arrOrderStatus = self.arrOrderStatus
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - UITABLEVIEW DATASOURCE & DELEGATE
extension OrderHistoryController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryCell") as? OrderHistoryCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        
        let order = self.dataSource[indexPath.row]
        let orderStatus = self.arrOrderStatus.filter {$0.key == order.status}.first
        cell.updateUI(data: dataSource[indexPath.row], status: orderStatus?.value ?? (order.status ?? ""))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showUpdateOrderStatus(indexPath)
    }
}

extension OrderHistoryController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        print("scrollViewWillBeginDragging")
        isDataLoading = false
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("scrollViewDidEndDragging")
        if (self.tableview.contentOffset.y + self.tableview.frame.size.height) >= self.tableview.contentSize.height {
            if self.isDataLoading == false {
                isDataLoading = true
                currentPage = currentPage + 1
                self.getMyOrders()
            }
        }
    }
}

// MARK: - APIs
extension OrderHistoryController {
    private func getMyOrders(refreshData: Bool = false) {
        Utility.showLoader()

        // create an instance and retain it
        let oauth1 = OAuth1Swift(
            consumerKey: Constants.CUSTOMER_KEY,
            consumerSecret: Constants.CUSTOMER_SECERT
        )
        
        self.oauthSwift = oauth1
        self.oauthSwift?.client.paramsLocation = .requestURIQuery
        
        let route = URL(string: "https://tmbooking.com/wp-json/wc/v2/orders")
        let params: [String: Any] = ["page": currentPage]
        
        _ = self.oauthSwift?.client.get((route?.absoluteString)!, parameters: params, headers: nil, success: { (response) in
            Utility.hideLoader()
            
            if let responseArray = try! JSONSerialization.jsonObject(with: response.data, options: []) as? [Any] {
                if refreshData {
                    self.dataSource.removeAll()
                }
                for item in responseArray {
                    let order = Order(JSON: item as! [String: AnyObject])
                    self.dataSource.append(order!)
                }

                self.lblNoOrderAvailable.isHidden = (responseArray.count > 0) ? true : false
                self.tableview.reloadData()
            }

        }, failure: { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        })
    }
    
    private func getAllOrderStatus() {
        APIManager.sharedInstance.apiManagerServicesWordPress.getAllOrderStatus(params: [:], success: { (response) in
            if let array = response as? [AnyObject] {
                var arrOrderStatus = Array<OrderStatusModel>()
                for item in array {
                    if let model = OrderStatusModel(JSON: item as! [String: Any]) {
                        arrOrderStatus.append(model)
                    }
                }
                self.arrOrderStatus = arrOrderStatus
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
