//
//  LoginViewController.swift
//  AdminTMBooking
//
//  Created by Akber Sayni on 03/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class LoginViewController: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBtnLogin(_ sender: AnyObject) {
        if isValidated() {
            getAuthToken(email: (self.txtEmail.text)!, pwd: (self.txtPassword.text)!)
        }
    }

    private func isValidated() -> Bool {
        if (self.txtEmail.text?.isEmpty)! || (self.txtPassword.text?.isEmpty)!{
            Utility.showAlert(title: "Error", message: "Required information not provided")
            return false
        }
        
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginViewController {
    private func getAuthToken(email: String, pwd: String){
        APIManager.sharedInstance.apiManagerServicesWordPress.authToken(email: email, pwd: pwd, success: {
            (success) in
            self.onSuccessResponseToken(success)
        })
    }
    
    private func userLogin(){
        APIManager.sharedInstance.apiManagerServicesWordPress.signInWith(success: {
            (success) in
            self.onSuccessResponseLogin(success)
        })
    }
    
    private func onSuccessResponseToken(_ result: AnyObject){
        if let token = result["token"] as? String {
            Defaults[.token] = token
            userLogin()
        } else if let message = result["message"] as? String {
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: message.html2String)
        } else {
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: "Something went wrong. Please try again")
        }
    }
    
    private func onSuccessResponseLogin(_ result: AnyObject){
        Utility.hideLoader()
        if let resultObject = result as? NSDictionary {
            if let userId = resultObject["id"] as? NSNumber {
                do {
                    let data = try JSONSerialization.data(withJSONObject: result, options: .prettyPrinted)
                    Defaults[.loginStatus] = true
                    Defaults[.loginData] = data
                    Defaults[.loginUserId] = userId.stringValue
					Constants.APP_DELEGATE.changeRootViewController(updateDeviceTokenToServer: true)
                } catch let error {
                    Utility.showAlert(title: "Error", message: error.localizedDescription)
                }
            } else {
                Utility.showAlert(title: "Error", message: "Failed to get user details. Please try again later")
            }
        } else {
            Utility.showAlert(title: "Error", message: "Failed to get user details. Please try again later")
        }
    }
}
