//
//  NotificationListController.swift
//  Template
//
//  Created by Akber Sayni on 07/11/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class NotificationListController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var arrNotifications = [NotificationListResponse]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.        
        self.getUserNotifications()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK: - Web API Methods
extension NotificationListController {
    func getUserNotifications() {
        let params = ["user_id": Defaults[.loginUserId],
                      "notification_for": "admin"]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getUserNotifications(params: params, success: { (response) in
            Utility.hideLoader()
            if let message = response["message"] as? String {
                Utility.showAlert(title: "Error", message: message.html2String)
            } else {
                do {
                    let data =  try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                    let array = try JSONDecoder().decode([NotificationListResponse].self, from: data)
                    self.arrNotifications = array
                    self.tableView.reloadData()
                } catch let error {
                    Utility.showAlert(title: "Error", message: error.localizedDescription)
                }
            }
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
}

//MARK: - UITableView DataSource Delegate

extension NotificationListController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNotifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationListCellIdentifier", for: indexPath) as! NotificationListCell
        cell.lblTitle.text = self.arrNotifications[indexPath.row].title
        if (cell.lblTitle.text?.isEmpty)! {
            cell.lblTitle.text = "Message"
        }
        cell.lblMessage.text = self.arrNotifications[indexPath.row].message ?? ""
        cell.lblDateTime.text = self.arrNotifications[indexPath.row].createdAt ?? ""
        cell.lblOrderId.text = self.arrNotifications[indexPath.row].orderId ?? ""
        cell.lblGuestName.text = self.arrNotifications[indexPath.row].guestName ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
