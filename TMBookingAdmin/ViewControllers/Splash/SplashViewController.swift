//
//  SplashViewController.swift
//  AdminTMBooking
//
//  Created by Akber Sayni on 30/07/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Load home root view controller
        self.loadHomeRootViewController()
    }
    
    private func loadHomeRootViewController() {
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
            Global.APP_DELEGATE.changeRootViewController()
        }
    }
}
