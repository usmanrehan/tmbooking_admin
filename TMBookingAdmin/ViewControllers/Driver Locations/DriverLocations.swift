//
//  DriverLocations.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 06/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import GoogleMaps
import ObjectMapper
import GooglePlacePicker

class DriverLocations: BaseViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var tfLocation: UITextField!
    
    var driverModel = DriverModel()
    var isDetail = false
    var arrDrivers = [DriverModel]()
    var arrayMarkers    = [CustomMarker]()
    var markerIcon: UIImageView?
    
    var infoWindow      = ProfileView(frame: CGRect(x: 0, y: 0, width: 200, height: 100))
    var tappedMarker    = GMSMarker()
    let locationManager = CLLocationManager()
    let currentLocationMarker = GMSMarker()
    let reloadDriversTime = 240
    
    weak var timer: Timer?
    var timerDispatchSourceTimer : DispatchSourceTimer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Drivers Location"
        if !self.isDetail{
            self.getDriverList()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.mapView.delegate = self
        if !self.isDetail{
            self.startTimer()
        }
        else{
            self.addSingleDriverForDetail()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !self.isDetail{
            self.stopTimer()
            self.mapView.delegate = nil
        }
    }
    
    @IBAction func onBtnSearchLocation(_ sender: UIButton) {
        self.pickPlace()
    }
}
// MARK: - Google Place Picker
extension DriverLocations: GMSPlacePickerViewControllerDelegate {
    // The code snippet below shows how to create and display a GMSPlacePickerViewController.
    private func pickPlace() {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
    }
    // To receive the results from the place picker 'self' will need to conform to
    // GMSPlacePickerViewControllerDelegate and implement this code.
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true) {
            self.tfLocation.text = place.name
            self.zoomToSearchLocation(location: place.coordinate)
        }
    }
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        print("No place selected")
    }
}
//MARK: - MarkerInfo window related configuration methods
extension DriverLocations{
    private func addMarkerInfoWindow(marker: GMSMarker) {
        //add custome marker info window
        self.infoWindow.removeFromSuperview()
        let location = CLLocationCoordinate2D(latitude: marker.position.latitude, longitude: marker.position.longitude)
        let frame = CGRect(x: 0, y: 0, width: 330, height: 150)
        if let pV = ProfileView.instanceFromNib() as? ProfileView {
            infoWindow = pV
            pV.frame = frame
            var point = mapView.projection.point(for: location)
            let yOffset = CGFloat(35)
            point.x -= (330)/2
            point.y -= (135) - yOffset
            infoWindow.center = point
            pV.btnShowProfile.addTarget(self, action: #selector(self.pushToDriverProfile), for: .touchUpInside)
            pV.btnAssignBooking.addTarget(self, action: #selector(self.pushToPendingBookingsList), for: .touchUpInside)
            
            if let driverObj = marker as? CustomMarker {
                if let data = driverObj.driverModel{
                    pV.lblCarName.text = data.assignedCar ?? "-"
                    pV.lblDriverName.text = data.name ?? "-"
                    pV.lblLicenseNumber.text = data.licenceNo ?? "-"
                    pV.lblEmergencyNumber.text = data.emergencyContact ?? "-"
                    self.driverModel = data
                }
            }
            
            //self.configureInfoWindow(infoWindow: pV, marker: marker)
            self.mapView.addSubview(infoWindow)
        }
    }

    @objc func pushToDriverProfile(){
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "DriverDetail") as? DriverDetail{
            controller.driverModel = self.driverModel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @objc func pushToPendingBookingsList(){
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "Bookings") as? Bookings{
            controller.bookingType = .pending
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    private func addSingleDriverForDetail(){
        let latitude = self.driverModel.latitude ?? "0.0"
        let longitude = self.driverModel.longitude ?? "0.0"
        let location = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!)
        self.addMarkerAt(location: location, driver: self.driverModel)
        self.zoomToSearchLocation(location: location)
    }
}
extension DriverLocations: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool{
        self.addMarkerInfoWindow(marker: marker)
        self.tappedMarker = marker
        return true
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition)         {
        //move marker info window on swipe or map change
        let location = CLLocationCoordinate2D(latitude: tappedMarker.position.latitude, longitude: tappedMarker.position.longitude)
        var point = mapView.projection.point(for: location)
        let yOffset = CGFloat(35)
        point.x -= (330)/2
        point.y -= (135) - yOffset
        infoWindow.center = point
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D)   {
        //remove custom window on tap anywhere else on map
        self.infoWindow.removeFromSuperview()
    }
}
//MARK:- Handle Drivers
extension DriverLocations{
    private func getAllDrivers(drivers: [DriverModel]){
        for driver in drivers {
            self.handleDriver(driver: driver)
        }
    }
    private func handleDriver(driver: DriverModel){
        if isDriverExist(driver: driver) {
            //move
            self.moveUser(driver: driver)
        }
        else{
            //add
            self.addUser(driver: driver)
        }
    }
    private func isDriverExist(driver: DriverModel) -> Bool    {
        //Check if the driver exist in array user if it does return true else false
        var contains = false
        for item in self.arrDrivers {
            if item.driverid == driver.driverid {
                contains = true
                break
            }else {
                contains = false
            }
        }
        return contains
    }
    private func addUser(driver: DriverModel){
        //Driver does not exist previously in the array so add it to the array and also add its marker on map
        let latitude = driver.latitude ?? ""
        let longitude = driver.longitude ?? ""
        if Validation.isDoubleParsable(value: latitude) || Validation.isDoubleParsable(value: longitude){
            self.arrDrivers.append(driver)
            print("========>")
            let location = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!)
            self.addMarkerAt(location: location, driver: driver)
        }
    }
    private func moveUser(driver: DriverModel){
        //Driver existed previously in the array so search for it and then only move its marker, dont re-add it.
        for marker in self.arrayMarkers {
            if marker.driverModel?.driverid == driver.driverid {
                CATransaction.begin()
                CATransaction.setAnimationDuration(0.5)
                let location = CLLocationCoordinate2D(latitude: Double(driver.latitude ?? "0.0")!, longitude: Double(driver.longitude ?? "0.0")!)
                marker.position = location
                if marker.map == nil {
                    marker.map = self.mapView
                }
                marker.driverModel = driver
                CATransaction.commit()
            }else {
                marker.map = nil
                marker.driverModel = driver
            }
        }
        //update user in self.array user
        for i in 0..<self.arrDrivers.count {
            if self.arrDrivers[i].driverid == driver.driverid {
                self.arrDrivers[i] = driver
            }
        }
    }
    private func addMarkerAt(location: CLLocationCoordinate2D, driver: DriverModel) {
        //Add other user marker at coordinate in red color
        let position = location
        let currentlocationMarker = CustomMarker(position: position)
        currentlocationMarker.driverModel = driver
        
        let house = UIImage(named: "driver_pin")!.withRenderingMode(.alwaysOriginal)
        let markerView = UIImageView(image: house)
        self.markerIcon = markerView
        
        // add custom view to marker
        currentlocationMarker.iconView = markerView
        currentlocationMarker.tracksViewChanges = true
        currentlocationMarker.map = mapView
        
        // add marker to map
        currentLocationMarker.map = mapView
        self.arrayMarkers.append(currentlocationMarker)
    }
    private func zoomToSearchLocation(location: CLLocationCoordinate2D){
        CATransaction.begin()
        CATransaction.setValue(1, forKey: kCATransactionAnimationDuration)
        let fancy = GMSCameraPosition.camera(withLatitude: location.latitude,
                                             longitude: location.longitude - 0.0002,
                                             zoom: 10,
                                             bearing: 0,
                                             viewingAngle: 45)
        self.mapView.animate(to: fancy)
        CATransaction.commit()
    }
}
//MARK:- Services
extension DriverLocations{
    private func getDriverList(){
        let params = [String:Any]()
        APIManager.sharedInstance.usersAPIManager.getDriverList(params: params, success: { (responseArray) in
            let response = Mapper<DriverModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            self.getAllDrivers(drivers: response)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
extension DriverLocations{
    func startTimer() {
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: TimeInterval(self.reloadDriversTime), repeats: true) { [weak self] _ in
                // do something here
                self?.getDriverList()
            }
        }
    }
    func stopTimer() {
        timer?.invalidate()
        //timerDispatchSourceTimer?.suspend() // if you want to suspend timer
        timerDispatchSourceTimer?.cancel()
    }
}
