//
//  ProfileView.swift
//  CatchMapp
//
//  Created by Hassan Khan on 10/25/17.
//  Copyright © 2017 Hassan Khan. All rights reserved.
//

import UIKit

class ProfileView: UIView {

    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblLicenseNumber: UILabel!
    @IBOutlet weak var lblEmergencyNumber: UILabel!
    @IBOutlet weak var btnShowProfile: UIButton!
    @IBOutlet weak var btnAssignBooking: UIButton!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "ProfileView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
