//
//  DriverDetail.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 07/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import SDWebImage
import ObjectMapper
import DropDown

enum ImageType{
    case driverImage
    case licenseImage
}
enum DatePickedType{
    case LicenseExpiry
    case JoiningDate
}
class DriverDetail: BaseViewController {

    @IBOutlet weak var svBtnsBlockDelete: UIStackView!
    @IBOutlet weak var btnEdit: UIBarButtonItem!
    @IBOutlet weak var imgDriverPhoto: UIImageView!
    @IBOutlet weak var imgLicensePhoto: UIImageView!
    @IBOutlet weak var btnDriverPhotoPicker: UIButton!
    @IBOutlet weak var btnLicensePhotoPicker: UIButton!
    @IBOutlet weak var tfVendors: UITextField!
    @IBOutlet weak var tfDriverName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfIkama: UITextField!
    @IBOutlet weak var tfNationality: UITextField!
    @IBOutlet weak var tfKafilName: UITextField!
    @IBOutlet weak var tfEmergencyNumber: UITextField!
    @IBOutlet weak var tfAge: UITextField!
    @IBOutlet weak var tfLicenseNumber: UITextField!
    @IBOutlet weak var tfLicenseExpiry: UITextField!
    @IBOutlet weak var tfRegistrationNumber: UITextField!
    @IBOutlet weak var tfCar: UITextField!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var tfState: UITextField!
    @IBOutlet weak var tfZipCode: UITextField!
    @IBOutlet weak var tfHomePhone: UITextField!
    @IBOutlet weak var tfWorkPhone: UITextField!
    @IBOutlet weak var tfJoiningDate: UITextField!
    @IBOutlet weak var btnBlock: UIButton!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var svLiveLocationTxt: UIStackView!
    
    var driverModel: DriverModel?
    var flagIsDriverDetail = true
    var flagCanEdit = false
    var imageType = ImageType.driverImage
    var driverImage: UIImage?
    var licenseImage: UIImage?
    var imagePicker: UIImagePickerController!
    var datePickedType: DatePickedType = .LicenseExpiry
    var arrVendors: [TransportVendor]?
    var selectedVendor: TransportVendor?
    
    var vendorDropDown: DropDown!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tfLicenseExpiry.delegate = self
        self.tfJoiningDate.delegate = self

        // Do any additional setup after loading the view.
        self.setUI()
        self.setVendorDropDown(self.tfVendors)
        self.getTransportVendors()
    }

    @IBAction func onBtnImagePicker(_ sender: UIButton) {
        if sender.tag == 0{//Driver Image
            self.imageType = .driverImage
        }
        else{
            self.imageType = .licenseImage
        }
        self.uploadImage()
    }
    @IBAction func onBtnEdit(_ sender: UIBarButtonItem) {
        self.driverImage = self.imgDriverPhoto.image
        self.licenseImage = self.imgLicensePhoto.image
        if self.flagIsDriverDetail{
            if !self.flagCanEdit{
                self.btnEdit.title = "Update"
                self.setUserInteractionEnable()
                self.flagCanEdit = !self.flagCanEdit
            }
            else{
                self.validate()
            }
        }
        else{//Register Driver
            self.validate()
        }
    }
    @IBAction func onBtnDriverLocation(_ sender: RoundedButton) {
        let latitude = self.driverModel?.latitude ?? ""
        let longitude = self.driverModel?.longitude ?? ""
        if Validation.isDoubleParsable(value: latitude) && Validation.isDoubleParsable(value: longitude){
            self.pushToDriversLocation()
        }
        else{
            Utility.showAlert(title: "Error", message: "Invalid location.")
        }
    }
    @IBAction func onBtnDeleteDriver(_ sender: UIButton) {
        Utility.showAlertWithYesNo(title: "Confirmation", message: "Are you sure to remove this record!") { (true) in
            self.removeDriver()
        }
    }
    @IBAction func onBtnBlockDriver(_ sender: UIButton) {
        guard let status = self.driverModel?.status else {return}
        switch status {
        case "1"://driver is un-block
            Utility.showAlertWithYesNo(title: "Confirmation", message: "Block this driver?") { (bool) in
                self.blockUnBlockDriverWith(status: "2")//2 to block
            }
        default:
            Utility.showAlertWithYesNo(title: "Confirmation", message: "Un-block this driver?") { (bool) in
                self.blockUnBlockDriverWith(status: "1")//1 to Unblock
            }
        }
    }
    @IBAction func onTfLicenseExpiry(_ sender: UITextField) {
        self.datePickedType = .LicenseExpiry
        self.getPickedDate(sender: sender)
    }
    @IBAction func onTfJoiningDate(_ sender: UITextField) {
        self.datePickedType = .JoiningDate
        self.getPickedDate(sender: sender)
    }
}
extension DriverDetail{
    private func setUI(){
        self.title = "Driver Detail"
        if !self.flagIsDriverDetail {
            self.setUserInteractionEnable()
            self.svBtnsBlockDelete.isHidden = true
            //self.svLiveLocationTxt.isHidden = true
            //self.btnLocation.isHidden = true
        }
        else{
            self.btnEdit.title = "Edit"
            self.svBtnsBlockDelete.isHidden = false
            self.setData(data: self.driverModel ?? DriverModel())
            //self.svLiveLocationTxt.isHidden = false
            //self.btnLocation.isHidden = false
        }
    }
    private func setData(data: DriverModel){
        let DriverImage = data.driversPhoto ?? "http://tmbooking.com/tmbooking/public/img/placeholder.png"
        self.imgDriverPhoto.sd_setImage(with: Utility.stringToUrl(string: DriverImage), completed: nil)
        let DriverLicenseImage = data.licensePhoto ?? "http://tmbooking.com/tmbooking/public/img/placeholder.png"
        self.imgLicensePhoto.sd_setImage(with: Utility.stringToUrl(string: DriverLicenseImage), completed: nil)
        self.driverImage = self.imgDriverPhoto.image
        self.licenseImage = self.imgLicensePhoto.image

        let vendor = TransportVendor()
        vendor.id = Int(data.vendorId ?? "0")!
        vendor.name = data.vendorName
        self.selectedVendor = vendor
        
        self.tfVendors.text = data.vendorName ?? "-"
        self.tfDriverName.text = data.name ?? "-"
        self.tfEmail.text = data.email ?? "-"
        self.tfPassword.text = data.password ?? "-"
        self.tfIkama.text = data.ikama ?? "-"
        self.tfNationality.text = data.nationality ?? "-"
        self.tfKafilName.text = data.kafilName ?? "-"
        self.tfEmergencyNumber.text = data.emergencyContact ?? "-"
        self.tfAge.text = data.age ?? "-"
        self.tfLicenseNumber.text = data.licenceNo ?? "-"
        self.tfLicenseExpiry.text = data.licenseExpiry ?? "-"
        self.tfRegistrationNumber.text = data.registerationNumnber ?? "-"
        self.tfCar.text = data.assignedCar ?? "-"
        self.tfAddress.text = data.address ?? "-"
        self.tfCity.text = data.city ?? "-"
        self.tfState.text = data.state ?? "-"
        self.tfZipCode.text = data.zipcode ?? "-"
        self.tfHomePhone.text = data.homePhone ?? "-"
        self.tfWorkPhone.text = data.workPhone ?? "-"
        self.tfJoiningDate.text = data.joinning ?? "-"
        guard let status = data.status else {return}
        switch status {
        case "1"://driver is un-blocked
            self.btnBlock.isSelected = false
        default:
            self.btnBlock.isSelected = true
        }
    }
    private func setVendorDropDown(_ anchorView: UITextField) {
        if let image = UIImage(named: "ic_dropdown") {
            anchorView.addPaddingRightIcon(image, padding: 8.0)
        }
        
        anchorView.delegate = self
        
        let dropdown = DropDown()
        dropdown.anchorView = anchorView
        dropdown.bottomOffset = CGPoint(x: 0, y: (dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.selectionAction = { (index: Int, item: String) in
            anchorView.text = item
            self.selectedVendor = self.arrVendors![index]
        }
        
        self.vendorDropDown = dropdown
    }
}
//MARK:- Image picker
extension DriverDetail{
    func uploadFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func uploadFromGallery(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
        
    }
    
    func uploadImage(){
        let alert = UIAlertController(title: "Upload Photo" , message: "How do you want to set your photo?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
            self.uploadFromCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
            self.uploadFromGallery()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    private func pushToDriversLocation(){
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "DriverLocations") as? DriverLocations {
            controller.driverModel = self.driverModel ?? DriverModel()
            controller.isDetail = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

//MARK:- UIImagePickerControllerDelegate
extension DriverDetail: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if self.imageType == .driverImage{
                self.driverImage = pickedImage
                self.imgDriverPhoto.image = pickedImage
            }
            else{
                self.licenseImage = pickedImage
                self.imgLicensePhoto.image = pickedImage
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- Helper Methods
extension DriverDetail{
    private func validate(){
        if self.driverImage == nil{
            Utility.showAlert(title: "Error", message: "Please provide driver's profile image.")
            return
        }
        if self.licenseImage == nil{
            Utility.showAlert(title: "Error", message: "Please provide driver's license image.")
            return
        }
        if self.selectedVendor == nil {
            Utility.showAlert(title: "Error", message: "Vendor details not provided")
            return
        }
        if (self.tfDriverName.text?.isEmpty)! || (self.tfEmail.text?.isEmpty)! || (self.tfPassword.text?.isEmpty)! || (self.tfIkama.text?.isEmpty)! || (self.tfNationality.text?.isEmpty)! || (self.tfKafilName.text?.isEmpty)! || (self.tfEmergencyNumber.text?.isEmpty)! || (self.tfAge.text?.isEmpty)! || (self.tfLicenseNumber.text?.isEmpty)! || (self.tfLicenseExpiry.text?.isEmpty)! || (self.tfRegistrationNumber.text?.isEmpty)! || (self.tfCar.text?.isEmpty)! || (self.tfAddress.text?.isEmpty)! || (self.tfCity.text?.isEmpty)! || (self.tfState.text?.isEmpty)! || (self.tfZipCode.text?.isEmpty)! || (self.tfHomePhone.text?.isEmpty)! || (self.tfWorkPhone.text?.isEmpty)! || (self.tfJoiningDate.text?.isEmpty)!{
            Utility.showAlert(title: "Error", message: "All fields are required!")
            return
        }
        if !Validation.isValidEmail(self.tfEmail.text ?? ""){
            Utility.showAlert(title: "Error", message: "Please provide a valid email address.")
            return
        }
        if self.flagIsDriverDetail{
            self.updateDriver()
        }
        else{
            self.addDriver()
        }
    }
    private func setUserInteractionEnable(){
        self.btnDriverPhotoPicker.isUserInteractionEnabled = true
        self.btnLicensePhotoPicker.isUserInteractionEnabled = true
        self.tfVendors.isUserInteractionEnabled = true
        self.tfDriverName.isUserInteractionEnabled = true
        self.tfEmail.isUserInteractionEnabled = true
        self.tfPassword.isUserInteractionEnabled = true
        self.tfIkama.isUserInteractionEnabled = true
        self.tfNationality.isUserInteractionEnabled = true
        self.tfKafilName.isUserInteractionEnabled = true
        self.tfEmergencyNumber.isUserInteractionEnabled = true
        self.tfAge.isUserInteractionEnabled = true
        self.tfLicenseNumber.isUserInteractionEnabled = true
        self.tfLicenseExpiry.isUserInteractionEnabled = true
        self.tfRegistrationNumber.isUserInteractionEnabled = true
        self.tfCar.isUserInteractionEnabled = true
        self.tfAddress.isUserInteractionEnabled = true
        self.tfCity.isUserInteractionEnabled = true
        self.tfState.isUserInteractionEnabled = true
        self.tfZipCode.isUserInteractionEnabled = true
        self.tfHomePhone.isUserInteractionEnabled = true
        self.tfWorkPhone.isUserInteractionEnabled = true
        self.tfJoiningDate.isUserInteractionEnabled = true
    }
    private func setUserInteractionDisable(){
        self.btnDriverPhotoPicker.isUserInteractionEnabled = false
        self.btnLicensePhotoPicker.isUserInteractionEnabled = false
        self.tfDriverName.isUserInteractionEnabled = false
        self.tfEmail.isUserInteractionEnabled = false
        self.tfPassword.isUserInteractionEnabled = false
        self.tfIkama.isUserInteractionEnabled = false
        self.tfNationality.isUserInteractionEnabled = false
        self.tfKafilName.isUserInteractionEnabled = false
        self.tfEmergencyNumber.isUserInteractionEnabled = false
        self.tfAge.isUserInteractionEnabled = false
        self.tfLicenseNumber.isUserInteractionEnabled = false
        self.tfLicenseExpiry.isUserInteractionEnabled = false
        self.tfRegistrationNumber.isUserInteractionEnabled = false
        self.tfCar.isUserInteractionEnabled = false
        self.tfAddress.isUserInteractionEnabled = false
        self.tfCity.isUserInteractionEnabled = false
        self.tfState.isUserInteractionEnabled = false
        self.tfZipCode.isUserInteractionEnabled = false
        self.tfHomePhone.isUserInteractionEnabled = false
        self.tfWorkPhone.isUserInteractionEnabled = false
        self.tfJoiningDate.isUserInteractionEnabled = false
    }
    private func getPickedDate(sender: UITextField){
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        if self.datePickedType == .LicenseExpiry{
            self.tfLicenseExpiry.text = dateFormatter.string(from: sender.date)
        }
        else{
            self.tfJoiningDate.text = dateFormatter.string(from: sender.date)
        }
    }
}
//MARK:- Services
extension DriverDetail{
    private func getAllParams()-> [String:Any]{
        let drivers_photo = UIImageJPEGRepresentation(self.driverImage ?? #imageLiteral(resourceName: "image_placeholder"), 0.1) ?? Data()
        let license_photo = UIImageJPEGRepresentation(self.licenseImage ?? #imageLiteral(resourceName: "image_placeholder"), 0.1) ?? Data()
        let vendorId = self.selectedVendor?.id ?? 0
        let name = self.tfDriverName.text ?? ""
        let email = self.tfEmail.text ?? ""
        let ikama = self.tfIkama.text ?? ""
        let nationality = self.tfIkama.text ?? ""
        let kafil_name = self.tfKafilName.text ?? ""
        let emergency_contact = self.tfEmergencyNumber.text ?? ""
        let licence_no = self.tfLicenseNumber.text ?? ""
        let license_expiry = self.tfLicenseExpiry.text ?? ""
        let assigned_Car = self.tfCar.text ?? ""
        let age = self.tfAge.text ?? ""
        let password = self.tfPassword.text ?? ""
        let address = self.tfAddress.text ?? ""
        let city = self.tfCity.text ?? ""
        let state = self.tfState.text ?? ""
        let zipcode = self.tfZipCode.text ?? ""
        let home_phone = self.tfHomePhone.text ?? ""
        let work_phone = self.tfWorkPhone.text ?? ""
        let joinning = self.tfJoiningDate.text ?? ""
        let registeration_numnber = self.tfRegistrationNumber.text ?? ""
        var id = ""
        if self.driverModel?.id ?? "" != ""{id = self.driverModel?.id ?? ""}
        if self.driverModel?.driverid ?? "" != ""{id = self.driverModel?.driverid ?? ""}
        
        let params: [String:Any] = ["drivers_photo":drivers_photo,
                                    "license_photo":license_photo,
                                    "vendor_id":vendorId,
                                    "name":name,
                                    "email":email,
                                    "ikama":ikama,
                                    "nationality":nationality,
                                    "kafil_name":kafil_name,
                                    "emergency_contact":emergency_contact,
                                    "licence_no":licence_no,
                                    "license_expiry":license_expiry,
                                    "assigned_Car":assigned_Car,
                                    "age":age,
                                    "password":password,
                                    "address":address,
                                    "city":city,
                                    "state":state,
                                    "zipcode":zipcode,
                                    "home_phone":home_phone,
                                    "work_phone":work_phone,
                                    "joinning":joinning,
                                    "registeration_numnber":registeration_numnber,
                                    "id":id
        ]
        return params
    }
    private func updateDriver(){
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.UpdateDriver(params: self.getAllParams(), success: { (responseObject) in
            Utility.hideLoader()
            print(responseObject)
            self.btnEdit.title = "Edit"
            self.setUserInteractionDisable()
            self.flagCanEdit = !self.flagCanEdit
            Utility.showAlert(title: "Success", message: "Record updated successfully", closure: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func addDriver(){
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.DriverRegistration(params: self.getAllParams(), success: { (responseObject) in
            Utility.hideLoader()
            Utility.showAlert(title: "Success", message: "Driver registered successfully", closure: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func removeDriver(){
        let param: [String:Any] = ["driverid":self.driverModel?.id ?? "0"]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.RemoveDriver(params: param, success: { (responseObject) in
            Utility.hideLoader()
            Utility.showAlert(title: "Success", message: "Driver deleted successfully!", closure: { (true) in
                self.navigationController?.popViewController(animated: true)
            })
            print(responseObject)
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func blockUnBlockDriverWith(status: String){
        //1 unblock
        //2 block
        var id = ""
        if self.driverModel?.id ?? "" != ""{id = self.driverModel?.id ?? ""}
        if self.driverModel?.driverid ?? "" != ""{id = self.driverModel?.driverid ?? ""}
        let params: [String:Any] = ["driverid":id,
                                    "status":status]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.Block_UnblockDriver(params: params, success: { (responseObject) in
            Utility.hideLoader()
            let response = Mapper<DriverModel>().map(JSON: responseObject as [String : Any]) ?? DriverModel()
            self.driverModel = response
            self.setData(data: self.driverModel ?? DriverModel())
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func getTransportVendors() {
        Utility.showLoader()
        APIManager.sharedInstance.apiManagerServicesWordPress.getTransportVendors(params: [:], success: { (response) in
            Utility.hideLoader()
            if let array = response as? [AnyObject] {
                var vendors = Array<TransportVendor>()
                for item in array {
                    let model = TransportVendor(JSON: item as! [String: Any])
                    vendors.append(model!)
                }                
                self.arrVendors = vendors
                self.vendorDropDown.dataSource = self.arrVendors!.map {$0.name ?? ""}
            }
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
}
extension DriverDetail: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.tfVendors {
            self.vendorDropDown.show()
            return false
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        if self.datePickedType == .LicenseExpiry && (self.tfLicenseExpiry.text?.isEmpty)!{
            self.tfLicenseExpiry.text = dateFormatter.string(from: Date())
        }
        else if self.datePickedType == .JoiningDate && (self.tfJoiningDate.text?.isEmpty)!{
            self.tfJoiningDate.text = dateFormatter.string(from: Date())
        }
    }
}
