//
//  DriversList.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 07/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class DriversList: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tfSearchField: UITextField!
    var arrDrivers = [DriverModel]()
    var arrFilteredDrivers = [DriverModel]()
    var isFilter = false
    var selectedDriver = DriverModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Drivers List"
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getDriverList()
    }
    
    @IBAction func onBtnAddDriver(_ sender: UIBarButtonItem) {
        self.pushToDriverDetail(driverModel: nil, flag: false)
    }
    
    @IBAction func onTfSearchFieldEditingChanged(_ sender: UITextField) {
        let searchText = sender.text ?? ""
        if searchText != ""{
            self.arrFilteredDrivers = self.arrDrivers.filter {
                ($0.name?.localizedCaseInsensitiveContains(searchText))! || ($0.driverid?.localizedCaseInsensitiveContains(searchText))!
            }
            self.isFilter = true
            self.tableView.reloadData()
        }
        else{
            self.isFilter = false
            self.tableView.reloadData()
        }
    }
}
//MARK:- Helper Methods
extension DriversList{
    private func pushToDriverDetail(driverModel: DriverModel?, flag:Bool){
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "DriverDetail") as? DriverDetail{
            controller.flagIsDriverDetail = flag
            controller.driverModel = driverModel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    private func pushToDriversLocation(){
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "DriverLocations") as? DriverLocations{
            controller.driverModel = self.selectedDriver
            controller.isDetail = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
//MARK:- UITableViewDataSource
extension DriversList: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !self.isFilter{
            return self.arrDrivers.count
        }
        else{
            return self.arrFilteredDrivers.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DriversListTVC", for: indexPath) as! DriversListTVC
        if !self.isFilter{
            cell.setData(data: self.arrDrivers[indexPath.row])
        }
        else{
            cell.setData(data: self.arrFilteredDrivers[indexPath.row])
        }
        cell.btnDriverLocation.tag = indexPath.row
        cell.btnDriverLocation.addTarget(self, action: #selector(onBtnDriverLocationPress(sender:)), for: .touchUpInside)
        return cell
    }
    @objc func onBtnDriverLocationPress(sender: RoundedButton){
        if !self.isFilter{
            self.selectedDriver = self.arrDrivers[sender.tag]
        }
        else{
            self.selectedDriver = self.arrFilteredDrivers[sender.tag]
        }
        let latitude = self.selectedDriver.latitude ?? ""
        let longitude = self.selectedDriver.longitude ?? ""
        if Validation.isDoubleParsable(value: latitude) && Validation.isDoubleParsable(value: longitude){
            self.pushToDriversLocation()
        }
        else{
            Utility.showAlert(title: "Error", message: "Invalid location.")
        }
    }
}
//MARK:- UITableViewDelegate
extension DriversList: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !self.isFilter{
            self.pushToDriverDetail(driverModel: self.arrDrivers[indexPath.row], flag: true)
        }
        else{
            self.pushToDriverDetail(driverModel: self.arrFilteredDrivers[indexPath.row], flag: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
//MARK:- Services
extension DriversList{
    private func getDriverList(){
        let params = [String:Any]()
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getDriverList(params: params, success: { (responseArray) in
            Utility.hideLoader()
            let response = Mapper<DriverModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            self.arrDrivers = response
            self.isFilter = false
            self.tfSearchField.text = ""
            self.tableView.reloadData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
