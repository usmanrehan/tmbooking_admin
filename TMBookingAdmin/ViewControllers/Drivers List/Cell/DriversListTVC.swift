//
//  DriversListTVC.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 07/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class DriversListTVC: UITableViewCell {

    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblLicenseNumber: UILabel!
    @IBOutlet weak var lblEmergencyNumber: UILabel!
    @IBOutlet weak var btnDriverLocation: RoundedButton!
        
    func setData(data: DriverModel){
        self.selectionStyle = .none
        let name = data.name ?? "-"
        let id = data.driverid ?? "0"
        self.lblDriverName.text =  name + "-" + id
        self.lblCarName.text = data.assignedCar ?? "-"
        self.lblLicenseNumber.text = data.licenceNo ?? "-"
        self.lblEmergencyNumber.text = data.emergencyContact ?? "-"
    }
}
