//
//  AddCountryVisaViewController.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 01/03/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import SimpleImageViewer

class AddCountryVisaViewController: BaseViewController {
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var countryNameTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!

    var imagePicker: UIImagePickerController!
    var countryImage: UIImage?
    
    var visaCountry: VisaCountry?
    var isEditingMode: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setData()
        self.updateUI()
    }
    
    private func setData() {
        guard self.visaCountry != nil else { return }
        
        self.countryNameTextField.text = self.visaCountry?.name
        self.countryImageView.loadImage(withURL: self.visaCountry?.imageURL)
    }
    
    private func updateUI() {
        if self.isEditingMode {
            self.submitButton.setTitle("UPDATE", for: .normal)
        } else {
            self.submitButton.setTitle("SUBMIT", for: .normal)
        }
    }
    
    @IBAction func onBtnAddImage(_ sender: AnyObject) {
        self.uploadImage()
    }
    
    @IBAction func imageTapped(_ gesture: UIGestureRecognizer) {
        if let imageView = gesture.view as? UIImageView {
            guard imageView.image != nil else {
                return
            }
            
            // Show full screen image view
            let configuration = ImageViewerConfiguration { config in
                config.imageView = imageView
            }
            
            let imageViewerController = ImageViewerController(configuration: configuration)
            
            self.present(imageViewerController, animated: true)
        }
    }
    
    @IBAction func onBtnSubmit(_ sender: AnyObject) {
        
        guard !(self.countryNameTextField.text?.isEmpty ?? true) else {
            Utility.showAlert(title: "Error", message: "Required information not provided")
            return
        }
        
        if self.isEditingMode == true {
            if let country = self.visaCountry {
                self.updateVisaCountry(country)
            } else {
                Utility.showAlert(title: "Error", message: "Failed to get country details. Please try again")
            }
        } else {
            self.addVisaCountry()
        }
    }

    // MARK: - API Methods
    
    private func addVisaCountry() {
        var params: [String: Any] = ["cname": self.countryNameTextField.text ?? ""]
        if let image = self.countryImage {
            let data = UIImageJPEGRepresentation(image, 0.5)
            params["photo"] = data
        }
        
        Utility.showLoader()
        
        APIManager.sharedInstance.usersAPIManager.addVisaCountry(params: params, success: { (response) in
            Utility.hideLoader()
            Utility.showAlert(title: "Message", message: "Visa country added successfully", closure: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }

    private func updateVisaCountry(_ country: VisaCountry) {
        var params: [String: Any] = ["countryid": country.id,
                                     "cname": self.countryNameTextField.text ?? ""]
        if let image = self.countryImage {
            let data = UIImageJPEGRepresentation(image, 0.5)
            params["photo"] = data
        }
        
        Utility.showLoader()
        
        APIManager.sharedInstance.usersAPIManager.updateVisaCountry(params: params, success: { (response) in
            Utility.hideLoader()
            Utility.showAlert(title: "Message", message: "Visa country updated successfully", closure: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK:- Image picker

extension AddCountryVisaViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func uploadFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
    }
    func uploadFromGallery(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
        
    }
    func uploadImage(){
        let alert = UIAlertController(title: "Upload Photo" , message: "How do you want to set your photo?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
            self.uploadFromCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
            self.uploadFromGallery()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerController Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.countryImage = pickedImage
            self.countryImageView.image = pickedImage
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}
