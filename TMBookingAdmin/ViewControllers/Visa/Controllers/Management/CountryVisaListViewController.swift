//
//  VisaController.swift
//  Template
//
//  Created by Akber Sayni on 10/04/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

enum VisaOptions: Int {
    case FamilyProfile = 0
    case VisaInfo
    case UAEVisas
    case SaudiArabiaVisas
}

class CountryVisaListViewController: BaseViewController {
    @IBOutlet var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: .zero)
        }
    }
    
    var arrVisaCountries = Array<VisaCountry>()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getVisaCountries()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func presentVisaInfoController() {
//        let controller = self.storyboard?.instantiateViewController(withIdentifier: "VisaWebViewController") as! VisaWebViewController
//        controller.url = URL(string: Constants.VISA_INFO_URL_STRING)
//
//        let navController = BaseNavigationController(rootViewController: controller)
//        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func onBtnAddVisaCountry(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "pushToAddCountryVisaSegue", sender: self)
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pushToUpdateCountryVisaSegue" {
            if let country = sender as? VisaCountry {
                let controller = segue.destination as! AddCountryVisaViewController
                controller.visaCountry = country
                controller.isEditingMode = true
            }
        } else if segue.identifier == "pushToVisaTypesSegue" {
            if let country = sender as? VisaCountry {
               let controller = segue.destination as! VisaTypesListViewController
                controller.selectedCountry = country
            }
        }
    }
    
    // MARK: - Web APIs Method
    
    private func getVisaCountries() {
        Utility.showLoader()
        
        APIManager.sharedInstance.usersAPIManager.getVisaCountriesList(params: [:], success: { (response) in
            Utility.hideLoader()
            
            self.arrVisaCountries.removeAll()
            for item in response {
                let model = VisaCountry(JSON: item as! [String: Any])
                self.arrVisaCountries.append(model!)
            }
            
            self.tableView.reloadData()
            
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    private func removeVisaCountry(_ countryId: String, indexPath: IndexPath) {
        let params = ["countryid": countryId]

        Utility.showLoader()
        
        APIManager.sharedInstance.usersAPIManager.removeVisaCountry(params: params, success: { (response) in
            Utility.hideLoader()
            self.arrVisaCountries.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            
            Utility.showAlert(title: "Message", message: "Record deleted successfully")
            
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
        
    }
}

extension CountryVisaListViewController: UITableViewDataSource, UITableViewDelegate {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrVisaCountries.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VisaCellIdentifier", for: indexPath) as! VisaCell
        cell.titleLable.text = self.arrVisaCountries[indexPath.row].name?.uppercased()
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let visaCountry = self.arrVisaCountries[indexPath.row]
        self.performSegue(withIdentifier: "pushToVisaTypesSegue", sender: visaCountry)
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Swipe left to edit or delete visa country"
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .default, title: "Edit") { (action, indexPath) in
            let country = self.arrVisaCountries[indexPath.row]
            self.performSegue(withIdentifier: "pushToUpdateCountryVisaSegue", sender: country)
        }
        edit.backgroundColor = .gray
        
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            self.removeVisaCountry(self.arrVisaCountries[indexPath.row].id, indexPath: indexPath)
        }
        
        return [delete, edit]
    }
}
