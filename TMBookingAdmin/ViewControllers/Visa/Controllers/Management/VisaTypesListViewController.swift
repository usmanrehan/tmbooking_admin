//
//  VisaListViewController.swift
//  Template
//
//  Created by Akber Sayni on 10/04/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class VisaTypesListViewController: BaseViewController {
    @IBOutlet var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView()
        }
    }
    
    var selectedCountry: VisaCountry?
    var selectedRow: Int?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = self.selectedCountry?.name
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBtnAddVisaType(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "pushToAddVisaTypeSegue", sender: self)
    }
    
    // MARK: API Methods
    
    private func removeVisaType(_ visaTypeId: String, indexPath: IndexPath) {
        let params = ["visatypeid": visaTypeId]
        
        Utility.showLoader()
        
        APIManager.sharedInstance.usersAPIManager.removeVisaType(params: params, success: { (response) in
            Utility.hideLoader()
            
            self.selectedCountry?.visaTypes?.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            
            Utility.showAlert(title: "Message", message: "Record deleted successfully")
            
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pushToAddVisaTypeSegue" {
            let controller = segue.destination as! VisaTypeDetail
            controller.delegate = self
            controller.countryId = self.selectedCountry?.id
            
        } else if segue.identifier == "pushToUpdateVisaTypeSegue" {
            let controller = segue.destination as! VisaTypeDetail
            controller.delegate = self
            controller.flagCanEdit = true
            controller.countryId = self.selectedCountry?.id
            controller.visaTypeModel = sender as! VisaTypeModel
        }
    }
}

extension VisaTypesListViewController: UITableViewDataSource, UITableViewDelegate {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.selectedCountry?.visaTypes?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VisaListCellIdentifier", for: indexPath) as! VisaListViewCell
        cell.setData((self.selectedCountry?.visaTypes![indexPath.row])!)
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let visaTypeModel = self.selectedCountry?.visaTypes![indexPath.row]
        self.selectedRow = indexPath.row
        self.performSegue(withIdentifier: "pushToUpdateVisaTypeSegue", sender: visaTypeModel)
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Swipe left to delete the item"
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    public func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            self.removeVisaType((self.selectedCountry?.visaTypes![indexPath.row].id)!, indexPath: indexPath)
        }
        
        return [delete]
    }
}

extension VisaTypesListViewController: VisaTypeDetailsDelegate {
    func addNewVisaType(_ visaType: VisaTypeModel) {
        self.selectedCountry?.visaTypes?.append(visaType)
        self.tableView.reloadData()
    }
    
    func updateVisaType(_ visaType: VisaTypeModel) {
        if let index = self.selectedRow {
            self.selectedCountry?.visaTypes![index] = visaType
            self.tableView.reloadData()
        }
    }
}
