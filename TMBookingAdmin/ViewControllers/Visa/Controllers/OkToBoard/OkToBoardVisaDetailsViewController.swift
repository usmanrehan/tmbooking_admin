//
//  OkToBoardVisaDetailsViewController.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 10/08/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper
import DropDown

class OkToBoardVisaDetailsViewController: BaseViewController {
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var dateOfEntryTextField: UITextField!
    @IBOutlet weak var stampTextField: UITextField!

    @IBOutlet weak var cnicImageView: UIImageView!
    @IBOutlet weak var visaImageView: UIImageView!
    @IBOutlet weak var airlineImageView: UIImageView!
    @IBOutlet weak var passportImageView: UIImageView!

    @IBOutlet weak var cnicImageSwitch: UISwitch!
    @IBOutlet weak var visaImageSwitch: UISwitch!
    @IBOutlet weak var airlineImageSwitch: UISwitch!
    @IBOutlet weak var passportImageSwitch: UISwitch!

    @IBOutlet weak var passportLoadingView: UIActivityIndicatorView!
    @IBOutlet weak var visaLoadingView: UIActivityIndicatorView!
    @IBOutlet weak var airlineLoadingView: UIActivityIndicatorView!
    @IBOutlet weak var cnicLoadingView: UIActivityIndicatorView!

    var cnicImage: UIImage?
    var visaImage: UIImage?
    var airlineImage: UIImage?
    var passportImage: UIImage?
    
    var stampsDropDown: DropDown!
    
    var visaDetails: OkToBoardVisaModel?
    var arrStamps = Array<OkToBoardStampModel>()
    var selectedStamp: OkToBoardStampModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.updateUI()
        self.setStampsDropDown()
        
        self.setData()
        
        self.getOkToBoardStamps()
    }
    
    private func updateUI() {
        // Setup textfield right view
        if let image = UIImage(named: "ic_dropdown") {
            self.countryTextField.addPaddingRightIcon(image, padding: 8.0)
        }
        
        if let image = UIImage(named: "ic_dropdown") {
            self.stampTextField.addPaddingRightIcon(image, padding: 8.0)
        }
        
        // Setup datepicker UI
        if let image = UIImage(named: "icon_calendar") {
            self.dateOfEntryTextField.addPaddingRightIcon(image, padding: 8.0)
        }
    }
    
    private func setStampsDropDown() {
        self.stampTextField.delegate = self
        
        if let image = UIImage(named: "ic_dropdown") {
            self.stampTextField.addPaddingRightIcon(image, padding: 8.0)
        }
        
        let dropdown = DropDown()
        dropdown.anchorView = self.stampTextField
        dropdown.bottomOffset = CGPoint(x: 0, y: (dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.selectionAction = { (index: Int, item: String) in
            self.stampTextField.text = item
            self.selectedStamp = self.arrStamps[index]
        }
        
        self.stampsDropDown = dropdown
    }
    
    private func reloadStampsDataSource() {
        let dataSource = self.arrStamps.map {$0.vendor ?? ""}
        self.stampsDropDown.dataSource = dataSource
    }
    
    private func setData() {
        guard self.visaDetails != nil else { return }
        
        self.fullNameTextField.text = self.visaDetails?.name
        self.countryTextField.text = self.visaDetails?.country
        self.dateOfEntryTextField.text = self.visaDetails?.dateOfEntry
        
        self.passportImageView.loadImage(self.visaDetails?.passport, loadingView: passportLoadingView) { (image) in
            self.passportImage = image
        }
        
        self.visaImageView.loadImage(self.visaDetails?.visa, loadingView: visaLoadingView) { (image) in
            self.visaImage = image
        }
        
        self.cnicImageView.loadImage(self.visaDetails?.cnic, loadingView: cnicLoadingView) { (image) in
            self.cnicImage = image
        }
        
        self.airlineImageView.loadImage(self.visaDetails?.ticket, loadingView: airlineLoadingView) { (image) in
            self.airlineImage = image
        }
    }
    
    @IBAction func onBtnSubmit(_ sender: AnyObject) {
        guard self.selectedStamp != nil else {
            Utility.showAlert(title: "Message", message: "Stamp details not provided")
            return
        }
        
        guard self.cnicImageSwitch.isOn && self.passportImageSwitch.isOn
            && self.airlineImageSwitch.isOn && self.visaImageSwitch.isOn else {
            Utility.showAlert(title: "Message", message: "Fields are not checked")
            return
        }        
        
        self.approveOkToBoardVisa()
    }
    
    // MARK: - Web API Methods
    
    private func getOkToBoardStamps() {
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getOkToBoardStamps(params: [:], success: { (responseArray) in
            Utility.hideLoader()
            self.arrStamps = Mapper<OkToBoardStampModel>().mapArray(JSONArray: responseArray as! [[String: Any]])
            self.reloadStampsDataSource()
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    private func approveOkToBoardVisa() {
        Utility.showLoader()
        
        let queryParams: [String: Any] = [
            "okToBoardId": self.visaDetails?.id ?? ""
        ]
        
        let params: [String: Any] = [
            "stamp_id": self.selectedStamp?.id ?? ""
        ]
		
        APIManager.sharedInstance.usersAPIManager.approveOkToBoardVisa(params: params, queryParams: queryParams, success: { (response) in
            Utility.hideLoader()
            Utility.showAlert(title: "Message", message: "Ok-To-Board visa status updated successfully", closure: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OkToBoardVisaDetailsViewController: UITextFieldDelegate {
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.stampTextField {
            self.stampsDropDown.show()
            return false
        }
        
        return true
    }
}

