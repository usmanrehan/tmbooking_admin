//
//  OkToBoardVisaListViewConttroller.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 10/08/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class OkToBoardVisaListViewConttroller: BaseViewController {
    @IBOutlet weak var noRecordFoundLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var arrVisas: [OkToBoardVisaModel]?

    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getOkToBoardVisas()
    }
    
    private func setupRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
        
        self.refreshControl = refreshControl
        self.tableView.addSubview(self.refreshControl)
    }
    
    private func onSelectVisaItem(_ visaDetails: OkToBoardVisaModel) {
        self.showVisaDetails(visaDetails)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getOkToBoardVisas()
        self.refreshControl.endRefreshing()
    }

    private func getOkToBoardVisas() {
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getOkToBoardVisas(params: [:], success: { (responseArray) in
            Utility.hideLoader()
            self.arrVisas = Mapper<OkToBoardVisaModel>().mapArray(JSONArray: responseArray as! [[String: Any]])
            self.tableView.reloadData()
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    private func showVisaDetails(_ visaDetails: OkToBoardVisaModel) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "OkToBoardVisaDetailsViewController")
            as! OkToBoardVisaDetailsViewController
        
        controller.visaDetails = visaDetails
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension OkToBoardVisaListViewConttroller: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.arrVisas?.count ?? 0
        self.noRecordFoundLabel.isHidden = count == 0 ? false: true
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OkToBoardCellIdentifier", for: indexPath)
            as! OkToBoardVisaCell
        cell.setData(self.arrVisas![indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let visaDetails = self.arrVisas![indexPath.row]
        DispatchQueue.main.async {
            self.onSelectVisaItem(visaDetails)
        }
    }
}

