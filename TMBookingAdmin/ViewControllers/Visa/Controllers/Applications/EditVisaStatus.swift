//
//  EditVisaStatus.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 23/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import DropDown
import ObjectMapper
import SDWebImage
import SimpleImageViewer

class EditVisaStatus: BaseViewController {
    
    @IBOutlet weak var imgVisa: UIImageView!
    @IBOutlet weak var okToBoardImageView: UIImageView!
    @IBOutlet weak var tfStatus: UITextField!
	@IBOutlet weak var visaExpiryTextField: UITextField!
    @IBOutlet weak var tvComment: UITextView!
	
	var activeField: UITextField?
    
    var memberDetailModel = MemberDetailModel()
    var visaModel: SingleVisaModel?    
    var arrStatus = [VisaStatus]()
    var arrStatusString = [String]()
    var selectedVisaStatus: VisaStatus?
    let dropDown = DropDown()
    var imagePicker: UIImagePickerController!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
		self.setDatePicker(visaExpiryTextField)
		
        self.getStatus()
        self.getVisaStatus()
    }
	
	private func setDatePicker(_ textField: UITextField) {
		textField.delegate = self
		
		if let image = UIImage(named: "icon_calendar") {
			textField.addPaddingRightIcon(image, padding: 8.0)
		}
		
		let picker = UIDatePicker()
		picker.datePickerMode = .date
		picker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
		
		textField.inputView = picker
	}
	
	@objc private func handleDatePicker(sender: UIDatePicker) {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd"
		
		if let textField = self.activeField {
			textField.text = dateFormatter.string(from: sender.date)
		}
	}

    @IBAction func onBtnAttachVisa(_ sender: UIButton) {
        self.uploadImage()
    }
    
    @IBAction func onBtnSave(_ sender: UIButton) {
        self.validateAndSave()
    }
    
    @IBAction func imageTapped(_ gesture: UIGestureRecognizer) {
        if let imageView = gesture.view as? UIImageView {
            guard imageView.image != nil else {
                return
            }
            
            // Show full screen image view
            let configuration = ImageViewerConfiguration { config in
                config.imageView = imageView
            }
            
            let imageViewerController = ImageViewerController(configuration: configuration)
            
            self.present(imageViewerController, animated: true)
        }
    }

}
//MARK:- Helper Methods
extension EditVisaStatus{
    private func setUI(){
        self.tfStatus.addPaddingRightIcon(UIImage(named: "ic_dropdown")!, padding: 8.0)

        self.dropDown.anchorView = self.tfStatus
        self.dropDown.frame = self.tfStatus.frame
        self.dropDown.dataSource = self.arrStatusString
        self.dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfStatus.text = item
            self.selectedVisaStatus = self.arrStatus[index]
        }
    }
    
    private func validateAndSave() {
        guard self.selectedVisaStatus != nil else {
            Utility.showAlert(title: "Error", message: "Visa status is not selected")
            return
        }
        
        // Visa approved case
        if let status = self.selectedVisaStatus?.status, status.lowercased() == "approved" {
            guard self.imgVisa.image != nil else {
                Utility.showAlert(title: "Error", message: "Visa image is not attached")
                return
            }
        }
        
        self.saveVisaStatus()
    }
    
    private func setData(data:VisaStatus) {
        self.tfStatus.text = data.status
        self.tvComment.text = data.reason
        
        if let visaImageURL = URL(string: data.image ?? ""){
            self.imgVisa.sd_setImage(with: visaImageURL, completed: nil)
        }
        
        if let imageURL = URL(string: data.okToBoardImage ?? "") {
            self.okToBoardImageView.sd_setImage(with: imageURL, completed: nil)
        }
        
        self.selectedVisaStatus = data
    }
}
//MARK:- Services
extension EditVisaStatus{
    private func getStatus(){
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getStatus(params: [:], success: { (response) in
            Utility.hideLoader()
            guard let responseArray = response as? [[String:Any]] else {return}
            self.arrStatus = Mapper<VisaStatus>().mapArray(JSONArray: responseArray)
            for item in self.arrStatus{
                let status = item.status ?? "-"
                self.arrStatusString.append(status)
            }
            self.setUI()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func saveVisaStatus(){
        var params: [String:Any] = ["formid": self.visaModel?.formid ?? "0",
                                    "status": self.selectedVisaStatus?.id ?? "0",
									"visa_expiry_date": self.visaExpiryTextField.text ?? "",
                                    "reason": self.tvComment.text ?? ""]
        
        if let image = self.imgVisa.image {
            let data = UIImageJPEGRepresentation(image, 0.7)
            params["image"] = data
        }
        
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.UpdateVisaStatus(params: params, success: { (responseObject) in
            Utility.hideLoader()
            Utility.showAlert(title: "Alert", message: "Visa status update successfully", closure: { (Ok) in
                self.navigationController?.popViewController(animated: true)
                NotificationCenter.default.post(name: NSNotification.Name.visaUpdateStatus, object: nil)
            })
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func getVisaStatus(){
        let formid = self.visaModel?.formid ?? ""
        let params: [String:Any] = ["formid":formid]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.GetVisatStatus(params: params, success: { (responseObject) in
            Utility.hideLoader()
            print(responseObject)
            let visaStatus = Mapper<VisaStatus>().map(JSON: responseObject) ?? VisaStatus()
            self.selectedVisaStatus = visaStatus
            self.setData(data:visaStatus)
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
//MARK:- Image picker
extension EditVisaStatus{
    func uploadFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func uploadFromGallery(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
        
    }
    
    func uploadImage(){
        let alert = UIAlertController(title: "Upload Photo" , message: "How do you want to set your photo?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
            self.uploadFromCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
            self.uploadFromGallery()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
//MARK: - UIImagePickerControllerDelegate
extension EditVisaStatus: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imgVisa.image = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension EditVisaStatus: UITextFieldDelegate {
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
		self.activeField = textField

        if textField == self.tfStatus {
            self.dropDown.show()
            return false
        }
        
        return true
    }
}
