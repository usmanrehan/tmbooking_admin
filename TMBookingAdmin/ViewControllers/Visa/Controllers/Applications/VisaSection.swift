//
//  VisaSection.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 22/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

enum VisaApplicationType{
    case group
    case individual
}

class VisaSection: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var groupButton: UIButton!
    @IBOutlet weak var individualButton: UIButton!
    @IBOutlet weak var noRecordFoundLabel: UILabel!
    @IBOutlet weak var clearFilterButton: UIButton! {
        didSet {
            clearFilterButton.isHidden = true
        }
    }
    
    var visaApplicationType: VisaApplicationType = .individual
    
    var arrVisas = [SingleVisaModel]()
    var arrGroupVisas = [SingleVisaModel]()
    var arrSingleVisas = [SingleVisaModel]()
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupRefreshControl()
        self.setupRightFilterBarButtonItem()
        self.registerNotificationObservers()
        self.tableView.addSubview(self.refreshControl)
        
        self.getAllVisas()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func setupRefreshControl() {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.darkGray
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
        
        self.refreshControl = refreshControl
    }
    
    private func setupRightFilterBarButtonItem() {
        let icon = UIImage(named: "icon_filter")
        let button = UIButton()
        button.setImage(icon, for: .normal)
        button.addTarget(self, action: #selector(onBtnFilterItem), for: UIControlEvents.touchUpInside)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    private func registerNotificationObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onUpdateVisaStatus(_:)),
                                               name: NSNotification.Name.visaUpdateStatus,
                                               object: nil)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getAllVisas()
        self.refreshControl.endRefreshing()
    }
    
    @objc func onUpdateVisaStatus(_ notification: Notification) {
        self.getAllVisas()
    }
    
    @objc func onBtnFilterItem() {
        self.performSegue(withIdentifier: "showVisaFilterSegue", sender: self)
    }
    
    @IBAction func onBtnChangeType(_ sender: UIButton) {
        if sender == self.individualButton {
            self.visaApplicationType = .individual
            
            self.individualButton.isSelected = true
            self.groupButton.isSelected = false
            self.individualButton.backgroundColor = Global.APP_COLOR
            self.groupButton.backgroundColor = .white
        } else {
            self.visaApplicationType = .group
            
            self.groupButton.isSelected = true
            self.individualButton.isSelected = false
            self.groupButton.backgroundColor = Global.APP_COLOR
            self.individualButton.backgroundColor = .white
        }
        
        self.tableView.reloadData()
    }
    
    @IBAction func onBtnClearFilters(_ sender: UIButton) {
        self.clearFilterButton.isHidden = true
        self.getAllVisas()
    }
    
    private func getAllVisas() {
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getAllVisas(params: [:], success: { (response) in
            Utility.hideLoader()
            guard let responseArray = response as? [[String:Any]] else { return }
            self.clearFilterButton.isHidden = true
            self.arrVisas = Mapper<SingleVisaModel>().mapArray(JSONArray: responseArray)
            self.arrGroupVisas = self.arrVisas.filter {$0.applicationType == "group"}
            self.arrSingleVisas = self.arrVisas.filter {$0.applicationType == "individual"}
            self.tableView.reloadData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func getAllVisasWithFilter(_ filterRequest: VisaFilterRequestModel) {
        Utility.showLoader()
        let params: [String: Any] = [
            "fromDate": filterRequest.fromDate ?? "",
            "toDate": filterRequest.toDate ?? "",
            "orderNo": filterRequest.orderNo ?? "",
            "passportNo": filterRequest.passport ?? "",
            "country": filterRequest.country ?? "",
            "paymentStatus": filterRequest.paymentStatus ?? "",
            "visaStatus": filterRequest.visaStatus ?? ""
        ]
        APIManager.sharedInstance.usersAPIManager.getAllVisas(params: params, success: { (response) in
            Utility.hideLoader()
            guard let responseArray = response as? [[String:Any]] else { return }
            self.arrVisas = Mapper<SingleVisaModel>().mapArray(JSONArray: responseArray)
            self.arrGroupVisas = self.arrVisas.filter {$0.applicationType == "group"}
            self.arrSingleVisas = self.arrVisas.filter {$0.applicationType == "individual"}
            self.tableView.reloadData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVisaFilterSegue" {
            let controller = segue.destination as! VisaFilterViewController
            controller.delegate = self
        }
     }
}

extension VisaSection{
    private func navigateToVisaDetail(_ visaModel: SingleVisaModel) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "VisaDetail") as? VisaDetail{
            controller.groupVisaModel = visaModel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func navigateToVisaMemberDetail(_ visaModel: SingleVisaModel) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "VisaMemberViewController") as? VisaMemberViewController {
            controller.visaModel = visaModel
            controller.memberId = visaModel.memberid
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension VisaSection:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.visaApplicationType {
        case .group:
            self.noRecordFoundLabel.isHidden = (self.arrGroupVisas.count > 0)
            return self.arrGroupVisas.count
        case .individual:
            self.noRecordFoundLabel.isHidden = (self.arrSingleVisas.count > 0)
            return self.arrSingleVisas.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VisaSectionTVC", for: indexPath) as! VisaSectionTVC
        switch self.visaApplicationType {
        case .group:
            cell.setData(data: self.arrGroupVisas[indexPath.row],visaApplicationType:.group)
        case .individual:
            cell.setData(data: self.arrSingleVisas[indexPath.row],visaApplicationType:.individual)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch self.visaApplicationType {
        case .group:
            let visaModel = self.arrGroupVisas[indexPath.row]
            self.navigateToVisaDetail(visaModel)
        case .individual:
            let visaModel = self.arrSingleVisas[indexPath.row]
            self.navigateToVisaMemberDetail(visaModel)
        }
    }
}

extension VisaSection: VisaFilterViewDelegate {
    func didSearchWithFilterRequest(_ filterRequest: VisaFilterRequestModel) {
        self.clearFilterButton.isHidden = false
        self.getAllVisasWithFilter(filterRequest)
    }
}
