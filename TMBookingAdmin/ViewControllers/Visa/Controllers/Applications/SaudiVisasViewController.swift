//
//  SaudiVisasViewController.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 13/07/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class SaudiVisasViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noRecordFoundLabel: UILabel!
    
    var arrVisas = Array<SingleVisaModel>()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAllSaudiVisas()
    }
    
    private func getAllSaudiVisas() {
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getAllSaudiVisas(params: [:], success: { (response) in
            Utility.hideLoader()
            guard let responseArray = response as? [[String:Any]] else { return }
            self.arrVisas = Mapper<SingleVisaModel>().mapArray(JSONArray: responseArray)
            self.tableView.reloadData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }

    // MARK: - Navigation
    
    private func showSaudiVisaDetails(_ indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SaudiVisaDetailsViewController")
            as! SaudiVisaDetailsViewController
        
        controller.visaDetails = self.arrVisas[indexPath.row]
        controller.memberId = self.arrVisas[indexPath.row].memberid
        
        self.navigationController?.pushViewController(controller, animated: true)
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
}

extension SaudiVisasViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.noRecordFoundLabel.isHidden = (self.arrVisas.count > 0)
        return self.arrVisas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VisaSectionTVC", for: indexPath) as! VisaSectionTVC
        cell.setData(data: self.arrVisas[indexPath.row], visaApplicationType:.individual)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showSaudiVisaDetails(indexPath)
    }
}

