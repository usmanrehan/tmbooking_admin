//
//  VisaDetail.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 23/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class VisaDetail: BaseViewController {
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var visaTypeLabel: UILabel!
    @IBOutlet weak var paymentStatusLabel: UILabel!
    @IBOutlet weak var visaStatusLabel: UILabel!
    @IBOutlet weak var dateOfEntryLabel: UILabel!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView()
        }
    }

    var groupVisaModel: SingleVisaModel?
    var arrGroupMembers = [SingleVisaModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.registerNotificationObservers()
        
        setData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func registerNotificationObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onUpdateVisaStatus(_:)),
                                               name: NSNotification.Name.visaUpdateStatus,
                                               object: nil)
    }
    
    @objc private func onUpdateVisaStatus(_ notification: Notification) {
        setData()
    }
}

extension VisaDetail{
    private func navigateToVisaMemberDetail(_ memberId: String?, visaModel: SingleVisaModel) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "VisaMemberViewController") as? VisaMemberViewController {
            controller.memberId = memberId
            controller.visaModel = visaModel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func setData(){
        if let visaModel = self.groupVisaModel {
            self.orderIdLabel.text = visaModel.orderid
            self.countryLabel.text = visaModel.country
            self.visaTypeLabel.text = visaModel.visatype
            self.paymentStatusLabel.text = visaModel.paymentStatus
            self.visaStatusLabel.text = visaModel.visaStatus
            self.dateOfEntryLabel.text = visaModel.date
            
            self.getGroupMembers(visaModel.applicationId ?? "0")
        }
    }
}

extension VisaDetail:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrGroupMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VisaMembers", for: indexPath) as! VisaMembers
        cell.setData(data: self.arrGroupMembers[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
}

extension VisaDetail:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let visaModel = self.arrGroupMembers[indexPath.row]
        self.navigateToVisaMemberDetail(visaModel.memberid, visaModel: visaModel)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Group Members"
    }
}

extension VisaDetail{
    private func getGroupMembers(_ applicationId: String) {
        let param: [String:Any] = [
            "applicationid": applicationId
        ]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getGroupMembers(params: param, success: { (response) in
            Utility.hideLoader()
            guard let responseArray = response as? [[String:Any]] else {return}
            self.arrGroupMembers = Mapper<SingleVisaModel>().mapArray(JSONArray: responseArray)
            self.tableView.reloadData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
