//
//  AddVisaMemberViewController.swift
//  Template
//
//  Created by Akber Sayni on 13/01/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import DropDown
import SimpleImageViewer
import MobileCoreServices
import ObjectMapper
import SwiftyUserDefaults

class VisaMemberViewController: BaseViewController {
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var maritalStatusTextField: UITextField!
    @IBOutlet weak var dateOfBirthTextField: UITextField!
    @IBOutlet weak var countryOfBirthTextField: UITextField!
    @IBOutlet weak var cnicTextField: UITextField!
    @IBOutlet weak var passportTextField: UITextField!
    @IBOutlet weak var aqamaTextField: UITextField!
    @IBOutlet weak var socialSecurityTextField: UITextField!
    @IBOutlet weak var residentTextField: UITextField!
    @IBOutlet weak var passportExpiryDateTextField: UITextField!
    @IBOutlet weak var passportIssueDateTextField: UITextField!
    @IBOutlet weak var passportIssueCountryTextField: UITextField!
    @IBOutlet weak var passportIssueCityTextField: UITextField!
    @IBOutlet weak var healthStatusTextField: UITextField!
    @IBOutlet weak var educationTextField: UITextField!
    @IBOutlet weak var occupationTextField: UITextField!
    
    @IBOutlet weak var passportImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var cnicImageView: UIImageView!
    @IBOutlet weak var pdfImageView: UIImageView!
    
    @IBOutlet weak var passportImageContainerView: UIView!
    @IBOutlet weak var profileImageContainerView: UIView!
    @IBOutlet weak var cnicImageContainerView: UIView!
    @IBOutlet weak var bankStatementContainerView: UIView!
    
    var countryId: String?
    
    var member: MemberDetailModel?
    var visaModel: SingleVisaModel?
    var memberId: String?

    var arrMemberFields: [VisaMemberFieldsModel]?
    
    let arrGenders = ["Male", "Female", "Others"]
    let arrMaritalStatus = ["Single", "Married", "Others"]
    let arrHealthStatus = ["Healthy", "Not healthy"]
    let arrEducations = ["Associate degree", "Bachelor's degree", "Master's degree", "Doctoral degree"]
    
    var genderDropDown: DropDown!
    var maritalDropDown: DropDown!
    var healthDropDown: DropDown!
    var educationDropDown: DropDown!
    
    var passportImage: UIImage?
    var profileImage: UIImage?
    var cnicImage: UIImage?
    var pdfURL: URL?
    
    var activeField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.updateUI()
        
        if let countryId = self.visaModel?.countryId {
            self.getVisaFields(countryId)
        }
        
        if let memberId = self.memberId {
            self.getMember(memberid: memberId)
        }
    }
    
    private func updateUI() {
        // Setup textfield right view
        self.countryOfBirthTextField.addPaddingRightIcon(UIImage(named: "ic_dropdown")!, padding: 8.0)
        self.residentTextField.addPaddingRightIcon(UIImage(named: "ic_dropdown")!, padding: 8.0)
        self.passportIssueCountryTextField.addPaddingRightIcon(UIImage(named: "ic_dropdown")!, padding: 8.0)

        // Setup dropdown UI
        self.genderDropDown = getDropDown(withDataSource: self.arrGenders, textField: self.genderTextField)
        self.maritalDropDown = getDropDown(withDataSource: self.arrMaritalStatus, textField: self.maritalStatusTextField)
        self.healthDropDown = getDropDown(withDataSource: self.arrHealthStatus, textField: self.healthStatusTextField)
        self.educationDropDown = getDropDown(withDataSource: self.arrEducations, textField: self.educationTextField)
        
        // Setup datepicker UI
        self.setDatePicker(self.dateOfBirthTextField)
        self.setDatePicker(self.passportExpiryDateTextField)
        self.setDatePicker(self.passportIssueDateTextField)
    }
    
    private func setData() {
        guard self.member != nil else {
            return
        }
        
        self.fullNameTextField.text = self.member?.fullname
        self.lastNameTextField.text = self.member?.sirname
        self.mobileTextField.text = self.member?.mobile
        self.emailTextField.text = self.member?.email
        self.genderTextField.text = self.member?.gender
        self.maritalStatusTextField.text = self.member?.martialstatus
        self.dateOfBirthTextField.text = self.member?.dob
        self.countryOfBirthTextField.text = self.member?.countryofbirth
        self.cnicTextField.text = self.member?.cnic
        self.passportTextField.text = self.member?.passportNo
        self.aqamaTextField.text = self.member?.aqama
        self.socialSecurityTextField.text = self.member?.socialsecuritynumber
        self.residentTextField.text = self.member?.resident
        self.passportExpiryDateTextField.text = self.member?.passportexpiry
        self.passportIssueDateTextField.text = self.member?.passportdateofissue
        self.passportIssueCountryTextField.text = self.member?.issuecountry
        self.passportIssueCityTextField.text = self.member?.issuecity
        self.healthStatusTextField.text = self.member?.healthstatus
        self.educationTextField.text = self.member?.education
        self.occupationTextField.text = self.member?.occupation

        // Set imageviews
        self.profileImageView.loadImage(withURL: self.member?.photo)
        self.cnicImageView.loadImage(withURL: self.member?.cnicphoto)
        if let passportImages = self.member?.passport, passportImages.count > 0 {
            self.passportImageView.loadImage(withURL: passportImages.first)
        }
    }
    
    private func getDropDown(withDataSource datasource: [String], textField: UITextField) -> DropDown {
        textField.delegate = self
        
        if let image = UIImage(named: "ic_dropdown") {
            textField.addPaddingRightIcon(image, padding: 8.0)
        }
        
        let dropdown = DropDown()
        dropdown.dataSource = datasource
        dropdown.anchorView = textField
        dropdown.bottomOffset = CGPoint(x: 0, y: (dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.selectionAction = { (index: Int, item: String) in
            textField.text = item
        }
        
        return dropdown
    }
    
    private func setDatePicker(_ textField: UITextField) {
        textField.delegate = self
        
        if let image = UIImage(named: "icon_calendar") {
            textField.addPaddingRightIcon(image, padding: 8.0)
        }

        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        
        textField.inputView = picker
    }
    
    private func showCountryPicker(_ sender: UITextField) {
        let picker = MICountryPicker()
        picker.showCallingCodes = false
        picker.didSelectCountryClosure = { name, code in
            // Dismiss country picker
            self.dismiss(animated: true, completion: {
                sender.text = name
            })
        }
        
        self.present(BaseNavigationController(rootViewController: picker), animated: true, completion: nil)
    }
    
    private func openPDFDocument(_ url: URL) {
        let documentController = UIDocumentInteractionController(url: url)
        documentController.delegate = self
        documentController.presentPreview(animated: true)
    }
    
    private func enableTextFieldUserInteraction() {
        self.fullNameTextField.isUserInteractionEnabled = true
        self.lastNameTextField.isUserInteractionEnabled = true
        self.mobileTextField.isUserInteractionEnabled = true
        self.emailTextField.isUserInteractionEnabled = true
        self.genderTextField.isUserInteractionEnabled = true
        self.maritalStatusTextField.isUserInteractionEnabled = true
        self.dateOfBirthTextField.isUserInteractionEnabled = true
        self.countryOfBirthTextField.isUserInteractionEnabled = true
        self.cnicTextField.isUserInteractionEnabled = true
        self.passportTextField.isUserInteractionEnabled = true
        self.aqamaTextField.isUserInteractionEnabled = true
        self.socialSecurityTextField.isUserInteractionEnabled = true
        self.residentTextField.isUserInteractionEnabled = true
        self.passportExpiryDateTextField.isUserInteractionEnabled = true
        self.passportIssueDateTextField.isUserInteractionEnabled = true
        self.passportIssueCountryTextField.isUserInteractionEnabled = true
        self.passportIssueCityTextField.isUserInteractionEnabled = true
        self.healthStatusTextField.isUserInteractionEnabled = true
        self.educationTextField.isUserInteractionEnabled = true
        self.occupationTextField.isUserInteractionEnabled = true
    }
   
    private func updateMemberFields() {
        guard self.arrMemberFields != nil else { return }
        
        for item in self.arrMemberFields! {
            switch item.identifier {
            case "aqama":
                self.aqamaTextField.isHidden = !(item.show == "1")
                break
            case "bankstatement":
                self.bankStatementContainerView.isHidden = !(item.show == "1")
                break
            case "cnic":
                self.cnicTextField.isHidden = !(item.show == "1")
                break
            case "cnicphoto":
                self.cnicImageContainerView.isHidden = !(item.show == "1")
                break
            case "countryofbirth":
                self.countryOfBirthTextField.isHidden = !(item.show == "1")
                break
            case "dob":
                self.dateOfBirthTextField.isHidden = !(item.show == "1")
                break
            case "education":
                self.educationTextField.isHidden = !(item.show == "1")
                break
            case "email":
                self.emailTextField.isHidden = !(item.show == "1")
                break
            case "fullname":
                self.fullNameTextField.isHidden = !(item.show == "1")
                break
            case "gender":
                self.genderTextField.isHidden = !(item.show == "1")
                break
            case "healthstatus":
                self.healthStatusTextField.isHidden = !(item.show == "1")
                break
            case "issuecity":
                self.passportIssueCityTextField.isHidden = !(item.show == "1")
                break
            case "issuecountry":
                self.passportIssueCountryTextField.isHidden = !(item.show == "1")
                break
            case "livingin":
                self.residentTextField.isHidden = !(item.show == "1")
                break
            case "martialstatus":
                self.maritalStatusTextField.isHidden = !(item.show == "1")
                break
            case "mobile":
                self.mobileTextField.isHidden = !(item.show == "1")
                break
            case "occupation":
                self.occupationTextField.isHidden = !(item.show == "1")
                break
            case "passportdateofissue":
                self.passportIssueDateTextField.isHidden = !(item.show == "1")
                break
            case "passportexpiry":
                self.passportExpiryDateTextField.isHidden = !(item.show == "1")
                break
            case "photo":
                self.profileImageContainerView.isHidden = !(item.show == "1")
                break
            case "referalcode":
                self.residentTextField.isHidden = !(item.show == "1")
                break
            case "resident":
                self.residentTextField.isHidden = !(item.show == "1")
                break
            case "sirname":
                self.lastNameTextField.isHidden = !(item.show == "1")
                break
            case "socialsecuritynumber":
                self.socialSecurityTextField.isHidden = !(item.show == "1")
                break
            default:
                break
            }
        }
    }
    
    private func openFullScreenImageView(_ imageView: UIImageView) {
        // Show full screen image view
        let configuration = ImageViewerConfiguration { config in
            config.imageView = imageView
        }
        
        let imageViewerController = ImageViewerController(configuration: configuration)
        
        self.present(imageViewerController, animated: true)
    }
    
    private func shareImage(_ image: UIImage) {
        // set up activity view controller
        let imageToShare = [image]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.postToFacebook]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    private func showImageOptionsAlertController(_ imageView: UIImageView) {
        let controller = UIAlertController(title: nil, message: "What would you like to do?", preferredStyle: .actionSheet)
        
        controller.addAction(UIAlertAction(title: "Open Image", style: .default, handler: { (action) in
            self.openFullScreenImageView(imageView)
        }))
        
        controller.addAction(UIAlertAction(title: "Save Image", style: .default, handler: { (action) in
            self.shareImage(imageView.image!)
        }))
        
        controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(controller, animated: true, completion: nil)
    }
    
    private func showEmailTextFieldAlertPopup() {
        let alertController = UIAlertController(title: "Email Visa Copy", message: "Enter email address, where to send visa copy", preferredStyle: .alert)
        let sendAction = UIAlertAction(title: "Send", style: .default) { (action) in
            if let textField = alertController.textFields?.first {
                let email = textField.text ?? ""
                if email.isValidEmail() {
                    self.sendVisaEmail(email)
                } else {
                    Utility.showAlert(title: "Error", message: "Email address is not valid")
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter email address"
        }
        
        alertController.addAction(sendAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func checkOrderPaymmentStatus() -> Bool {
        if let status = self.visaModel?.paymentStatus, !status.isEmpty, status.lowercased() == "confirmed" {
            return true
        }
        
        return false
    }
    
    @objc private func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if let textField = self.activeField {
            textField.text = dateFormatter.string(from: sender.date)
        }
    }
    
    // MARK: - Navigation
    
    private func navigateToEditVisaStatus() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "EditVisaStatus") as? EditVisaStatus {
            if let memberDetails = self.member {
                controller.memberDetailModel = memberDetails
                controller.visaModel = self.visaModel
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
}

// MARK: - IBAction Methods
extension VisaMemberViewController {
    @IBAction func onBtnEditApplication(_ sender: UIButton) {
        guard self.checkOrderPaymmentStatus() else {
            Utility.showAlert(title: "Message", message: "Payment status is not confirmed")
            return
        }
        
        if sender.isSelected {
            // Update member details
            self.updateVisaMemberDetail()
        } else {
            // Allow editing member details
            sender.isSelected = true
            self.enableTextFieldUserInteraction()
        }
    }
    
    @IBAction func onBtnChangeStatus(_ sender: UIButton) {
        guard self.checkOrderPaymmentStatus() else {
            Utility.showAlert(title: "Message", message: "Payment status is not confirmed")
            return
        }

        self.navigateToEditVisaStatus()
    }
    
    @IBAction func onBtnSendEmailVisa(_ sender: UIButton) {
        guard self.checkOrderPaymmentStatus() else {
            Utility.showAlert(title: "Message", message: "Payment status is not confirmed")
            return
        }

        if let status = self.visaModel?.visaStatus, !status.isEmpty, status.lowercased() == "approved" {
            self.showEmailTextFieldAlertPopup()
        } else {
            Utility.showAlert(title: "Message", message: "Visa status is not approved")
        }
    }
    
    @IBAction func onBtnOkToBoard(_ sender: UIButton) {
        guard self.checkOrderPaymmentStatus() else {
            Utility.showAlert(title: "Message", message: "Payment status is not confirmed")
            return
        }
        
        if let status = self.visaModel?.visaStatus, !status.isEmpty, status.lowercased() == "approved" {
            self.getOkToBoardStamp()
        } else {
            Utility.showAlert(title: "Message", message: "Visa status is not approved")
        }
    }
    
    @IBAction func imageTapped(_ gesture: UIGestureRecognizer) {
        if let imageView = gesture.view as? UIImageView {
            guard imageView.image != nil else {
                return
            }
            
            self.showImageOptionsAlertController(imageView)
        }
    }
    
    @IBAction func pdfImageTapped(_ gesture: UIGestureRecognizer) {
        guard self.pdfURL != nil else {
            Utility.showAlert(title: "Error", message: "No file found")
            return
        }
        
        self.openPDFDocument(self.pdfURL!)
    }
}

// MARK: - API Methods
extension VisaMemberViewController {
    private func getMember(memberid:String) {
        let param: [String:Any] = ["memberid":memberid]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.GetMember(params: param, success: { (responseArray) in
            Utility.hideLoader()
            if !responseArray.isEmpty{
                if let memberDetail = Mapper<MemberDetailModel>().map(JSONObject: responseArray.first) {
                    self.member = memberDetail
                    self.setData()
                }
            }
            else{
                Utility.showAlert(title: "Error", message: "No record found.")
            }
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func updateVisaMemberDetail() {
        let userId = Defaults[.loginUserId]
        let params: [String: Any] = [
            "userid": userId,
            "memberid": self.member?.id ?? "0",
            "fullname": self.fullNameTextField.text ?? "",
            "mobile": self.mobileTextField.text ?? "",
            "email": self.emailTextField.text ?? "",
            "sirname": self.lastNameTextField.text ?? "",
            "gender": self.genderTextField.text ?? "",
            "martialstatus": self.maritalStatusTextField.text ?? "",
            "dob": self.dateOfBirthTextField.text ?? "",
            "countryofbirth": self.countryOfBirthTextField.text ?? "",
            "cnic": self.cnicTextField.text ?? "",
            "passport_no": self.passportTextField.text ?? "",
            "aqama": self.aqamaTextField.text ?? "",
            "socialsecuritynumber": self.socialSecurityTextField.text ?? "",
            "resident": self.residentTextField.text ?? "",
            "passportexpiry": self.passportExpiryDateTextField.text ?? "",
            "passportdateofissue": self.passportIssueDateTextField.text ?? "",
            "issuecountry": self.passportIssueCountryTextField.text ?? "",
            "issuecity": self.passportIssueCityTextField.text ?? "",
            "healthstatus": self.healthStatusTextField.text ?? "",
            "education": self.educationTextField.text ?? "",
            "occupation": self.occupationTextField.text ?? "",
            ]
        Utility.showLoader()        
        APIManager.sharedInstance.usersAPIManager.UpdateVisaMemberDetail(params: params, success: { (responseObject) in
            Utility.hideLoader()
            Utility.showAlert(title: "Message", message: "Member profiled updated")
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedFailureReason)
        }
    }
    
    private func sendVisaEmail(_ email: String) {
        let params: [String: Any] = [
            "formid": self.visaModel?.formid ?? "",
            "email": email,
			"type": "admin"
        ]
        Utility.showLoader()
        APIManager.sharedInstance.apiManagerServicesWordPress.sendVisaEmail(parameters: params, success: { (response) in
            Utility.hideLoader()
            Utility.showAlert(title: "Message", message: "Visa email sent")
            print("Email sent")
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    private func getVisaFields(_ countryId: String) {
        let params: [String: Any] = ["country_id": countryId]
        APIManager.sharedInstance.apiManagerServicesWordPress.getVisaFields(params: params, success: { (response) in
            if let data = response as? Array<Any> {
                var array = Array<VisaMemberFieldsModel>()
                for item in data {
                    let model = VisaMemberFieldsModel(JSON: item as! [String: Any])
                    array.append(model!)
                }
                self.arrMemberFields = array
                self.updateMemberFields()
            }
        }) { (error) in
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    private func getOkToBoardStamp() {
        Utility.showLoader()
        let params: [String: Any] = [
            "application_id": self.visaModel?.applicationId ?? "0"
        ]
        APIManager.sharedInstance.apiManagerServicesWordPress.getOkToBoardStamp(params: params, success: { (response) in
            Utility.hideLoader()
            Utility.showAlert(title: "Message", message: "Visa OkToBoard stamp added")
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
        
    }
}

extension VisaMemberViewController: UITextFieldDelegate {
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.activeField = textField
        
        if textField == self.genderTextField {
            self.genderDropDown.show()
            return false
        } else if textField == self.maritalStatusTextField {
            self.maritalDropDown.show()
            return false
        } else if textField == self.healthStatusTextField {
            self.healthDropDown.show()
            return false
        } else if textField == self.educationTextField {
            self.educationDropDown.show()
            return false
        } else if textField == self.countryOfBirthTextField {
            self.showCountryPicker(textField)
            return false
        } else if textField == self.residentTextField {
            self.showCountryPicker(textField)
            return false
        } else if textField == self.passportIssueCountryTextField {
            self.showCountryPicker(textField)
            return false
        } else {
            return true
        }
    }
}

extension VisaMemberViewController: UIDocumentPickerDelegate {
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        // you get from the urls parameter the urls from the files selected
        if urls.count > 0 {
            self.pdfURL = urls.first
            self.pdfImageView.isHidden = false
        }
    }
    
    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}

extension VisaMemberViewController: UIDocumentInteractionControllerDelegate {
    public func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        //or use return self.navigationController for fetching app navigation bar colour
        if let navigationController = self.navigationController {
            return navigationController
        } else {
            return self
        }
    }
}
