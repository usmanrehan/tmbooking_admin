//
//  SaudiVisaDetailsViewController.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 13/07/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import DropDown
import ObjectMapper
import SimpleImageViewer

class SaudiVisaDetailsViewController: BaseViewController {
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var passportTextField: UITextField!
    @IBOutlet weak var statusTextField: UITextField!
    @IBOutlet weak var passportImageView: UIImageView!

    private var dropDown: DropDown!

    private let arrStatus = ["Unknown", "Yes", "No"]
    private var selectedStatus: String?

    var memberId: String?
    var visaDetails: SingleVisaModel?
    var memberDetails: MemberDetailModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setDropDown(statusTextField)
        self.setSaudiVisaFeeStatus()
        self.getMemberDetails()
    }
    
    private func setDropDown(_ textField: UITextField) {
        textField.delegate = self
        
        if let image = UIImage(named: "ic_dropdown") {
            textField.addPaddingRightIcon(image, padding: 8.0)
        }
        
        let dropdown = DropDown()
        dropdown.dataSource = self.arrStatus
        dropdown.anchorView = textField
        dropdown.bottomOffset = CGPoint(x: 0, y: (dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.selectionAction = { (index: Int, item: String) in
            textField.text = item
            self.selectedStatus = String(index)
        }
        
        self.dropDown = dropdown
    }
    
    private func setSaudiVisaFeeStatus() {
        if let status = self.visaDetails?.saudiFeeApplicable {
            switch status {
            case "1":
                self.statusTextField.text = "Yes"
                self.selectedStatus = status
                break
            case "2":
                self.statusTextField.text = "No"
                self.selectedStatus = status
                break
            default:
                self.statusTextField.text = "Unknown"
                self.selectedStatus = status
                break
            }
        }
    }
    
    private func setData() {
        guard self.memberDetails != nil else {
            return
        }
        
        self.fullNameTextField.text = self.memberDetails?.fullname
        self.passportTextField.text = self.memberDetails?.passportNo
        
        // Set imageviews
        if let passportImages = self.memberDetails?.passport, passportImages.count > 0 {
            self.passportImageView.loadImage(withURL: passportImages.first)
        }
    }
    
    private func openFullScreenImageView(_ imageView: UIImageView) {
        // Show full screen image view
        let configuration = ImageViewerConfiguration { config in
            config.imageView = imageView
        }
        
        let imageViewerController = ImageViewerController(configuration: configuration)
        
        self.present(imageViewerController, animated: true)
    }
    
    @IBAction func imageTapped(_ gesture: UIGestureRecognizer) {
        if let imageView = gesture.view as? UIImageView {
            guard imageView.image != nil else {
                return
            }
            
            self.openFullScreenImageView(imageView)
        }
    }
    
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        guard self.selectedStatus != nil else {
            Utility.showAlert(title: "Message", message: "Saudi visa fee status not provided")
            return
        }
        
        self.updateSaudiVisaStatus()
    }

    private func getMemberDetails() {
        Utility.showLoader()
        let param: [String:Any] = [
            "memberid": self.memberId ?? ""
        ]
        APIManager.sharedInstance.usersAPIManager.GetMember(params: param, success: { (responseArray) in
            Utility.hideLoader()
            if !responseArray.isEmpty{
                if let details = Mapper<MemberDetailModel>().map(JSONObject: responseArray.first) {
                    self.memberDetails = details
                    self.setData()
                }
            }
            else{
                Utility.showAlert(title: "Error", message: "No record found.")
            }
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func updateSaudiVisaStatus() {
        Utility.showLoader()
        let params: [String: Any] = [
            "formid": self.visaDetails?.formid ?? "",
            "saudi_fee_applicable": self.selectedStatus ?? "0"
        ]
        APIManager.sharedInstance.usersAPIManager.UpdateSaudiVisaStatus(params: params, success: { (response) in
            Utility.hideLoader()
            Utility.showAlert(title: "Message", message: "Status updated successfully", closure: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SaudiVisaDetailsViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.statusTextField {
            self.dropDown.show()
            return false
        }
        
        return true
    }
}
