//
//  VisaFilterViewController.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 19/03/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import SwiftDate
import DropDown

protocol VisaFilterViewDelegate {
    func didSearchWithFilterRequest(_ filterRequest: VisaFilterRequestModel)
}

class VisaFilterViewController: BaseViewController {
    @IBOutlet weak var fromDateTextField: UITextField!
    @IBOutlet weak var toDateTextField: UITextField!
    @IBOutlet weak var orderIdTextField: UITextField!
    @IBOutlet weak var passportTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var paymentStatusTextField: UITextField!
    @IBOutlet weak var visaStatusTextField: UITextField!
    
    var activeField: UITextField?
    var countryDropDown: DropDown!
    var paymentStatusDropDown: DropDown!
    var visaStatusDropDown: DropDown!
    
    var arrVisaCountries = [VisaCountry]()
    var arrPaymentStatus = ["Pending payment", "Processing", "On-Hold", "Cancelled", "Confirmed"]
    var arrVisaStatus = ["Pending", "Approved", "Rejected"]
    
    var filterRequest = VisaFilterRequestModel()
    
    var delegate: VisaFilterViewDelegate?    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupDatePicker(fromDateTextField)
        self.setupDatePicker(toDateTextField)
        self.setupCountryDropDown(countryTextField)
        self.setupPaymentStatusDropDown(paymentStatusTextField)
        self.setupVisaStatusDropDown(visaStatusTextField)
        
        self.getVisaCountries()
    }
    
    private func setupDatePicker(_ sender: UITextField) {
        if let image = UIImage(named: "icon_calendar") {
            sender.addPaddingRightIcon(image, padding: 8.0)
        }
        
        //let checkInDate = DateInRegion() + 0.days
        
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        //picker.maximumDate = checkInDate.date
        picker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        
        sender.inputView = picker
    }
    
    private func setupCountryDropDown(_ anchorView: UITextField) {
        if let image = UIImage(named: "ic_dropdown") {
            anchorView.addPaddingRightIcon(image, padding: 8.0)
        }
        
        let dropdown = DropDown()
        dropdown.anchorView = anchorView
        dropdown.bottomOffset = CGPoint(x: 0, y: (dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.selectionAction = { (index: Int, item: String) in
            anchorView.text = item
        }
        
        self.countryDropDown = dropdown
    }

    private func setupPaymentStatusDropDown(_ anchorView: UITextField) {
        if let image = UIImage(named: "ic_dropdown") {
            anchorView.addPaddingRightIcon(image, padding: 8.0)
        }
        
        let dropdown = DropDown()
        dropdown.anchorView = anchorView
        dropdown.dataSource = self.arrPaymentStatus
        dropdown.bottomOffset = CGPoint(x: 0, y: (dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.selectionAction = { (index: Int, item: String) in
            anchorView.text = item
        }
        
        self.paymentStatusDropDown = dropdown
    }

    private func setupVisaStatusDropDown(_ anchorView: UITextField) {
        if let image = UIImage(named: "ic_dropdown") {
            anchorView.addPaddingRightIcon(image, padding: 8.0)
        }
        
        let dropdown = DropDown()
        dropdown.anchorView = anchorView
        dropdown.dataSource = self.arrVisaStatus
        dropdown.bottomOffset = CGPoint(x: 0, y: (dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.selectionAction = { (index: Int, item: String) in
            anchorView.text = item
        }
        
        self.visaStatusDropDown = dropdown
    }

    public func setCountryDataSource(_ countries: [VisaCountry]) {
        self.arrVisaCountries = countries
        
        // Update datasource of country
        let dataSource = countries.map {$0.name ?? ""}
        self.countryDropDown.dataSource = dataSource
    }

    @objc private func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let textField = self.activeField {
            textField.text = dateFormatter.string(from: sender.date)
        }
    }
    
    @IBAction func onBtnSearch(_ sender: UIButton) {
        let filterModel = VisaFilterRequestModel()
        
        filterModel.fromDate = self.fromDateTextField.text
        filterModel.toDate = self.toDateTextField.text
        filterModel.orderNo = self.orderIdTextField.text
        filterModel.passport = self.passportTextField.text
        filterModel.country = self.countryTextField.text
        filterModel.paymentStatus = self.paymentStatusTextField.text
        filterModel.visaStatus = self.visaStatusTextField.text
        
        self.delegate?.didSearchWithFilterRequest(filterModel)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    private func getVisaCountries() {
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getVisaCountriesList(params: [:], success: { (response) in
            Utility.hideLoader()
            var countries = Array<VisaCountry>()
            for item in response {
                let model = VisaCountry(JSON: item as! [String: Any])
                countries.append(model!)
            }
            self.setCountryDataSource(countries)
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension VisaFilterViewController: UITextFieldDelegate {
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.activeField = textField

        if textField == self.countryTextField {
            self.countryDropDown.show()
            return false
        } else if textField == self.paymentStatusTextField {
            self.paymentStatusDropDown.show()
            return false
        } else if textField == self.visaStatusTextField {
            self.visaStatusDropDown.show()
            return false
        }
        
        return true
    }
}
