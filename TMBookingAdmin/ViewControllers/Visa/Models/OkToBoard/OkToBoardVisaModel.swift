//
//  OkToBoardVisaModel.swift
//  Template
//
//  Created by Akber Sayni on 09/08/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class OkToBoardVisaModel: NSObject, Mappable {
    var id: String = ""
    var okToBoardId: String?
    var name: String?
    var orderNo: String?
    var visa: String?
    var passport: String?
    var ticket: String?
    var cnic: String?
    var userId: String?
    var status: String?
    var visaStatus: String?
    var paymentStatus: String?
    var stamp: String?
    var country: String?
    var dateOfEntry: String?
    var singleVisaAppliedId: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        okToBoardId <- map["ok_to_board_id"]
        name <- map["fullname"]
        orderNo <- map["orderid"]
        visa <- map["visa_copy"]
        passport <- map["passport_copy"]
        ticket <- map["airline_ticket"]
        cnic <- map["cnic_copy"]
        userId <- map["user_id"]
        status <- map["okb_status"]
        visaStatus <- map["visa_status"]
        paymentStatus <- map["payment_status"]
        stamp <- map["stamp_image"]
        country <- map["country"]
        dateOfEntry <- map["date_of_entry"]
        singleVisaAppliedId <- map["singlevisaapply_id"]
    }
}
