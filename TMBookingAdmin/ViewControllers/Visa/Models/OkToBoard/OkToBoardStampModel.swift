//
//  OkToBoardStampModel.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 10/08/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class OkToBoardStampModel: NSObject, Mappable{
    var id: String = ""
    var vendor: String?
    var stamp: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        vendor <- map["vendor_name"]
        stamp <- map["stamp"]
    }
}
