//
//  VisaMemberFieldsModel.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 22/05/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class VisaMemberFieldsModel: NSObject, Mappable {
    var identifier: String?
    var show: String?
    var required: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        identifier <- map["identifier"]
        show <- map["show"]
        required <- map["required"]
    }
}
