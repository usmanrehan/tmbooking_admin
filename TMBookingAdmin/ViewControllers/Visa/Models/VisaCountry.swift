//
//  VisaCountry.swift
//  Template
//
//  Created by Akber Sayni on 06/01/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class VisaCountry: NSObject, Mappable {
    var id: String = ""
    var name: String?
    var imageURL: String?
    var visaTypes: [VisaTypeModel]?

    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["cname"]
        imageURL <- map["photo"]
        visaTypes <- map["countryvisatypeinfo"]
    }
}
