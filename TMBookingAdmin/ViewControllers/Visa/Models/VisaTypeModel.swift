//
//  VisaTypeModel.swift
//  Template
//
//  Created by Akber Sayni on 15/04/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class VisaTypeModel: NSObject, Mappable {
    var id: String = ""
    var countryId: String = ""
    var visaType: String?
    var title: String?
    var duration: String?
    var price: String?
    var entries: String?
    var image: String?
    var visaDescription: String?
    var howToApplyText: String?
    var restrictionText: String?
    var arrivalText: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        countryId <- map["countryid"]
        visaType <- map["visatype"]
        title <- map["title"]
        duration <- map["duration"]
        price <- map["price"]
        entries <- map["entries"]
        image <- map["image"]
        visaDescription <- map["description"]
        howToApplyText <- map["apply"]
        restrictionText <- map["restriction"]
        arrivalText <- map["arrival"]
    }
}
