//
//  VisaFilterRequestModel.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 19/03/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit

class VisaFilterRequestModel: NSObject {
    var fromDate: String?
    var toDate: String?
    var orderNo: String?
    var passport: String?
    var country: String?
    var paymentStatus: String?
    var visaStatus: String?
}
