//
//  OkToBoardVisaCell.swift
//  Template
//
//  Created by Akber Sayni on 09/08/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit

class OkToBoardVisaCell: UITableViewCell {
    @IBOutlet weak var applicantNameLabel: UILabel!
    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var paymentStatusLabel: UILabel!
    @IBOutlet weak var visaStatusLabel: UILabel!
    @IBOutlet weak var dateOfEntryLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var orderNumberStackView: UIStackView!
    @IBOutlet weak var countryStackView: UIStackView!
    @IBOutlet weak var paymentStatusStackView: UIStackView!
    @IBOutlet weak var visaStatusStackView: UIStackView!
    @IBOutlet weak var dateOfEntryStackView: UIStackView!
    @IBOutlet weak var statusStackView: UIStackView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func setData(_ data: OkToBoardVisaModel) {        
        self.applicantNameLabel.text = data.name ?? "-"
        self.orderNumberLabel.text = data.orderNo ?? "-"
        self.countryLabel.text = data.country ?? "-"
        self.paymentStatusLabel.text = data.paymentStatus ?? "-"
        self.visaStatusLabel.text = data.visaStatus ?? "-"
        self.dateOfEntryLabel.text = data.dateOfEntry ?? "-"
        self.statusLabel.text = data.status ?? "-"        
    }
}
