//
//  VisaMembers.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 23/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class VisaMembers: UITableViewCell {

    @IBOutlet weak var lblMemberName: UILabel!
    @IBOutlet weak var lblMemberType: UILabel!
    @IBOutlet weak var lblVisaStatus: UILabel!
    
    func setData(data:SingleVisaModel){
        self.selectionStyle = .none
        self.lblMemberName.text = data.fullname ?? "-"
        self.lblMemberType.text = (data.isLead == "1") ? "Leader" : data.relation ?? ""
        self.lblVisaStatus.text = data.visaStatus ?? "-"
    }
}
