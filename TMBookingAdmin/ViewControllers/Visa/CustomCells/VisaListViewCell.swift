//
//  VisaListViewCell.swift
//  Template
//
//  Created by Akber Sayni on 10/04/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class VisaListViewCell: UITableViewCell {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var visaTypeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func setData(_ model: VisaTypeModel) {
        self.visaTypeLabel.text = model.visaType
        self.titleLabel.text = model.title
        self.priceLabel.text = String(format: "SAR %@", model.price ?? "0")
    }    
}
