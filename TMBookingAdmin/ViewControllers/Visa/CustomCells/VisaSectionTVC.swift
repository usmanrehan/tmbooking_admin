//
//  VisaSectionTVC.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 22/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class VisaSectionTVC: UITableViewCell {

    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblApplicant: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblValidity: UILabel!
    @IBOutlet weak var lblVisaType: UILabel!
    @IBOutlet weak var lblVisaStatus: UILabel!
    @IBOutlet weak var lblPaymentStatus: UILabel!
    @IBOutlet weak var lblDateOfEntry: UILabel!
    @IBOutlet weak var lblSaudiVisaFeeApplicable: UILabel?
    @IBOutlet weak var visaStatusStackView: UIStackView!
    
    func setData(data:SingleVisaModel, visaApplicationType: VisaApplicationType) {
        self.selectionStyle = .none
        
        self.lblOrderNumber.text = data.orderid ?? "0"
        self.lblApplicant.text = data.fullname ?? "-"
        self.lblCountry.text = data.country ?? "-"
        self.lblValidity.text = data.validity ?? "-"
        self.lblVisaType.text = data.visatype ?? "-"
        self.lblPaymentStatus.text = data.paymentStatus ?? "-"
        self.lblVisaStatus.text = data.visaStatus ?? "-"
        self.lblDateOfEntry.text = data.date ?? "-"
        
        if let label = self.lblSaudiVisaFeeApplicable {
            switch data.saudiFeeApplicable {
            case "1":
                label.text = "Yes"
                break
            case "2":
                label.text = "No"
                break
            default:
                label.text = "Unknown"
                break
            }
        }
        
        self.visaStatusStackView.isHidden = (visaApplicationType == .group) ? true : false
    }
}
