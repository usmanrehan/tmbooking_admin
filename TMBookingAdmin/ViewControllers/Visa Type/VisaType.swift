//
//  VisaType.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 06/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class VisaType: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var arrVisaTypes = [VisaTypeModel]()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Visa Types"
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getVisaTypeList()
    }
    @IBAction func onBtnAddVisaType(_ sender: UIBarButtonItem) {
        self.pushToVisaTypeDetail(index: -1, flag: false)//Add New Visa Type
    }
}
//MARK:- Helper Methods
extension VisaType{
    private func pushToVisaTypeDetail(index:Int, flag: Bool){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "VisaTypeDetail") as? VisaTypeDetail{
            controller.flagIsVisaTypeDetail = flag
            if flag {controller.visaTypeModel = self.arrVisaTypes[index]}
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
//MARK:- UITableViewDataSource
extension VisaType: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrVisaTypes.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VisaTypeTVC", for: indexPath) as! VisaTypeTVC
        cell.setData(data: self.arrVisaTypes[indexPath.row])
        return cell
    }
}
//MARK:- UITableViewDelegate
extension VisaType: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pushToVisaTypeDetail(index: indexPath.row, flag: true)//Show Visa Type Detail
    }
}
//MARK:- Services
extension VisaType{
    private func getVisaTypeList(){
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.VisaTypeList(params: [String:Any](), success: { (responseArray) in
            Utility.hideLoader()
            let response = Mapper<VisaTypeModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            self.arrVisaTypes = response
            self.tableView.reloadData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
