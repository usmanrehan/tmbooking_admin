//
//  VisaTypeTVC.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 06/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class VisaTypeTVC: UITableViewCell {

    @IBOutlet weak var lblVisaTitle: UILabel!
    @IBOutlet weak var lblVisaType: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    func setData(data: VisaTypeModel){
        self.selectionStyle = .none
        self.lblVisaTitle.text = data.title ?? "-"
        self.lblVisaType.text = data.visatype ?? "-"
        self.lblPrice.text = data.price ?? "-"
    }
}
