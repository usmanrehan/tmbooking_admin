//
//  VisaTypeDetail.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 06/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class VisaTypeDetail: BaseViewController {

    @IBOutlet weak var btnEditVisa: UIBarButtonItem!
    @IBOutlet weak var btnImagePicker: UIButton!
    @IBOutlet weak var imgVisaType: UIImageView!
    @IBOutlet weak var tfVisaType: UITextField!
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tfDuration: UITextField!
    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var tvDescription: UITextView!
    @IBOutlet weak var tvApply: UITextView!
    @IBOutlet weak var tvRestriction: UITextView!
    @IBOutlet weak var tvArrival: UITextView!
    @IBOutlet weak var btnDelete: UIButton!
    
    var visaTypeImage: UIImage?
    var imagePicker: UIImagePickerController!
    var flagCanEdit = false
    var flagIsVisaTypeDetail = true
    var visaTypeModel = VisaTypeModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
    }

    @IBAction func onBtnEditVisa(_ sender: UIBarButtonItem) {
        if flagIsVisaTypeDetail{
            if !self.flagCanEdit{
                self.btnEditVisa.title = "Update"
                self.enableUserInteraction()
                self.flagCanEdit = true
            }
            else{
                self.validate()
            }
        }
        else{
            self.validate()
        }
    }
    @IBAction func onBtnAddImage(_ sender: UIButton) {
        self.uploadImage()
    }
    @IBAction func onBtnDelete(_ sender: UIButton) {
        Utility.showAlertWithYesNo(title: "Confirmation", message: "Are you sure to remove this record!") { (true) in
            self.deleteVisaType()
        }
    }
}
//MARK:- Helper Methods
extension VisaTypeDetail{
    private func setUI(){
        self.title = "Visa Type"
        if self.flagIsVisaTypeDetail{
            self.setData()
            self.btnDelete.isHidden = false
        }
        else{
            self.setUIForAddVisaType()
            self.btnDelete.isHidden = true
        }
    }
    private func setData(){
        let data = self.visaTypeModel
        let visaTypeImage = data.image ?? "http://tmbooking.com/tmbooking/public/img/placeholder.png"
        self.imgVisaType.sd_setImage(with: Utility.stringToUrl(string: visaTypeImage), completed: nil)
        self.tfVisaType.text = data.visatype ?? "-"
        self.tfTitle.text = data.title ?? "-"
        self.tfDuration.text = data.duration ?? "-"
        self.tfPrice.text = data.price ?? "-"
        self.tvDescription.text = data.descriptionValue ?? "-"
        self.tvApply.text = data.apply ?? "-"
        self.tvRestriction.text = data.restriction ?? "-"
        self.tvArrival.text = data.arrival ?? "-"
    }
    private func setUIForAddVisaType(){
        self.btnEditVisa.title = "Update"
        self.enableUserInteraction()
    }
    private func enableUserInteraction(){
        self.btnImagePicker.isUserInteractionEnabled = true
        self.tfVisaType.isUserInteractionEnabled = true
        self.tfTitle.isUserInteractionEnabled = true
        self.tfDuration.isUserInteractionEnabled = true
        self.tfPrice.isUserInteractionEnabled = true
        self.tvDescription.isUserInteractionEnabled = true
        self.tvApply.isUserInteractionEnabled = true
        self.tvRestriction.isUserInteractionEnabled = true
        self.tvArrival.isUserInteractionEnabled = true
    }
    private func disableUserInteraction(){
        self.btnImagePicker.isUserInteractionEnabled = false
        self.tfVisaType.isUserInteractionEnabled = false
        self.tfTitle.isUserInteractionEnabled = false
        self.tfDuration.isUserInteractionEnabled = false
        self.tfPrice.isUserInteractionEnabled = false
        self.tvDescription.isUserInteractionEnabled = false
        self.tvApply.isUserInteractionEnabled = false
        self.tvRestriction.isUserInteractionEnabled = false
        self.tvArrival.isUserInteractionEnabled = false
    }
    private func validate(){
        self.visaTypeImage = self.imgVisaType.image
        if visaTypeImage == nil{
            Utility.showAlert(title: "Error", message: "Please provide visa type photo.")
            return
        }
        if (self.tfVisaType.text?.isEmpty)! || (self.tfTitle.text?.isEmpty)! || (self.tfDuration.text?.isEmpty)! || (self.tfPrice.text?.isEmpty)! || (self.tvDescription.text?.isEmpty)! || (self.tvApply.text?.isEmpty)! || (self.tvRestriction.text?.isEmpty)! || (self.tvArrival.text?.isEmpty)!{
            Utility.showAlert(title: "Error", message: "All fields are required.")
            return
        }
        if flagIsVisaTypeDetail{
            self.updateVisaType()
        }
        else{
            self.addVisaType()
        }
    }
    private func getParams()->[String:Any]{
        let visatype = self.tfVisaType.text ?? ""
        let title = self.tfTitle.text ?? ""
        let duration = self.tfDuration.text ?? ""
        let price = self.tfPrice.text ?? ""
        let description = self.tvDescription.text ?? ""
        let apply = self.tvApply.text ?? ""
        let image = UIImageJPEGRepresentation(self.visaTypeImage!, 0.1)! as Data
        let restriction = self.tvRestriction.text ?? ""
        let arrival = self.tvArrival.text ?? ""
        let id = self.visaTypeModel.id ?? "0"
        let params: [String:Any] = ["visatype":visatype,
                                    "title":title,
                                    "duration":duration,
                                    "price":price,
                                    "description":description,
                                    "apply":apply,
                                    "image":image,
                                    "restriction":restriction,
                                    "arrival":arrival,
                                    "id":id]
        return params
    }
}
//MARK:- Image picker
extension VisaTypeDetail{
    func uploadFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
    }
    func uploadFromGallery(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
        
    }
    func uploadImage(){
        let alert = UIAlertController(title: "Upload Photo" , message: "How do you want to set your photo?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
            self.uploadFromCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
            self.uploadFromGallery()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
//MARK: - UIImagePickerControllerDelegate
extension VisaTypeDetail: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.visaTypeImage = pickedImage
            self.imgVisaType.image = pickedImage
        }
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- Services
extension VisaTypeDetail{
    private func deleteVisaType(){
        let param: [String:Any] = ["id":self.visaTypeModel.id ?? "0"]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.RemoveVisaType(params: param, success: { (response) in
            Utility.hideLoader()
            Utility.showAlert(title: "Confirmation", message: "Visa type deleted successfully.", closure: { (true) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func updateVisaType(){
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.UpdateVisaType(params: self.getParams(), success: { (responseObject) in
            Utility.hideLoader()
            let response = Mapper<VisaTypeModel>().map(JSON: responseObject as [String : Any]) ?? VisaTypeModel()
            self.visaTypeModel = response
            self.setUI()
            self.btnEditVisa.title = "Edit"
            self.disableUserInteraction()
            self.flagCanEdit = false
            Utility.showAlert(title: "Confirmation", message: "Record updated successfully.")
            print(responseObject)
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func addVisaType(){
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.AddVisaType(params: self.getParams(), success: { (responseObject) in
            Utility.hideLoader()
            let response = Mapper<VisaTypeModel>().map(JSON: responseObject as [String : Any]) ?? VisaTypeModel()
            self.visaTypeModel = response
            self.setUI()
            Utility.showAlert(title: "Confirmation", message: "Record added successfully.")
            print(responseObject)
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
