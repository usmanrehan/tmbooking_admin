//
//  CoverWithEditDelete.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 06/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class CoverWithEditDelete: UITableViewCell {

    @IBOutlet weak var imgCover: UIImageView!
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
}
