//
//  BookingsCalendar.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 05/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import FSCalendar
import ObjectMapper
import SwiftyUserDefaults

class BookingsCalendar: BaseViewController {
    @IBOutlet weak var calendar: FSCalendar!
    
	let date = Date()
	var arrListBookingCount = [BookingsCountModel]()
    let week_days_view = weekDaysView.instanceFromNib() as! weekDaysView
	
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
		self.addNavigationItems()
		self.updateCalendarUI()
		self.getBookingsWith(month: self.getCurrentMonth(), year: self.getCurrenYear())
    }
	
	private func addNavigationItems() {
		guard let navigation = self.navigationController else { return }
		if (navigation.viewControllers.count) == 1 {
			self.addSideMenuButtonItem()
		}
	}

    @IBAction func onBtnSubmit(_ sender: UIButton) {
        self.pushToBookingsTypeMakkahMadina()
    }
}
extension BookingsCalendar:FSCalendarDataSource{
    func calendar(_ calendar: FSCalendar, subtitleFor date: Date) -> String? {
        let calendarDateFormat = "yyyy-MM-dd HH:mm:ss Z"
        let calendarDate = Utility.stringDateFormatter(dateStr: String(describing: date), dateFormat: calendarDateFormat, formatteddate: "dd-MM-yyyy")
        for date in self.arrListBookingCount{
            if (date.selectedData ?? "") == calendarDate && date.count != 0{
                return "\(date.count)"
            }
        }
        return nil
    }
}
extension BookingsCalendar:FSCalendarDelegate{
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.resetCalendar()
        let calendarDateFormat = "yyyy-MM-dd HH:mm:ss Z"
        let month = Utility.stringDateFormatter(dateStr: String(describing: calendar.currentPage), dateFormat: calendarDateFormat, formatteddate: "MM")
        let year = Utility.stringDateFormatter(dateStr: String(describing: calendar.currentPage), dateFormat: calendarDateFormat, formatteddate: "yyyy")
        self.getBookingsWith(month: month, year: year)
    }
}
//MARK:- Helper Methods
extension BookingsCalendar{
    private func pushToBookingsTypeMakkahMadina(){
        let storyboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "BookingsTypeMakkahMadina") as? BookingsTypeMakkahMadina{
            controller.dates = self.getCommaSeparatedDates()
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    private func updateCalendarUI(){
        self.calendar.headerHeight = self.view.frame.height * 0.10
        self.calendar.appearance.headerDateFormat = "MMM YYYY"
        self.calendar.appearance.headerTitleFont = UIFont.init(name: "Poppins-Medium", size: 19)
        self.calendar.appearance.headerTitleColor = UIColor.darkGray
        self.week_days_view.frame = self.calendar.calendarWeekdayView.frame
        self.calendar.calendarWeekdayView.addSubview(self.week_days_view)
        self.calendar.appearance.separators = .none
        self.calendar.allowsMultipleSelection = true
        
    }
    private func getToday()-> String {
        let calendar = Calendar.current
        let day = calendar.component(.day, from: self.date)
        let year = calendar.component(.year, from: self.date)
        let month = calendar.component(.month, from: self.date)
        var today = String(day) + "-" + String(month) + "-" + String(year)
        if day < 10{
            today = "0" + String(day) + "-" + String(month) + "-" + String(year)
        }
        return today
    }
    private func getCurrentMonth()-> String {
        let calendar = Calendar.current
        let month = calendar.component(.month, from: self.date)
        return "\(month)"
    }
    private func getCurrenYear()-> String {
        let calendar = Calendar.current
        let year = calendar.component(.year, from: self.date)
        return "\(year)"
    }
    private func resetCalendar(){
        for date_ in self.calendar.selectedDates{
            self.calendar.deselect(date_)
        }
    }
    private func getCommaSeparatedDates()->String{
        var arrDatesString = [String]()
        for dateObj in self.calendar.selectedDates{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MM-yyyy"
            let CalDate = dateFormatterPrint.string(from: dateObj)
            arrDatesString.append(CalDate)
        }
        if !arrDatesString.isEmpty{
            return arrDatesString.joined(separator: ",")
        }
        else{
            return self.getToday()
        }
    }
}
//MARK:- Service
extension BookingsCalendar{
    private func getBookingsWith(month:String,year:String){
        let user_id = Defaults[.loginUserId]
        let params:[String:Any] = ["user_id":user_id,
                                   "month":month,
                                   "year":year]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.ListBookingsCount(params: params, success: { (responseObject) in
            Utility.hideLoader()
            let response = responseObject as NSDictionary
            if let listBookings = response["all"] as? [[String:Any]]{
                self.arrListBookingCount = Mapper<BookingsCountModel>().mapArray(JSONArray: listBookings)
            }
            self.calendar.reloadData()
        }) { (error) in
            Utility.hideLoader()
        }
    }
}
