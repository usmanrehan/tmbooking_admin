//
//  BaseViewController.swift
//  AdminTMBooking
//
//  Created by Akber Sayni on 03/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import LGSideMenuController
import ObjectMapper

class BaseViewController: UIViewController {
    
    var notificationBarItem: UIBarButtonItem?

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let navigation = self.navigationController else {return}
		
        if (navigation.viewControllers.count) > 1 {
            self.addBackBarButtonItem()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	func addSideMenuButtonItem() {
		let image = UIImage(named: "menu_icon")
		let backItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(onBtnOpenSideMenu(_:)))
		self.navigationItem.leftBarButtonItem = backItem
	}
    
    func addBackBarButtonItem() {
        let image = #imageLiteral(resourceName: "ic_back").withRenderingMode(.alwaysOriginal)
        let backItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(backWithPop))
        self.navigationItem.leftBarButtonItem = backItem
    }
    
    func getNotificationItem() -> UIBarButtonItem {
        let icon = UIImage(named: "ic_notification")
        let button = UIButton()
        button.setImage(icon, for: .normal)
        button.addTarget(self, action: #selector(onBtnNotificationItem), for: UIControlEvents.touchUpInside)
        
        //self.notificationBarItem = BBBadgeBarButtonItem(customUIButton: button)
        self.notificationBarItem = UIBarButtonItem(customView: button)
        //self.notificationBarItem?.badgeBGColor = UIColor.white
        //self.notificationBarItem?.badgeTextColor = UIColor(red:0.85, green:0.13, blue:0.16, alpha:1.0)
        
        return self.notificationBarItem!
    }
	
    func getUserId() -> String? {
        return Defaults[.loginUserId]
    }
	
	func isAdminUserLoggedIn() -> Bool {
		if let loginData = Defaults[.loginData]?.toString() {
			let user = Mapper<User>().map(JSONObject: Utility.convertToDictionary(text: loginData))
			if let role = user?.userRole, role == "administrator" {
				return true
			}
		}
		
		return false
	}
	
	// MARK: - Callback Methods
	
	@objc func backWithPop() {
		if (self.navigationController?.viewControllers.count)! > 1 {
			self.navigationController?.popViewController(animated: true)
		} else {
			self.dismiss(animated: true, completion: nil)
		}
	}
	
	@objc func onBtnNotificationItem() {
		if let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: NotificationListController.storyboardId) as? NotificationListController {
			self.navigationController?.pushViewController(controller, animated: true)
		}
	}
	
	// MARK: - IBAction Methods

	@IBAction func onBtnOpenSideMenu(_ sender: Any) {
		self.sideMenuController?.showLeftView()
	}
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
