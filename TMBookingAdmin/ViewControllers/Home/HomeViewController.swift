//
//  HomeViewController.swift
//  AdminTMBooking
//
//  Created by Akber Sayni on 03/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import Crashlytics

class HomeViewController: BaseViewController {
    
    let notificationIdentifier: String = "PushController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let notificationItem = self.getNotificationItem()
        self.navigationItem.rightBarButtonItem = notificationItem
    }
    
    //MARK: - Helper Methods
    private func navigateToBookings() {
        let storyboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "BookingsCalendar") as? BookingsCalendar{
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func navigateToVisaSection() {
        let storyboard = UIStoryboard(name: "VisaSection", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "VisaSection") as? VisaSection{
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func navigateToVisaTypes() {
        let storyboard = UIStoryboard(name: "VisaSection", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "CountryVisaListViewController") as? CountryVisaListViewController {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func navigateToRidesBooking() {
        let storyboard = UIStoryboard(name: "Rides", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "Bookings") as? Bookings {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func navigateToDriversListing() {
        let storyboard = UIStoryboard(name: "Rides", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "DriversList") as? DriversList {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func navigateToDriversLocation() {
        let storyboard = UIStoryboard(name: "Rides", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "DriverLocations") as? DriverLocations {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func showOrders() {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "OrderHistoryController")
            as! OrderHistoryController
        
        self.navigationController?.pushViewController(controller, animated: true)
    }    
}

//MARK: - IBAction Methods
extension HomeViewController {
    @IBAction func onBtnVisaApplications(_ sender: Any) {
        self.navigateToVisaSection()
    }
    
    @IBAction func onBtnVisaManagement(_ sender: UIButton) {
        self.navigateToVisaTypes()        
    }
    
    @IBAction func onBtnRidesManagement(_ sender: Any) {
        self.navigateToRidesBooking()
    }
    
    @IBAction func onBtnDriversListing(_ sender: Any) {
        self.navigateToDriversListing()
    }
    
    @IBAction func onBtnDriversLocation(_ sender: Any) {
        self.navigateToDriversLocation()
    }
    
    @IBAction func onBtnBookingManagement(_ sender: Any) {
        self.navigateToBookings()
    }
    
    @IBAction func onBtnOrders(_ sender: AnyObject) {
        self.showOrders()
    }
}
