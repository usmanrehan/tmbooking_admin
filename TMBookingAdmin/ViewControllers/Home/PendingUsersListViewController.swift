//
//  PendingUsersListViewController.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 18/06/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import ObjectMapper

class PendingUsersListViewController: BaseViewController {
    @IBOutlet weak var noRecordFound: UILabel!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.tableFooterView = UIView()
        }
    }
    
    var arrUserRequests: [UserRequestModel] = Array()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getPendingUsers()
    }
    
    private func getPendingUsers() {
        Utility.showLoader()
        let params: [String: Any] = [
            "current_user_id": Defaults[.loginUserId]
        ]
        APIManager.sharedInstance.apiManagerServicesWordPress.getPendingUsers(params: params, success: { (response) in
            Utility.hideLoader()
            if let data = response as? Array<AnyObject> {
                let array = Mapper<UserRequestModel>().mapArray(JSONArray: data as! [[String : Any]])
                
                self.arrUserRequests = array
                self.tableView.reloadData()
            }
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    // MARK: - Navigation
    
    private func showRequestDetails(_ indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PendingUserDetailsViewController")
            as! PendingUserDetailsViewController
        
        controller.userRequest = self.arrUserRequests[indexPath.row]
        
        self.navigationController?.pushViewController(controller, animated: true)
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
}

extension PendingUsersListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.noRecordFound.isHidden = (self.arrUserRequests.count == 0) ? false : true
        return self.arrUserRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PendingRequestCellIdentifier", for: indexPath)
            as! PendingRequestListViewCell
        
        let data = self.arrUserRequests[indexPath.row]
        cell.usernameLabel.text = data.username ?? "-"
        cell.emailLabel.text = data.email ?? "-"
        cell.statusLabel.text = data.statusName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.showRequestDetails(indexPath)
    }
}
