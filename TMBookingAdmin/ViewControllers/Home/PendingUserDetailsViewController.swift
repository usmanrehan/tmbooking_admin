//
//  PendingUserDetailsViewController.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 18/06/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class PendingUserDetailsViewController: BaseViewController {
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var roleNameLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.tableFooterView = UIView()
        }
    }

    var userRequest: UserRequestModel?    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setData(self.userRequest)
    }
    
    private func setData(_ data: UserRequestModel?) {
        guard data != nil else {
            return
        }
        
        self.usernameLabel.text = data?.username
        self.nameLabel.text = String(format: "%@ %@", data?.firstName ?? " ", data?.lastName ?? " ")
        self.emailLabel.text = data?.email
        self.statusLabel.text = data?.statusName
        self.roleNameLabel.text = data?.roleName
        
        self.tableView.reloadData()
    }
    
    @IBAction func onBtnApprove(_ sender: AnyObject) {
        self.updateUserStatus(1)
    }
    
    @IBAction func onBtnDisapprove(_ sender: AnyObject) {
        self.updateUserStatus(2)
    }
    
    private func updateUserStatus(_ status: Int) {
        Utility.showLoader()
        let params: [String: Any] = [
            "user_id": self.userRequest?.id ?? 0,
            "current_user_id": Defaults[.loginUserId],
            "status": status
        ]
        APIManager.sharedInstance.apiManagerServicesWordPress.updateUserStatus(params: params, success: { (response) in
            Utility.hideLoader()
            if let data = response as? Dictionary<String, Any> {
                self.userRequest = UserRequestModel(JSON: data)
                self.setData(self.userRequest)
            }
            Utility.showAlert(title: "Message", message: "User status updated")
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }

    // MARK: - Navigation
    
    private func showRequestDetails(_ indexPath: IndexPath) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PendingUserDetailsViewController")
            as! PendingUserDetailsViewController
        
        controller.userRequest = self.userRequest?.subUsers![indexPath.row]
        
        self.navigationController?.pushViewController(controller, animated: true)
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
}

extension PendingUserDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userRequest?.subUsers?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PendingRequestCellIdentifier", for: indexPath)
            as! PendingRequestListViewCell
        
        let data = self.userRequest!.subUsers![indexPath.row]
        cell.usernameLabel.text = data.username ?? "-"
        cell.emailLabel.text = data.email ?? "-"
        cell.statusLabel.text = data.statusName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        self.showRequestDetails(indexPath)
    }
}
