//
//  LeftSideMenuViewController.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 04/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import ObjectMapper

class LeftSideMenuViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
	
	var arrMenuItems = Array<String>()
	
    let notificationName = Notification.Name("PushController")
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
        // Do any additional setup after loading the view.
		self.setMenuItems()
    }
	
	private func setMenuItems() {
		if let loginData = Defaults[.loginData]?.toString() {
			let user = Mapper<User>().map(JSONObject: Utility.convertToDictionary(text: loginData))
			if let role = user?.userRole, role == "administrator" {
				self.arrMenuItems = ["Home", "Pending Users", "Saudi Visa Applications", "Ok-To-Board Visas", "Visa Applications", "Visa Management", "Rides Management", "Booking Management", "Drivers Listing", "Logout"]
			} else {
				self.arrMenuItems = ["Home", "Logout"]
			}
		}
	}
    
    private func logout(){
        let message = "Are you sure you want to Logout?"
        Utility.showAlertWithYesNo(title: "Confirmation", message: message, closure: { (_) in
            self.logoutAPI()
            
            Defaults[.loginStatus] = false
            Defaults.remove(.loginData)
            Constants.APP_DELEGATE.changeRootViewController()
        })
    }
    
    private func showPendingUsers() {
        if let navigationController = self.sideMenuController?.rootViewController as? UINavigationController {
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let controller = storyboard.instantiateViewController(withIdentifier: "PendingUsersListViewController")
                as! PendingUsersListViewController
            
            navigationController.pushViewController(controller, animated: true)
        }
    }
    
    private func showVisaAppplications() {
        if let navigationController = self.sideMenuController?.rootViewController as? UINavigationController {
            let storyboard = UIStoryboard(name: "VisaSection", bundle: Bundle.main)
            let controller = storyboard.instantiateViewController(withIdentifier: "VisaSection") as! VisaSection
            navigationController.pushViewController(controller, animated: true)
        }
    }
    
    private func showSaudiVisaApplications() {
        if let navigationController = self.sideMenuController?.rootViewController as? UINavigationController {
            let storyboard = UIStoryboard(name: "VisaSection", bundle: Bundle.main)
            let controller = storyboard.instantiateViewController(withIdentifier: "SaudiVisasViewController")
                as! SaudiVisasViewController
            navigationController.pushViewController(controller, animated: true)
        }
    }
    
    private func showOkToBoardVisas() {
        if let navigationController = self.sideMenuController?.rootViewController as? UINavigationController {
            let storyboard = UIStoryboard(name: "VisaSection", bundle: Bundle.main)
            let controller = storyboard.instantiateViewController(withIdentifier: "OkToBoardVisaListViewConttroller")
                as! OkToBoardVisaListViewConttroller
            navigationController.pushViewController(controller, animated: true)
        }
    }
    
    private func showVisaManagement() {
        if let navigationController = self.sideMenuController?.rootViewController as? UINavigationController {
            let storyboard = UIStoryboard(name: "VisaSection", bundle: Bundle.main)
            let controller = storyboard.instantiateViewController(withIdentifier: "CountryVisaListViewController") as! CountryVisaListViewController
            navigationController.pushViewController(controller, animated: true)
        }
    }
    
    private func showRidesManagement() {
        if let navigationController = self.sideMenuController?.rootViewController as? UINavigationController {
            let storyboard = UIStoryboard(name: "Rides", bundle: Bundle.main)
            let controller = storyboard.instantiateViewController(withIdentifier: "Bookings") as! Bookings
            navigationController.pushViewController(controller, animated: true)
        }
    }
    
    private func showBookingManagement() {
        if let navigationController = self.sideMenuController?.rootViewController as? UINavigationController {
            let storyboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
            let controller = storyboard.instantiateViewController(withIdentifier: "BookingsCalendar") as! BookingsCalendar
            navigationController.pushViewController(controller, animated: true)
        }
    }
    
    private func showDriversListing() {
        if let navigationController = self.sideMenuController?.rootViewController as? UINavigationController {
            let storyboard = UIStoryboard(name: "Rides", bundle: Bundle.main)
            let controller = storyboard.instantiateViewController(withIdentifier: "DriversList") as! DriversList
            navigationController.pushViewController(controller, animated: true)
        }
    }
}

extension LeftSideMenuViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrMenuItems.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftSideMenuCellIdentifier", for: indexPath) as! LeftSideMenuViewCell
        cell.imgDropDown.isHidden = true
        cell.lblTitle.text = self.arrMenuItems[indexPath.section]
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.arrMenuItems[indexPath.section]
        switch item {
        case "Home":
            break
        case "Pending Users":
            self.showPendingUsers()
            break
        case "Saudi Visa Applications":
            self.showSaudiVisaApplications()
            break
        case "Ok-To-Board Visas":
            self.showOkToBoardVisas()
            break
        case "Visa Applications":
            self.showVisaAppplications()
            break
        case "Visa Management":
            self.showVisaManagement()
            break
        case "Rides Management":
            self.showRidesManagement()
            break
        case "Booking Management":
            self.showBookingManagement()
            break
        case "Drivers Listing":
            self.showDriversListing()
            break
        case "Logout":
			DispatchQueue.main.async {
				self.logout()
			}
            return
        default:
            break
        }
        
        self.sideMenuController?.hideLeftView()
    }
}

//MARK:- Web APIs
extension LeftSideMenuViewController {
    private func logoutAPI() {
        let params = ["user_id": Defaults[.loginUserId],
                      "token": Defaults[.deviceToken]] as [String : Any]
        
        APIManager.sharedInstance.apiManagerServicesWordPress.signOutWith(parameters: params, success: { (response) in
            print("Logout API success")
        }) { (error) in
            print("Failed to logout: \(error.localizedDescription)")
        }
    }
}

