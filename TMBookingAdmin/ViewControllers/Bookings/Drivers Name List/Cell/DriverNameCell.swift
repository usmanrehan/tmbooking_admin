//
//  DriverNameCell.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 28/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class DriverNameCell: UITableViewCell {

    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var tfDriverName: UILabel!
    

}
