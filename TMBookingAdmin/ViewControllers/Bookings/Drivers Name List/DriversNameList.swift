//
//  DriversNameList.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 28/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import DropDown

class DriversNameList: UIViewController {
    @IBOutlet weak var vendorTextField: UITextField!
    @IBOutlet weak var driverTextField: UITextField!
    
    var activeTextField: UITextField!
    var vendorsDropDown: DropDown!
    var driversDropDown: DropDown!

    var rideObject = RideModel()
    var arrVendors: [TransportVendor]?
    var selectedDriver: DriverModel?
    var selectedVendor: TransportVendor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.vendorsDropDown = setDropDown(self.vendorTextField)
        self.driversDropDown = setDropDown(self.driverTextField)
        
        self.getTransportVendors()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func setDropDown(_ anchorView: UITextField) -> DropDown {
        if let image = UIImage(named: "ic_dropdown") {
            anchorView.addPaddingRightIcon(image, padding: 8.0)
        }
        
        anchorView.delegate = self
        
        let dropdown = DropDown()
        dropdown.anchorView = anchorView
        dropdown.bottomOffset = CGPoint(x: 0, y: (dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.selectionAction = { (index: Int, item: String) in
            if anchorView == self.vendorTextField {
                self.onSelectVendor(index, item: item)
            } else if anchorView == self.driverTextField {
                self.onSelectDriver(index, item: item)
            }
        }
        
        return dropdown
    }
    
    private func onSelectVendor(_ index: Int, item: String) {
        self.vendorTextField.text = item
        self.selectedVendor = self.arrVendors![index]
        
        // Reset driver details
        self.selectedDriver = nil
        self.driverTextField.text = nil
        if let drivers = self.selectedVendor?.drivers {
            self.driversDropDown.dataSource = drivers.map {$0.name ?? ""}
        } else {
            self.driversDropDown.dataSource = Array<String>()
        }
    }

    private func onSelectDriver(_ index: Int, item: String) {
        self.driverTextField.text = item
        if let vendor = self.selectedVendor {
            self.selectedDriver = vendor.drivers![index]
        }
    }
    
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        guard self.selectedVendor != nil else {
            Utility.showAlert(title: "Message", message: "Vendor details not provided")
            return
        }

        guard self.selectedDriver != nil else {
            Utility.showAlert(title: "Message", message: "Driver details not provided")
            return
        }
        
        self.assignDriver()
    }
}

//MARK:- Services
extension DriversNameList {
    private func getTransportVendors() {
        Utility.showLoader()
        APIManager.sharedInstance.apiManagerServicesWordPress.getTransportVendors(params: [:], success: { (response) in
            Utility.hideLoader()
            if let array = response as? [AnyObject] {
                var vendors = Array<TransportVendor>()
                for item in array {
                    let model = TransportVendor(JSON: item as! [String: Any])
                    vendors.append(model!)
                }
                self.arrVendors = vendors
                self.vendorsDropDown.dataSource = self.arrVendors!.map {$0.name ?? ""}
            }
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    private func assignDriver(){
        var params: [String:Any] = [
            "id": self.rideObject.id ?? "",
            "driverid": self.selectedDriver?.id ?? 0,
            "pickuptime": self.rideObject.pickuptime ?? ""
        ]
        if let status = self.rideObject.statusValue, status == "1" {
            // Re-assign driver
            params["status"] = "5"
        }
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.AssignDriver(params: params, success: { (responseObject) in
            Utility.hideLoader()
            Utility.showAlert(title: "Message", message: "Driver assigned successfully", closure: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}

extension DriversNameList: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.activeTextField = textField
        
        if textField == self.vendorTextField {
            self.vendorsDropDown.show()
            return false
        } else if textField == self.driverTextField {
            self.driversDropDown.show()
            return false
        }
        
        return true
    }
}
