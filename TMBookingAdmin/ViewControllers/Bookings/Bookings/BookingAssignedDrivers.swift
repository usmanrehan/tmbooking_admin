//
//  BookingAssignedDrivers.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 14/04/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class BookingAssignedDrivers: BaseViewController {
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView()
        }
    }
    
    var rideObject: RideModel?
    var assignedDrivers = [AssignedDriverModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.getAssignedDriversLogs(self.rideObject?.id ?? "0")
    }
    
    private func getAssignedDriversLogs(_ bookingId: String) {
        let param: [String:Any] = ["id": bookingId]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.GetAssignedDrivers(params: param, success: { (response) in
            Utility.hideLoader()
            let array = Mapper<AssignedDriverModel>().mapArray(JSONArray: response as! [[String : Any]])
            self.assignedDrivers = array
            self.tableView.reloadData()

        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BookingAssignedDrivers: UITableViewDataSource, UITableViewDelegate {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.assignedDrivers.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AssignDriverCellIdentifier") as! BookingAssignDriverCell
        cell.setData(self.assignedDrivers[indexPath.row])
        
        return cell
    }

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
