//
//  NewBookings.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 08/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

enum BookingsType {
    case pending
    case confirmed
    case active
    case history
    case cancel
}
class Bookings: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pendingButton: UIButton!
    @IBOutlet weak var confirmedButton: UIButton!
    @IBOutlet weak var activeButton: UIButton!
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    var bookingTypeButtons: [UIButton] { return [pendingButton, confirmedButton, activeButton, historyButton, cancelButton] }

    @IBOutlet weak var lblNoBookingAvailable: UILabel!

    var bookingType = BookingsType.pending
    var arrBookings = [RideModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refreshData()
    }
    
    private func refreshData() {
        self.clearTableViewData()
        switch self.bookingType {
        case .pending:
            self.getBookingRides("0")
            break
        case .confirmed:
            self.getBookingRides("6")
            break
        case .active:
            self.getActiveRides()
            break
        case .history:
            self.getBookingRides("4")
            break
        case .cancel:
            self.getBookingRides("7")
            break
        }
    }
    
    private func clearTableViewData() {
        self.arrBookings = Array()
        self.tableView.reloadData()
    }
    
    private func updateBookingButtonsUI(_ sender: UIButton) {
        for item in self.bookingTypeButtons {
            if item == sender {
                item.isSelected = true
                item.backgroundColor = Global.APP_COLOR
            } else {
                item.isSelected = false
                item.backgroundColor = .white
            }
        }
    }
    
    @IBAction func onBtnChangeBookingType(_ sender: UIButton) {
        switch sender {
        case pendingButton:
            self.bookingType = .pending
            break
        case confirmedButton:
            self.bookingType = .confirmed
            break
        case activeButton:
            self.bookingType = .active
            break
        case historyButton:
            self.bookingType = .history
            break
        case cancelButton:
            self.bookingType = .cancel
            break
        default:
            break
        }
        
        self.updateBookingButtonsUI(sender)
        self.refreshData()
    }
}

//MARK:- Helper Methods
extension Bookings{
    private func pushToBookingDetail(index: Int){
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "BookingDetails") as? BookingDetails {
            controller.bookingType = self.bookingType
            controller.rideObject = self.arrBookings[index]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

extension Bookings: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.lblNoBookingAvailable.isHidden = (self.arrBookings.count > 0) ? true : false
        
        return self.arrBookings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingsTVC", for: indexPath) as! BookingsTVC
        cell.setData(data: self.arrBookings[indexPath.row])
        return cell
    }
}

extension Bookings: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pushToBookingDetail(index: indexPath.row)
    }
}

//MARK:- Services
extension Bookings{
    private func getBookingRides(_ status: String) {
        let params = [
            "status": status
        ]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.getBookingRides(params: params, success: { (responseArray) in
            Utility.hideLoader()
            let response = Mapper<RideModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            self.arrBookings = response
            self.tableView.reloadData()
        }) { (error) in
            print(error.localizedDescription)
            Utility.hideLoader()
        }
    }
    
    private func getPendingRides(){
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.PendingRides(params: [String:Any](), success: { (responseArray) in
            Utility.hideLoader()
            let response = Mapper<RideModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            self.arrBookings = response
            self.tableView.reloadData()
        }) { (error) in
            print(error.localizedDescription)
            Utility.hideLoader()
        }
    }
    
    private func getActiveRides(){
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.ActiveRides(params: [String:Any](), success: { (responseArray) in
            Utility.hideLoader()
            let response = Mapper<RideModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            self.arrBookings = response
            self.tableView.reloadData()
        }) { (error) in
            print(error.localizedDescription)
            Utility.hideLoader()
        }
    }
    
    private func getCancelRidesList(){
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.CancelRidesList(params: [String:Any](), success: { (responseArray) in
            Utility.hideLoader()
            let response = Mapper<RideModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            self.arrBookings = response
            self.tableView.reloadData()
        }) { (error) in
            print(error.localizedDescription)
            Utility.hideLoader()
        }
    }
    
    private func getBookingHistoryList(){
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.BookingHistory(params: [String:Any](), success: { (responseArray) in
            Utility.hideLoader()
            let response = Mapper<RideModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            self.arrBookings = response
            self.tableView.reloadData()
        }) { (error) in
            print(error.localizedDescription)
            Utility.hideLoader()
        }
    }
}
