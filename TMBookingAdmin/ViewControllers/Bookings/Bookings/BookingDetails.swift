//
//  BookingDetails.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 07/04/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit

class BookingDetails: BaseViewController {
    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var pickupLabel: UILabel!
    @IBOutlet weak var dropoffLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var carTypeLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var riderLabel: UILabel!
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var assignButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var assignedLogButton: UIButton!
    
    var bookingType = BookingsType.pending
    var rideObject = RideModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setData(rideObject)
        self.updateActionButtonUI()
    }
    
    private func setData(_ data: RideModel) {
        self.orderLabel.text = data.orderId ?? "-"
        self.pickupLabel.text = data.pickup ?? "-"
        self.dropoffLabel.text = data.dropoff ?? "-"
        self.dateLabel.text = data.pickuptime ?? "-"
        self.carTypeLabel.text = data.carName ?? "-"
        self.detailsLabel.text = data.extrainfo ?? "-"
        self.riderLabel.text = data.riderName ?? "-"
    }
    
    private func updateActionButtonUI() {
        if let status = rideObject.statusValue {
            switch status {
            case "0": // Pending
                self.assignButton.isHidden = true
                self.cancelButton.isHidden = true
                self.confirmButton.isHidden = false
                self.assignedLogButton.isHidden = true
                break
            case "1": // Assigned driver
                self.assignButton.isHidden = false
                self.cancelButton.isHidden = true
                self.confirmButton.isHidden = true
                self.assignedLogButton.isHidden = false
                self.assignButton.setTitle("RE-ASSIGN DRIVER", for: .normal)
                break
            case "6": // Confirmed
                self.assignButton.isHidden = false
                self.cancelButton.isHidden = true
                self.confirmButton.isHidden = true
                self.assignedLogButton.isHidden = true
                break
            default:
                self.assignButton.isHidden = true
                self.cancelButton.isHidden = true
                self.confirmButton.isHidden = true
                self.assignedLogButton.isHidden = false
                break
            }
        }
    }
    
    private func showDriverListing() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "DriversNameList") as? DriversNameList {
            controller.rideObject = self.rideObject
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func showAssignedDrivers() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "BookingAssignedDrivers")
            as? BookingAssignedDrivers {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func onBtnConfirmBooking(_ sender: UIButton) {
        Utility.showAlertWithYesNo(title: "Confirmation", message: "Are you sure you want to confirm this booking?") { (actionYes) in
            self.confirmationBooking()
        }
    }
    
    @IBAction func onBtnAssignDirver(_ sender: UIButton) {
        self.showDriverListing()        
    }
    
    @IBAction func onBtnCancelBooking(_ sender: UIButton) {
        Utility.showAlertWithYesNo(title: "Confirmation", message: "Are you sure you want to cancel this booking?") { (actionYes) in
            self.cancelBooking()
        }
    }
    
    @IBAction func onBtnAssignedDrivers(_ sender: UIButton) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "BookingAssignedDrivers")
            as? BookingAssignedDrivers {
            controller.rideObject = self.rideObject
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BookingDetails {
    private func confirmationBooking() {
        let data = self.rideObject
        let param: [String:Any] = ["id":data.id ?? "0"]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.ConfirmationBooking(params: param, success: { (responseObject) in
            Utility.hideLoader()
            //let response = responseObject as NSDictionary
            //guard let status = response["status"] as? String else {return}
            //self.rideObject.statusValue = status
            //self.setData(self.rideObject)
            Utility.showAlert(title: "Message", message: "Booking status confirmed", closure: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    private func cancelBooking(){
        let data = self.rideObject
        let param: [String:Any] = ["id":data.id ?? "0"]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.CancelBooking(params: param, success: { (response) in
            Utility.hideLoader()
            Utility.showAlert(title: "Message", message: "Booking status cancelled", closure: { (action) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func getAssignedDriversLogs() {
        let data = self.rideObject
        let param: [String:Any] = ["id":data.id ?? "0"]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.GetAssignedDrivers(params: param, success: { (response) in
            Utility.hideLoader()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
