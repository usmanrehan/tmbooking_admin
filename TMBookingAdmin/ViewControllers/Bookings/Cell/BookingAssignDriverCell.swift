//
//  BookingAssignDriverCell.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 14/04/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit

class BookingAssignDriverCell: UITableViewCell {
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func setData(_ model: AssignedDriverModel) {
        self.driverNameLabel.text = model.driverName
        self.statusLabel.text = model.status
        self.dateTimeLabel.text = model.updatedAt
        
    }

}
