//
//  MyRidesCell.swift
//  Template
//
//  Created by Aiom hk 18 on 08/05/2018.
//  Copyright © 2018 Muzamil Hassan. All rights reserved.
//

import UIKit
import SwiftDate

class BookingsTVC: UITableViewCell {

    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var pickupLabel: UILabel!
    @IBOutlet weak var dropoffLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var carTypeLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var riderName: UILabel!
    
    func setData(data: RideModel){
        self.selectionStyle = .none
        self.orderLabel.text = data.orderId ?? "-"
        self.pickupLabel.text = data.pickup ?? "-"
        self.dropoffLabel.text = data.dropoff ?? "-"
        self.dateLabel.text = data.pickuptime ?? "-"
        self.carTypeLabel.text = data.carName ?? "-"
        self.detailsLabel.text = data.extrainfo ?? "-"
        self.riderName.text = data.riderName ?? "-"
    }
}

