//
//  NewBookings.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 09/09/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class NewBookings: BaseViewController {
    
    @IBOutlet weak var btnPendingRides: UIButton!
    @IBOutlet weak var btnActiveRides: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoBookingAvailable: UILabel!
    var bookingType: BookingsType = .pending
    var arrRides = [RideModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getPendingRides()
        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnPendingRides(_ sender: UIButton) {
        if sender.isSelected{return}
        self.bookingType = .pending
        self.getPendingRides()
        self.btnActiveRides.isSelected = false
        self.btnPendingRides.isSelected = true
    }
    @IBAction func onBtnActiveRides(_ sender: UIButton) {
        if sender.isSelected{return}
        self.bookingType = .active
        self.getActiveRides()
        self.btnActiveRides.isSelected = true
        self.btnPendingRides.isSelected = false
    }
}
//MARK:- Helper Methods
extension NewBookings{
    private func pushToBookingDetail(index: Int){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "BookingDetail") as? BookingDetail{
            controller.bookingType = self.bookingType
            controller.rideObject = self.arrRides[index]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
extension NewBookings: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrRides.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookingsTVC", for: indexPath) as! BookingsTVC
        cell.setData(data: self.arrRides[indexPath.row])
        return cell
    }
}
extension NewBookings: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pushToBookingDetail(index: indexPath.row)
    }
}
//MARK:- Services
extension NewBookings{
    private func getPendingRides(){
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.PendingRides(params: [String:Any](), success: { (responseArray) in
            Utility.hideLoader()
            let response = Mapper<RideModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            self.arrRides = response
            self.bookingType = .pending
            self.tableView.reloadData()
        }) { (error) in
            print(error.localizedDescription)
            Utility.hideLoader()
        }
    }
    private func getActiveRides(){
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.ActiveRides(params: [String:Any](), success: { (responseArray) in
            Utility.hideLoader()
            let response = Mapper<RideModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            self.arrRides = response
            self.bookingType = .active
            self.tableView.reloadData()
        }) { (error) in
            print(error.localizedDescription)
            Utility.hideLoader()
        }
    }
}
