//
//  BookingDetail.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 08/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

//Ride Statuses
//Confirmed = 6
//Pending = 0
//Assigned = 1
//Re assigned = 5

import UIKit
import GoogleMaps

class BookingDetail: BaseViewController {

    @IBOutlet weak var tfCar: UITextField!
    @IBOutlet weak var tfPickUp: UITextField!
    @IBOutlet weak var tfDropOff: UITextField!
    @IBOutlet weak var tfPickUpDate: UITextField!
    @IBOutlet weak var tfPickUpTime: UITextField!
    @IBOutlet weak var tvExtraInfo: UITextView!
    @IBOutlet weak var tfStatus: UITextField!
    @IBOutlet weak var tfAction: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var bookingType = BookingsType.pending
    var rideObject = RideModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Booking Detail"
        self.setData()
        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        let data = self.rideObject
        if self.bookingType != .cancel{
            guard let status = data.statusValue else { return }
            switch status {
            case "0":
                Utility.showAlertWithYesNo(title: "Confirmation", message: "Are you sure Confirm this booking?") { (true) in
                    self.confirmationBooking()
                }
            case "6":
                self.presentDriversListController(statusValue: status)
            case "1":
                self.presentDriversListController(statusValue: status)
            default:
                break
            }
        }
        else if self.bookingType == .cancel{
            Utility.showAlertWithYesNo(title: "Confirmation", message: "Are you sure Cancel this booking?") { (true) in
                self.cancelBooking()
            }
        }
    }
}
//MARK:- Helper Methods
extension BookingDetail{
    private func setData(){
        let data = self.rideObject
        self.tfCar.text = data.carName ?? "-"
        self.tfPickUp.text = data.pickup ?? "-"
        self.tfDropOff.text = data.dropoff ?? "-"
        let date = data.pickuptime ?? "2018-08-29 11:00:00"
        self.tfPickUpDate.text = Utility.stringDateFormatter(dateStr: date, dateFormat: "yyyy-MM-dd HH:mm:ss", formatteddate: "yyyy-MM-dd")
        self.tfPickUpTime.text = Utility.stringDateFormatter(dateStr: date, dateFormat: "yyyy-MM-dd HH:mm:ss", formatteddate: "HH:mm:ss")
        self.tvExtraInfo.text = data.extrainfo ?? "-"
        self.tfStatus.text = data.bookingStatus ?? "-"
        self.setActionStatus()
    }
    private func setActionStatus(){
        let data = self.rideObject
        if self.bookingType == .active || self.bookingType == .pending {
            guard let status = data.statusValue else { return }
            switch status {
            case "0"://Pending = 0
                self.tfAction.text = "Confirmation"
            case "6"://Confirmed = 6
                self.tfAction.text = "Assign Driver"
            case "1":
                self.tfAction.text = "Re-Assign Driver"
            default:
                break
            }
        }
        else if self.bookingType == .cancel{
            self.tfAction.text = "Cancel Booking"
        }
        else if self.bookingType == .history{
            self.btnSubmit.isHidden = true
            self.tfAction.text = "None"
        }
    }
    private func presentDriversListController(statusValue: String){
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "DriversNameList") as? DriversNameList{
            controller.rideObject = self.rideObject
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
//MARK:- Services
extension BookingDetail{
    private func confirmationBooking(){
        let data = self.rideObject
        let param: [String:Any] = ["id":data.id ?? "0"]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.ConfirmationBooking(params: param, success: { (responseObject) in
            Utility.hideLoader()
            let response = responseObject as NSDictionary
            guard let status = response["status"] as? String else {return}
            self.rideObject.statusValue = status
            self.setData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func cancelBooking(){
        let data = self.rideObject
        let param: [String:Any] = ["id":data.id ?? "0"]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.CancelBooking(params: param, success: { (response) in
            Utility.hideLoader()
            Utility.showAlert(title: "Confirmation", message: "Booking cancelled successfully.", closure: { (true) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}

