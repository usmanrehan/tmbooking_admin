//
//  ViewBookingDetail.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 06/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class ViewBookingDetail: BaseViewController {

    @IBOutlet weak var lblHotelName: UILabel!
    @IBOutlet weak var lblPickup: UILabel!
    @IBOutlet weak var lblDropOff: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblNumberOfGuest: UILabel!
    @IBOutlet weak var lblCarType: UILabel!
	@IBOutlet weak var confirmationButton: UIButton!
	
    var transport = TransportModel()
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
        // Do any additional setup after loading the view.
		self.setData()
		self.updateUI()
    }
	
	private func updateUI() {
		if self.isAdminUserLoggedIn() {
			self.confirmationButton.isHidden = false
		} else {
			self.confirmationButton.isHidden = true
		}
	}
	
	private func setData(){
		self.lblHotelName.text = self.transport.name ?? "-"
		self.lblPickup.text = "Pickup: " + (self.transport.pickupPoint ?? "-")
		self.lblDropOff.text = "Drop-off: " + (self.transport.dropoffPoint  ?? "-")
		self.lblDate.text = "Date: " + (self.transport.date  ?? "-")
		self.lblNumberOfGuest.text = "Number of Guests: " + ("-")
		self.lblCarType.text = "Car Type: " + (self.transport.type  ?? "-")
	}
	
	private func isOrderCancelledOrPending() -> Bool {
		if let status = self.transport.status {
			if status.caseInsensitiveCompare("Cancelled") == .orderedSame
				|| status.caseInsensitiveCompare("Pending payment") == .orderedSame {
				Utility.showAlert(title: "Error", message: "User action is not allowed. Please check your order status")
				return true
			}
		}
		
		return false
	}

    @IBAction func onBtnGuestDetails(_ sender: UIButton) {
        self.pushToGuestDetails()
    }
	
	@IBAction func onBtnConfirmation(_ sender: UIButton) {
		guard !self.isOrderCancelledOrPending() else { return }
		self.pushToConfirmationList()
	}
}

//MARK:- Helper Methods

extension ViewBookingDetail{
	
    private func pushToGuestDetails(){
        let storyboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "GuestDetails") as? GuestDetails{
            controller.transport = self.transport
            controller.flagIsTransport = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
	
	private func pushToConfirmationList() {
		if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationList") as? ConfirmationList {
			controller.bookingId = self.transport.bookingId
			controller.isPackageBookingType = self.transport.isPackageBooking
			controller.confirmationType = .transport
			self.navigationController?.pushViewController(controller, animated: true)
		}
	}
}
