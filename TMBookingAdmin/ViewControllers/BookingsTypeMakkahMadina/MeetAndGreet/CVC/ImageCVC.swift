//
//  ImageCVC.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 07/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftyUserDefaults

class ImageCVC: UICollectionViewCell {
    
    @IBOutlet weak var imgImageView: UIImageView!
    @IBOutlet weak var btnDelete: RoundedButton!
    
    func setData(){
        let user = Mapper<User>().map(JSONObject: Utility.convertToDictionary(text: (Defaults[.loginData]?.toString())!))
        if let role = user?.userRole{
            if role == "rm_trade_partner" || role == "rm_main"{
                self.btnDelete.isHidden = true
            }
        }
    }
    
}
