//
//  MeetAndGreet.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 07/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper
import SDWebImage
import SwiftyUserDefaults

class MeetAndGreet: BaseViewController {

    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var tvOtherComments: UITextView!
    @IBOutlet weak var collectionView: UICollectionView!    
    @IBOutlet weak var btnAddAPicture: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var meetGreet = MeetGreetModel()
    var hotel = HotelModel()
    var arrImages = [UIImage]()
    var imagePicker: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getMeetGreet()
        // Do any additional setup after loading the view.
    }

    @IBAction func onBtnYes(_ sender: UIButton) {
        if !self.btnYes.isSelected{
            self.btnYes.isSelected = true
            self.btnNo.isSelected = false
        }
    }
    @IBAction func onBtnNo(_ sender: UIButton) {
        if !self.btnNo.isSelected{
            self.btnYes.isSelected = false
            self.btnNo.isSelected = true
        }
    }
    @IBAction func onBtnAddPicture(_ sender: UIButton) {
        if self.arrImages.count < 3{
            self.uploadImage()
        }
        else{
            Utility.showAlert(title: "Error", message: "Max 3 images can be uploaded")
        }
    }
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        if (self.tvOtherComments.text ?? "").isEmpty{
            Utility.showAlert(title: "Error", message: "Please add comments")
            return
        }
        self.postMeetAndGreet()
    }
}
extension MeetAndGreet:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCVC", for: indexPath) as! ImageCVC
        cell.imgImageView.image = self.arrImages[indexPath.row]
        cell.btnDelete.tag = indexPath.item
        cell.btnDelete.addTarget(self, action: #selector(self.onBtnDeleteImage(sender:)), for: .touchUpInside)
        cell.setData()
        return cell
    }
    @objc func onBtnDeleteImage(sender:UIButton){
        self.arrImages.remove(at: sender.tag)
        self.collectionView.reloadData()
    }
}
extension MeetAndGreet:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.height, height: collectionView.frame.size.height)
    }
}

//MARK:- Helper Methods
extension MeetAndGreet{
    private func setData(){
        self.tvOtherComments.text = self.meetGreet.meetComments ?? ""
        if Int(self.meetGreet.isMandoobMeet ?? "0") == 1{
            self.btnYes.isSelected = true
            self.btnNo.isSelected = false
        }
        else{
            self.btnYes.isSelected = false
            self.btnNo.isSelected = true
        }
        self.collectionView.reloadData()
        let user = Mapper<User>().map(JSONObject: Utility.convertToDictionary(text: (Defaults[.loginData]?.toString())!))
        if let role = user?.userRole{
            if role == "rm_trade_partner" || role == "rm_main"{
                self.btnYes.isUserInteractionEnabled = false
                self.btnNo.isUserInteractionEnabled = false
                self.btnSubmit.isHidden = true
                self.btnAddAPicture.isHidden = true
            }
        }
    }
}
//MARK:- Image picker
extension MeetAndGreet{
    func uploadFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
        else{
            Utility.showAlert(title: "Error", message: "Camera not available")
        }
    }
    func uploadFromGallery(){
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
        
    }
    func uploadImage(){
        let alert = UIAlertController(title: "Upload Photo" , message: "How do you want to set your photo?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
            self.uploadFromCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (UIAlertAction) in
            self.uploadFromGallery()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
//MARK: - UIImagePickerControllerDelegate
extension MeetAndGreet: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.arrImages.append(pickedImage)
            self.collectionView.reloadData()
        }
        self.dismiss(animated: true, completion: nil)
    }
}
//MARK:- Services
extension MeetAndGreet{
    private func getMeetGreet(){
        Utility.showLoader()
        
        let bookingId = self.hotel.isPackageBooking ?
            (self.hotel.packageAccommodationId ?? "") : (self.hotel.bookingId ?? "0")
        
        let param: [String: Any] = [
            "booking_id":bookingId,
            "type": self.hotel.isPackageBooking ? "package" : "normal"
        ]
        
        APIManager.sharedInstance.usersAPIManager.MeetGreet(params: param, success: { (responseObject) in
            Utility.hideLoader()
            self.meetGreet = Mapper<MeetGreetModel>().map(JSON: responseObject) ?? MeetGreetModel()
            self.meetGreet.meetPicture.removeAll()
            let response = responseObject as NSDictionary
            if let images = response["meet_picture"] as? [String]{
                for image in images{
                    if let imgURL = URL(string: image){
                        self.meetGreet.meetPicture.append(image)
                        
                        let data = try? Data(contentsOf: imgURL)
                        if let imageData = data {
                            let image = UIImage(data: imageData)
                            self.arrImages.append(image!)
                        }
                    }
                }
            }
            self.setData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    
    private func postMeetAndGreet() {
        Utility.showLoader()

        let bookingId = self.hotel.isPackageBooking ?
            (self.hotel.packageAccommodationId ?? "") : (self.hotel.bookingId ?? "0")
        
        var params: [String: Any] = [
            "booking_id": bookingId,
            "meet_comments": self.tvOtherComments.text ?? "",
            "is_mandoob_meet": self.btnYes.isSelected ? "1" : "0",
            "type": self.hotel.isPackageBooking ? "package" : "normal"
        ]
        
        if self.meetGreet.meetId != nil{
            params["meet_id"] = (self.meetGreet.meetId ?? "")
        }
        
        for (index,image) in self.arrImages.enumerated(){
            params["meet_picture[\(index)]"] = UIImageJPEGRepresentation(image, 0.5)
        }
        
        APIManager.sharedInstance.usersAPIManager.PostMeetGreet(params: params, success: { (responseObject) in
            Utility.hideLoader()
            self.meetGreet = Mapper<MeetGreetModel>().map(JSON: responseObject) ?? MeetGreetModel()
            self.meetGreet.meetPicture.removeAll()
            self.arrImages.removeAll()
            let response = responseObject as NSDictionary
            if let images = response["meet_picture"] as? [String]{
                for image in images{
                    if let imgURL = URL(string: image){
                        self.meetGreet.meetPicture.append(image)
                        
                        let data = try? Data(contentsOf: imgURL)
                        
                        if let imageData = data {
                            let image = UIImage(data: imageData)
                            self.arrImages.append(image!)
                        }
                    }
                }
            }
            self.setData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}











