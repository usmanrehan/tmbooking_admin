//
//  BookingsTypeMakkahMadina.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 05/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftyUserDefaults
import DropDown
import FontAwesomeKit_Swift

enum BookingType{
    case makkah
    case madina
}
enum FilterType{
    case NoFilter
    case Gift
    case Transport
    case Hotel
}
enum ObjectType{
    case Gift
    case Transport
    case Hotel
}
enum StatusFilterType{
    case All
    case Confirmed
    case Processing
    case Delivered
    case Cancelled
    case Onhold
    case PendingPayment
}
struct CustomObjectParent {
    var arrCustomObject:[CustomObject]
    var isSelected:Bool = false
}
struct CustomObject {
    var objectType:ObjectType
    var object:Any
    var time:String
    var status:String
}

class BookingsTypeMakkahMadina: BaseViewController {
    
    @IBOutlet weak var btnMakkah: SelectUnSelectBookingButton!
    @IBOutlet weak var btnMadina: SelectUnSelectBookingButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnAll: SelectUnSelectFilterButton!
    @IBOutlet weak var btnGift: SelectUnSelectFilterButton!
    @IBOutlet weak var btnTransport: SelectUnSelectFilterButton!
    @IBOutlet weak var btnHotel: SelectUnSelectFilterButton!
    @IBOutlet weak var btnSelectStatus: SelectUnSelectFilterButton!
    
    var bookingType = BookingType.makkah
    var arrMakkahBookings = [BookingTypeModel]()
    var arrMadinaBookings = [BookingTypeModel]()
    
    var arrSortedMakkahBookings = [CustomObjectParent]()
    var arrSortedMadinaBookings = [CustomObjectParent]()
    
    var arrSortedGiftsWRTStatus = [CustomObjectParent]()
    var arrSortedTransportWRTStatus = [CustomObjectParent]()
    var arrSortedHotelWRTStatus = [CustomObjectParent]()
    
    var dates = ""
    var filterType = FilterType.NoFilter
    let arrStatusFilterType = ["All","Confirmed","Processing","Delivered","Cancelled","On hold","Pending payment"]
    var selectedStatusFilterType = StatusFilterType.All
    let dropDownStatus = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setData()
        self.setIcons()
        self.getBookings()
    }
    
    private func setData() {
        self.dropDownStatus.anchorView = self.btnSelectStatus
        self.dropDownStatus.dataSource = self.arrStatusFilterType
        self.dropDownStatus.selectionAction = { [unowned self] (index: Int, item: String) in
            self.onChangedBookingStatusWith(index: index, item: item)
        }
    }
    
    private func setIcons() {
        let buttonString = String.fa.fontAwesome(.gift)
        let buttonStringAttributed = NSMutableAttributedString(string: buttonString, attributes: nil)
        self.btnGift.setAttributedTitle(buttonStringAttributed, for: .normal)
    }
    
    private func onChangedBookingStatusWith(index: Int, item: String) {
        self.btnSelectStatus.setTitle(item, for: .selected)
        self.btnSelectStatus.isSelected = true
        self.btnAll.isSelected = true
        self.btnGift.isSelected = false
        self.btnHotel.isSelected = false
        self.btnTransport.isSelected = false
        self.filterType = .NoFilter
        
        switch index {
        case 0:
            self.selectedStatusFilterType = .All
        case 1:
            self.selectedStatusFilterType = .Confirmed
        case 2:
            self.selectedStatusFilterType = .Processing
        case 3:
            self.selectedStatusFilterType = .Delivered
        case 4:
            self.selectedStatusFilterType = .Cancelled
        case 5:
            self.selectedStatusFilterType = .Onhold
        case 6:
            self.selectedStatusFilterType = .PendingPayment
        default:
            break
        }
        
        switch self.bookingType{
        case .makkah:
            self.getSortedMakkahBookingsArrayWith(status: self.selectedStatusFilterType)
        case .madina:
            self.getSortedMadinaBookingsArrayWith(status: self.selectedStatusFilterType)
        }
    }
    
    @IBAction func onBtnMakkah(_ sender: SelectUnSelectBookingButton) {
        if !sender.isSelected{
            self.btnMakkah.isSelected = true
            self.btnMadina.isSelected = false
            self.bookingType = .makkah
        }
        self.tableView.reloadData()
    }
    
    @IBAction func onBtnMadina(_ sender: SelectUnSelectBookingButton) {
        if !sender.isSelected{
            self.btnMakkah.isSelected = false
            self.btnMadina.isSelected = true
            self.bookingType = .madina
        }
        self.tableView.reloadData()
    }
    
    @IBAction func onBtnAll(_ sender: SelectUnSelectFilterButton) {
        if !self.btnAll.isSelected{
            self.btnAll.isSelected = true
            self.btnGift.isSelected = false
            self.btnHotel.isSelected = false
            self.btnTransport.isSelected = false
            self.filterType = .NoFilter
            self.tableView.reloadData()
        }
    }
    
    @IBAction func onBtnGift(_ sender: SelectUnSelectFilterButton) {
        if !self.btnGift.isSelected{
            self.btnGift.isSelected = true
            self.btnHotel.isSelected = false
            self.btnTransport.isSelected = false
            self.btnAll.isSelected = false
            //self.btnSelectStatus.isSelected = false
            //self.btnSelectStatus.setTitle("Select status", for: .normal)
            //self.selectedStatusFilterType = .All
            self.filterType = .Gift
            self.getSortedGiftsArrayBySelectedStatus()
        }
//        else{
//            self.btnGift.isSelected = false
//            self.btnAll.isSelected = true
//            self.filterType = .NoFilter
//            switch self.bookingType{
//            case .makkah:
//                self.getSortedMakkahBookingsArrayWith(status: self.selectedStatusFilterType)
//            case .madina:
//                self.getSortedMadinaBookingsArrayWith(status: self.selectedStatusFilterType)
//            }
//        }
//        if !self.btnGift.isSelected && !self.btnHotel.isSelected && !self.btnTransport.isSelected{
//            self.filterType = .NoFilter
//            switch self.bookingType{
//            case .makkah:
//                self.getSortedMakkahBookingsArrayWith(status: .All)
//            case .madina:
//                self.getSortedMadinaBookingsArrayWith(status: .All)
//            }
//        }
    }
    
    @IBAction func onBtnTransport(_ sender: SelectUnSelectFilterButton) {
        if !self.btnTransport.isSelected{
            self.btnGift.isSelected = false
            self.btnHotel.isSelected = false
            self.btnTransport.isSelected = true
            self.btnAll.isSelected = false
            //self.btnSelectStatus.isSelected = false
            //self.btnSelectStatus.setTitle("Select status", for: .normal)
            //self.selectedStatusFilterType = .All
            self.filterType = .Transport
            self.getSortedTransportArrayBySelectedStatus()
        }
//        else{
//            self.btnTransport.isSelected = false
//            self.btnAll.isSelected = true
//            self.filterType = .NoFilter
//            switch self.bookingType{
//                case .makkah:
//                self.getSortedMakkahBookingsArrayWith(status: self.selectedStatusFilterType)
//            case .madina:
//                self.getSortedMadinaBookingsArrayWith(status: self.selectedStatusFilterType)
//            }
//
//        }
//        if !self.btnGift.isSelected && !self.btnHotel.isSelected && !self.btnTransport.isSelected{
//            self.filterType = .NoFilter
//            switch self.bookingType{
//            case .makkah:
//                self.getSortedMakkahBookingsArrayWith(status: .All)
//            case .madina:
//                self.getSortedMadinaBookingsArrayWith(status: .All)
//            }
//        }
    }
    
    @IBAction func onBtnHotel(_ sender: SelectUnSelectFilterButton) {
        if !self.btnHotel.isSelected{
            self.btnGift.isSelected = false
            self.btnHotel.isSelected = true
            self.btnTransport.isSelected = false
            self.btnAll.isSelected = false
            //self.btnSelectStatus.isSelected = false
            //self.btnSelectStatus.setTitle("Select status", for: .normal)
            //self.selectedStatusFilterType = .All
            self.filterType = .Hotel
            self.getSortedHotelArrayBySelectedStatus()
        }
//        else{
//            self.btnHotel.isSelected = false
//            self.btnAll.isSelected = true
//            self.filterType = .NoFilter
//            switch self.bookingType{
//            case .makkah:
//                self.getSortedMakkahBookingsArrayWith(status: self.selectedStatusFilterType)
//            case .madina:
//                self.getSortedMadinaBookingsArrayWith(status: self.selectedStatusFilterType)
//            }
//        }
//        if !self.btnGift.isSelected && !self.btnHotel.isSelected && !self.btnTransport.isSelected{
//            self.filterType = .NoFilter
//            switch self.bookingType{
//            case .makkah:
//                self.getSortedMakkahBookingsArrayWith(status: .All)
//            case .madina:
//                self.getSortedMadinaBookingsArrayWith(status: .All)
//            }
//        }
    }
    
    @IBAction func onBtnSelectStatus(_ sender: SelectUnSelectFilterButton) {
        self.dropDownStatus.show()
    }
}

//MARK:- UITableView
extension BookingsTypeMakkahMadina:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        switch self.bookingType {
        case .makkah:
            return self.arrMakkahBookings.count
        case .madina:
            return self.arrMadinaBookings.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.bookingType {
        case .makkah:
            switch self.filterType{
            case .NoFilter:
                if self.arrSortedMakkahBookings[section].isSelected {
                    return 0
                }
                return self.arrSortedMakkahBookings[section].arrCustomObject.count
            case .Gift:
                let deliveryCount = self.arrSortedGiftsWRTStatus[section].arrCustomObject.count//self.arrMakkahBookings[section].delivery.count
                if self.arrSortedGiftsWRTStatus[section].isSelected{//self.arrMakkahBookings[section].isSelected{
                    return 0
                }
                return deliveryCount
            case .Transport:
                let transportCount = self.arrSortedTransportWRTStatus[section].arrCustomObject.count//self.arrMakkahBookings[section].transport.count
                if self.arrSortedTransportWRTStatus[section].isSelected{//self.arrMakkahBookings[section].isSelected{
                    return 0
                }
                return transportCount
            case .Hotel:
                let hotelCount = self.arrSortedHotelWRTStatus[section].arrCustomObject.count//self.arrMakkahBookings[section].hotels.count
                if self.arrSortedHotelWRTStatus[section].isSelected{//self.arrMakkahBookings[section].isSelected{
                    return 0
                }
                return hotelCount
            }
        case .madina:
            switch self.filterType{
            case .NoFilter:
                if self.arrSortedMadinaBookings[section].isSelected{
                    return 0
                }
                return self.arrSortedMadinaBookings[section].arrCustomObject.count
            case .Gift:
                let deliveryCount = self.arrSortedGiftsWRTStatus[section].arrCustomObject.count//self.arrMadinaBookings[section].delivery.count
                if self.arrSortedGiftsWRTStatus[section].isSelected{//self.arrMadinaBookings[section].isSelected{
                    return 0
                }
                return deliveryCount
            case .Transport:
                let transportCount = self.arrSortedTransportWRTStatus[section].arrCustomObject.count//self.arrMadinaBookings[section].transport.count
                if self.arrSortedTransportWRTStatus[section].isSelected{//self.arrMadinaBookings[section].isSelected{
                    return 0
                }
                return transportCount
            case .Hotel:
                let hotelCount = self.arrSortedHotelWRTStatus[section].arrCustomObject.count//self.arrMadinaBookings[section].hotels.count
                if self.arrSortedHotelWRTStatus[section].isSelected{//self.arrMadinaBookings[section].isSelected{
                    return 0
                }
                return hotelCount
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.bookingType {
        case .makkah:
            switch self.filterType{
            case .NoFilter:
                let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTypeTVC", for: indexPath) as! BookingTypeTVC
                let obj = self.arrSortedMakkahBookings[indexPath.section].arrCustomObject[indexPath.row].object
                let objType = self.arrSortedMakkahBookings[indexPath.section].arrCustomObject[indexPath.row].objectType
                switch objType{
                case .Gift:
                    cell.setData(data: obj as! DeliveryModel)
                case .Transport:
                    cell.setData(data: obj as! TransportModel)
                case .Hotel:
                    cell.setData(data: obj as! HotelModel)
                }
                return cell
            case .Gift:
                let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTypeTVC", for: indexPath) as! BookingTypeTVC
                //cell.setData(data: self.arrMakkahBookings[indexPath.section].delivery[(indexPath.row)])
                cell.setData(data: self.arrSortedGiftsWRTStatus[indexPath.section].arrCustomObject[indexPath.row].object as! DeliveryModel)
                return cell
            case .Transport:
                let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTypeTVC", for: indexPath) as! BookingTypeTVC
                //cell.setData(data: self.arrMakkahBookings[indexPath.section].transport[(indexPath.row)])
                cell.setData(data: self.arrSortedTransportWRTStatus[indexPath.section].arrCustomObject[indexPath.row].object as! TransportModel)
                return cell
            case .Hotel:
                let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTypeTVC", for: indexPath) as! BookingTypeTVC
                //cell.setData(data: self.arrMakkahBookings[indexPath.section].hotels[(indexPath.row)])
                cell.setData(data: self.arrSortedHotelWRTStatus[indexPath.section].arrCustomObject[indexPath.row].object as! HotelModel)
                return cell
            }
        case .madina:
            switch self.filterType{
            case .NoFilter:
                let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTypeTVC", for: indexPath) as! BookingTypeTVC
                let obj = self.arrSortedMadinaBookings[indexPath.section].arrCustomObject[indexPath.row].object
                let objType = self.arrSortedMadinaBookings[indexPath.section].arrCustomObject[indexPath.row].objectType
                switch objType{
                case .Gift:
                    cell.setData(data: obj as! DeliveryModel)
                case .Transport:
                    cell.setData(data: obj as! TransportModel)
                case .Hotel:
                    cell.setData(data: obj as! HotelModel)
                }
                return cell
            case .Gift:
                let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTypeTVC", for: indexPath) as! BookingTypeTVC
                //cell.setData(data: self.arrMadinaBookings[indexPath.section].delivery[(indexPath.row)])
                cell.setData(data: self.arrSortedGiftsWRTStatus[indexPath.section].arrCustomObject[indexPath.row].object as! DeliveryModel)
                return cell
            case .Transport:
                let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTypeTVC", for: indexPath) as! BookingTypeTVC
                //cell.setData(data: self.arrMadinaBookings[indexPath.section].transport[(indexPath.row)])
                cell.setData(data: self.arrSortedTransportWRTStatus[indexPath.section].arrCustomObject[indexPath.row].object as! TransportModel)
                return cell
            case .Hotel:
                let cell = tableView.dequeueReusableCell(withIdentifier: "BookingTypeTVC", for: indexPath) as! BookingTypeTVC
                //cell.setData(data: self.arrMadinaBookings[indexPath.section].hotels[(indexPath.row)])
                cell.setData(data: self.arrSortedHotelWRTStatus[indexPath.section].arrCustomObject[indexPath.row].object as! HotelModel)
                return cell
            }
        }
    }
}
//MARK:- UITableView
extension BookingsTypeMakkahMadina:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = HeaderDate.instanceFromNib() as! HeaderDate
        header.btnHeader.tag = section
        header.btnHeader.addTarget(self, action: #selector(self.onBtnHeader(sender:)), for: .touchUpInside)
        switch self.bookingType {
        case .makkah:
            header.lblDate.text = self.arrMakkahBookings[section].selectedData ?? "-"
            return header
        case .madina:
            header.lblDate.text = self.arrMadinaBookings[section].selectedData ?? "-"
            return header
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 16.0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    @objc func onBtnHeader(sender:UIButton){
//        if self.filterType == .NoFilter{
//            switch self.bookingType {
//            case .makkah:
//                self.arrSortedMakkahBookings[sender.tag].isSelected = !self.arrSortedMakkahBookings[sender.tag].isSelected
//            case .madina:
//                self.arrSortedMadinaBookings[sender.tag].isSelected = !self.arrSortedMadinaBookings[sender.tag].isSelected
//            }
//            self.tableView.reloadSections([sender.tag], with: .automatic)
//        }
//        else{
//            switch self.bookingType {
//            case .makkah:
//                self.arrMakkahBookings[sender.tag].isSelected = !self.arrMakkahBookings[sender.tag].isSelected
//            case .madina:
//                self.arrMadinaBookings[sender.tag].isSelected = !self.arrMadinaBookings[sender.tag].isSelected
//            }
//            self.tableView.reloadSections([sender.tag], with: .automatic)
//        }
        switch self.filterType{
        case .NoFilter:
            switch self.bookingType {
            case .makkah:
                self.arrSortedMakkahBookings[sender.tag].isSelected = !self.arrSortedMakkahBookings[sender.tag].isSelected
            case .madina:
                self.arrSortedMadinaBookings[sender.tag].isSelected = !self.arrSortedMadinaBookings[sender.tag].isSelected
            }
        case .Gift:
            self.arrSortedGiftsWRTStatus[sender.tag].isSelected = !self.arrSortedGiftsWRTStatus[sender.tag].isSelected
        case .Transport:
            self.arrSortedTransportWRTStatus[sender.tag].isSelected = !self.arrSortedTransportWRTStatus[sender.tag].isSelected
        case .Hotel:
            self.arrSortedHotelWRTStatus[sender.tag].isSelected = !self.arrSortedHotelWRTStatus[sender.tag].isSelected
        }
        self.tableView.reloadSections([sender.tag], with: .automatic)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch self.bookingType {
        case .makkah:
            switch self.filterType{
            case .NoFilter:
                let obj = self.arrSortedMakkahBookings[indexPath.section].arrCustomObject[indexPath.row].object
                let objType = self.arrSortedMakkahBookings[indexPath.section].arrCustomObject[indexPath.row].objectType
                switch objType{
                case .Gift:
                    self.pushToGiftDelivery(gift: obj as! DeliveryModel)
                case .Transport:
                    self.pushToGiftViewBooking(transport: obj as! TransportModel)
                case .Hotel:
                    self.pushToHotelDetails(hotel: obj as! HotelModel)
                }
            case .Gift:
                //let gift = self.arrMakkahBookings[indexPath.section].delivery[(indexPath.row)]
                let gift = self.arrSortedGiftsWRTStatus[indexPath.section].arrCustomObject[indexPath.row].object as! DeliveryModel
                self.pushToGiftDelivery(gift: gift)
            case .Transport:
                //let transport = self.arrMakkahBookings[indexPath.section].transport[(indexPath.row)]
                let transport = self.arrSortedTransportWRTStatus[indexPath.section].arrCustomObject[indexPath.row].object as! TransportModel
                self.pushToGiftViewBooking(transport: transport)
            case .Hotel:
                //let hotel = self.arrMakkahBookings[indexPath.section].hotels[indexPath.row]
                let hotel = self.arrSortedHotelWRTStatus[indexPath.section].arrCustomObject[indexPath.row].object as! HotelModel
                self.pushToHotelDetails(hotel: hotel)
            }
        case .madina:
            switch self.filterType{
            case .NoFilter:
                let obj = self.arrSortedMadinaBookings[indexPath.section].arrCustomObject[indexPath.row].object
                let objType = self.arrSortedMadinaBookings[indexPath.section].arrCustomObject[indexPath.row].objectType
                switch objType{
                case .Gift:
                    self.pushToGiftDelivery(gift: obj as! DeliveryModel)
                case .Transport:
                    self.pushToGiftViewBooking(transport: obj as! TransportModel)
                case .Hotel:
                    self.pushToHotelDetails(hotel: obj as! HotelModel)
                }
            case .Gift:
                //let gift = self.arrMadinaBookings[indexPath.section].delivery[(indexPath.row)]
                let gift = self.arrSortedGiftsWRTStatus[indexPath.section].arrCustomObject[indexPath.row].object as! DeliveryModel
                self.pushToGiftDelivery(gift: gift)
            case .Transport:
                //let transport = self.arrMadinaBookings[indexPath.section].transport[(indexPath.row)]
                let transport = self.arrSortedTransportWRTStatus[indexPath.section].arrCustomObject[indexPath.row].object as! TransportModel
                self.pushToGiftViewBooking(transport: transport)
            case .Hotel:
                //let hotel = self.arrMadinaBookings[indexPath.section].hotels[indexPath.row]
                let hotel = self.arrSortedHotelWRTStatus[indexPath.section].arrCustomObject[indexPath.row].object as! HotelModel
                self.pushToHotelDetails(hotel: hotel)
            }
        }
    }
}
//MARK:- Helper Methods
extension BookingsTypeMakkahMadina{
    private func pushToGiftDelivery(gift:DeliveryModel){
        let storyboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "GiftDelivery") as? GiftDelivery{
            controller.gift = gift
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    private func pushToGiftViewBooking(transport:TransportModel){
        let storyboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "ViewBookingDetail") as? ViewBookingDetail{
            controller.transport = transport
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    private func pushToHotelDetails(hotel:HotelModel){
        let storyboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "HotelDetails") as? HotelDetails{
            controller.hotel = hotel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    private func getSortedMakkahBookingsArray(){
        var arrAllSortedBookings = [CustomObjectParent]()
        for item in self.arrMakkahBookings{
            let hotels = item.hotels
            let gifts = item.delivery
            let transport = item.transport
            var arrCustomObject = [CustomObject]()
            for i in hotels{
                arrCustomObject.append(CustomObject(objectType: .Hotel, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
            }
            for i in gifts{
                arrCustomObject.append(CustomObject(objectType: .Gift, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
            }
            for i in transport{
                arrCustomObject.append(CustomObject(objectType: .Transport, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
            }
            let tempArray = arrCustomObject.sorted(by: { Int($0.time)! < Int($1.time)! })
            arrAllSortedBookings.append(CustomObjectParent(arrCustomObject: tempArray, isSelected: false))
        }
        self.arrSortedMakkahBookings = arrAllSortedBookings
        self.tableView.reloadData()
    }
    private func getSortedMadinaBookingsArray(){
        var arrAllSortedBookings = [CustomObjectParent]()
        for item in self.arrMadinaBookings{
            let hotels = item.hotels
            let gifts = item.delivery
            let transport = item.transport
            var arrCustomObject = [CustomObject]()
            for i in hotels{
                arrCustomObject.append(CustomObject(objectType: .Hotel, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
            }
            for i in gifts{
                arrCustomObject.append(CustomObject(objectType: .Gift, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
            }
            for i in transport{
                arrCustomObject.append(CustomObject(objectType: .Transport, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
            }
            let tempArray = arrCustomObject.sorted(by: { Int($0.time)! < Int($1.time)! })
            arrAllSortedBookings.append(CustomObjectParent(arrCustomObject: tempArray, isSelected: false))
        }
        self.arrSortedMadinaBookings = arrAllSortedBookings
        self.tableView.reloadData()
    }
    private func getSortedMakkahBookingsArrayWith(status:StatusFilterType){
        var status_key = ""
        switch status {
        case .All:
            self.getSortedMakkahBookingsArray()
            return
        case .Confirmed:
            status_key = "Confirmed"
        case .Processing:
            status_key = "Processing"
        case .Delivered:
            status_key = "Delivered"
        case .Cancelled:
            status_key = "Cancelled"
        case .Onhold:
            status_key = "On hold"
        case .PendingPayment:
            status_key = "Pending payment"
        }
        var arrAllSortedBookings = [CustomObjectParent]()
        for item in self.arrMakkahBookings{
            let hotels = item.hotels
            let gifts = item.delivery
            let transport = item.transport
            var arrCustomObject = [CustomObject]()
            for i in hotels{
                arrCustomObject.append(CustomObject(objectType: .Hotel, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
            }
            for i in gifts{
                arrCustomObject.append(CustomObject(objectType: .Gift, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
            }
            for i in transport{
                arrCustomObject.append(CustomObject(objectType: .Transport, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
            }
            let tempSortedArray = arrCustomObject.sorted(by: { Int($0.time)! < Int($1.time)! })
            let tempFilteredArray = tempSortedArray.filter{$0.status == status_key}
            arrAllSortedBookings.append(CustomObjectParent(arrCustomObject: tempFilteredArray, isSelected: false))
        }
        self.arrSortedMakkahBookings = arrAllSortedBookings
        self.tableView.reloadData()
        
    }
    private func getSortedMadinaBookingsArrayWith(status:StatusFilterType){
        var status_key = ""
        switch status {
        case .All:
            self.getSortedMadinaBookingsArray()
            return
        case .Confirmed:
            status_key = "Confirmed"
        case .Processing:
            status_key = "Processing"
        case .Delivered:
            status_key = "Delivered"
        case .Cancelled:
            status_key = "Cancelled"
        case .Onhold:
            status_key = "On hold"
        case .PendingPayment:
            status_key = "Pending payment"
        }
        var arrAllSortedBookings = [CustomObjectParent]()
        for item in self.arrMadinaBookings{
            let hotels = item.hotels
            let gifts = item.delivery
            let transport = item.transport
            var arrCustomObject = [CustomObject]()
            for i in hotels{
                arrCustomObject.append(CustomObject(objectType: .Hotel, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
            }
            for i in gifts{
                arrCustomObject.append(CustomObject(objectType: .Gift, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
            }
            for i in transport{
                arrCustomObject.append(CustomObject(objectType: .Transport, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
            }
            let tempSortedArray = arrCustomObject.sorted(by: { Int($0.time)! < Int($1.time)! })
            let tempFilteredArray = tempSortedArray.filter{$0.status == status_key}
            arrAllSortedBookings.append(CustomObjectParent(arrCustomObject: tempFilteredArray, isSelected: false))
        }
        self.arrSortedMadinaBookings = arrAllSortedBookings
        self.tableView.reloadData()
        
    }
    
    private func getSortedGiftsArrayBySelectedStatus(){
        var status_key = ""
        switch self.selectedStatusFilterType {
        case .All:
            status_key = "All"
        case .Confirmed:
            status_key = "Confirmed"
        case .Processing:
            status_key = "Processing"
        case .Delivered:
            status_key = "Delivered"
        case .Cancelled:
            status_key = "Cancelled"
        case .Onhold:
            status_key = "On hold"
        case .PendingPayment:
            status_key = "Pending payment"
        }
        var arrAllSortedBookings = [CustomObjectParent]()
        switch self.bookingType {
        case .makkah:
            for item in self.arrMakkahBookings{
                let gifts = item.delivery
                var arrCustomObject = [CustomObject]()
                for i in gifts{
                    arrCustomObject.append(CustomObject(objectType: .Gift, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
                }
                let tempSortedArray = arrCustomObject.sorted(by: { Int($0.time)! < Int($1.time)! })
                var tempFilteredArray = [CustomObject]()
                if status_key != "All"{
                    tempFilteredArray = tempSortedArray.filter{$0.status == status_key}
                }
                arrAllSortedBookings.append(CustomObjectParent(arrCustomObject: tempFilteredArray, isSelected: false))
            }
        case .madina:
            for item in self.arrMadinaBookings{
                let gifts = item.delivery
                var arrCustomObject = [CustomObject]()
                for i in gifts{
                    arrCustomObject.append(CustomObject(objectType: .Gift, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
                }
                let tempSortedArray = arrCustomObject.sorted(by: { Int($0.time)! < Int($1.time)! })
                var tempFilteredArray = [CustomObject]()
                if status_key != "All"{
                    tempFilteredArray = tempSortedArray.filter{$0.status == status_key}
                }
                arrAllSortedBookings.append(CustomObjectParent(arrCustomObject: tempFilteredArray, isSelected: false))
            }
        }
        
        self.arrSortedGiftsWRTStatus = arrAllSortedBookings
        self.tableView.reloadData()
    }
    private func getSortedHotelArrayBySelectedStatus(){
        var status_key = ""
        switch self.selectedStatusFilterType {
        case .All:
            status_key = "All"
        case .Confirmed:
            status_key = "Confirmed"
        case .Processing:
            status_key = "Processing"
        case .Delivered:
            status_key = "Delivered"
        case .Cancelled:
            status_key = "Cancelled"
        case .Onhold:
            status_key = "On hold"
        case .PendingPayment:
            status_key = "Pending payment"
        }
        var arrAllSortedBookings = [CustomObjectParent]()
        switch self.bookingType{
        case .makkah:
            for item in self.arrMakkahBookings{
                let hotels = item.hotels
                var arrCustomObject = [CustomObject]()
                for i in hotels{
                    arrCustomObject.append(CustomObject(objectType: .Hotel, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
                }
                let tempSortedArray = arrCustomObject.sorted(by: { Int($0.time)! < Int($1.time)! })
                var tempFilteredArray = [CustomObject]()
                if status_key != "All"{
                    tempFilteredArray = tempSortedArray.filter{$0.status == status_key}
                    arrAllSortedBookings.append(CustomObjectParent(arrCustomObject: tempFilteredArray, isSelected: false))
                }
                else{
                    arrAllSortedBookings.append(CustomObjectParent(arrCustomObject: tempSortedArray, isSelected: false))
                }
            }
        case .madina:
            for item in self.arrMadinaBookings{
                let hotels = item.hotels
                var arrCustomObject = [CustomObject]()
                for i in hotels{
                    arrCustomObject.append(CustomObject(objectType: .Hotel, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
                }
                let tempSortedArray = arrCustomObject.sorted(by: { Int($0.time)! < Int($1.time)! })
                var tempFilteredArray = [CustomObject]()
                if status_key != "All"{
                    tempFilteredArray = tempSortedArray.filter{$0.status == status_key}
                    arrAllSortedBookings.append(CustomObjectParent(arrCustomObject: tempFilteredArray, isSelected: false))
                }
                else{
                    arrAllSortedBookings.append(CustomObjectParent(arrCustomObject: tempSortedArray, isSelected: false))
                }
            }
        }
        self.arrSortedHotelWRTStatus = arrAllSortedBookings
        self.tableView.reloadData()
    }
    private func getSortedTransportArrayBySelectedStatus(){
        var status_key = ""
        switch self.selectedStatusFilterType {
        case .All:
            status_key = "All"
        case .Confirmed:
            status_key = "Confirmed"
        case .Processing:
            status_key = "Processing"
        case .Delivered:
            status_key = "Delivered"
        case .Cancelled:
            status_key = "Cancelled"
        case .Onhold:
            status_key = "On hold"
        case .PendingPayment:
            status_key = "Pending payment"
        }
        var arrAllSortedBookings = [CustomObjectParent]()
        switch self.bookingType{
        case .makkah:
            for item in self.arrMakkahBookings{
                let transport = item.transport
                var arrCustomObject = [CustomObject]()
                for i in transport{
                    arrCustomObject.append(CustomObject(objectType: .Transport, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
                }
                let tempSortedArray = arrCustomObject.sorted(by: { Int($0.time)! < Int($1.time)! })
                var tempFilteredArray = [CustomObject]()
                if status_key != "All"{
                    tempFilteredArray = tempSortedArray.filter{$0.status == status_key}
                    arrAllSortedBookings.append(CustomObjectParent(arrCustomObject: tempFilteredArray, isSelected: false))
                }
                else{
                    arrAllSortedBookings.append(CustomObjectParent(arrCustomObject: tempSortedArray, isSelected: false))
                }
            }
        case .madina:
            for item in self.arrMadinaBookings{
                let transport = item.transport
                var arrCustomObject = [CustomObject]()
                for i in transport{
                    arrCustomObject.append(CustomObject(objectType: .Transport, object: i, time: (i.time ?? "00:00").digits, status: i.status ?? "All"))
                }
                let tempSortedArray = arrCustomObject.sorted(by: { Int($0.time)! < Int($1.time)! })
                var tempFilteredArray = [CustomObject]()
                if status_key != "All"{
                    tempFilteredArray = tempSortedArray.filter{$0.status == status_key}
                    arrAllSortedBookings.append(CustomObjectParent(arrCustomObject: tempFilteredArray, isSelected: false))
                }
                else{
                    arrAllSortedBookings.append(CustomObjectParent(arrCustomObject: tempSortedArray, isSelected: false))
                }
            }
        }
        self.arrSortedTransportWRTStatus = arrAllSortedBookings
        self.tableView.reloadData()
    }
}
//MARK:- Services
extension BookingsTypeMakkahMadina{
    private func getBookings() {
        let dates = self.dates
        let user_id = Defaults[.loginUserId]
        let params:[String:Any] = ["dates":dates,
                                   "user_id":user_id]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.GetListBookings(params: params, success: { (responseObject) in
            Utility.hideLoader()
            print(responseObject)
            let response = responseObject as NSDictionary
            
            if let madinaBookings = response["madina"] as? [[String : Any]]{
                self.arrMadinaBookings = Mapper<BookingTypeModel>().mapArray(JSONArray: madinaBookings)
            }
            
            if let makkahBookings = response["makkah"] as? [[String : Any]]{
                self.arrMakkahBookings = Mapper<BookingTypeModel>().mapArray(JSONArray: makkahBookings)
            }
            
            self.getSortedMakkahBookingsArray()
            self.getSortedMadinaBookingsArray()
            
            let user = Mapper<User>().map(JSONObject: Utility.convertToDictionary(text: (Defaults[.loginData]?.toString())!))
            if let role = user?.userRole {
                switch role {
                case "mandoob_makkah":
                    self.bookingType = .makkah
                    self.btnMakkah.isSelected = true
                    self.btnMadina.isHidden = true
                    self.getSortedMakkahBookingsArray()
                case "mandoob_madina":
                    self.bookingType = .madina
                    self.btnMadina.isSelected = true
                    self.btnMakkah.isHidden = true
                    self.getSortedMadinaBookingsArray()
                default:
                    break
                }
            }            
        }) { (error) in
            print(error.localizedDescription)
            Utility.hideLoader()
        }
    }
}
extension String {
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }
}
