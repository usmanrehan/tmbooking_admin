//
//  ConfirmationList.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 17/06/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit

enum PurchaseOrderType {
	case transport
	case accomodation
}

class ConfirmationList: BaseViewController {
    @IBOutlet weak var noRecordFoundLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var bookingId: String?
	var isPackageBookingType: Bool = false
	var confirmationType: PurchaseOrderType = .accomodation
    var arrPurchaseOrders: [PurchaseOrderModel] = Array()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getPurchaseOrders()
    }
	
	@IBAction func onBtnAddPurchaseOrder(_ sender: AnyObject) {
		self.pushToAddConfirmation()
	}
	
	// MARK: - API Methods
	
	private func getPurchaseOrders() {
		Utility.showLoader()
		let params: [String: Any] = [
			"id": self.bookingId ?? "0",
			"type": self.isPackageBookingType ? "package" : "normal"
		]
		
		if confirmationType == .transport {
			APIManager.sharedInstance.apiManagerServicesWordPress.getTransportPurchaseOrders(params: params, success: { (response) in
				Utility.hideLoader()
				self.onSuccessResponse(response)
			}) { (error) in
				Utility.hideLoader()
				Utility.showAlert(title: "Error", message: error.localizedDescription)
			}
		} else {
			APIManager.sharedInstance.apiManagerServicesWordPress.getPurchaseOrders(params: params, success: { (response) in
				Utility.hideLoader()
				self.onSuccessResponse(response)
			}) { (error) in
				Utility.hideLoader()
				Utility.showAlert(title: "Error", message: error.localizedDescription)
			}
		}
	}
	
	private func onSuccessResponse(_ response: AnyObject) {
		if let data = response as? [AnyObject] {
			self.arrPurchaseOrders.removeAll()
			for item in data {
				let model = PurchaseOrderModel(JSON: item as! [String: Any])
				self.arrPurchaseOrders.append(model!)
			}
			self.tableView.reloadData()
		}
	}

    // MARK: - Navigation
    
    private func pushToAddConfirmation() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddConfirmation") as? AddConfirmation {
            controller.bookingId = self.bookingId
			controller.isPackageBookingType = self.isPackageBookingType
			controller.confirmationType = self.confirmationType
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    private func pushToUpdateConfirmation(_ indexPath: IndexPath) {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "AddConfirmation") as? AddConfirmation {
			controller.isEditingMode = true
            controller.bookingId = self.bookingId
			controller.isPackageBookingType = self.isPackageBookingType
			controller.confirmationType = self.confirmationType
            controller.purchaseOrder = self.arrPurchaseOrders[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
}

extension ConfirmationList: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.noRecordFoundLabel.isHidden = (self.arrPurchaseOrders.count == 0) ? false : true
        return self.arrPurchaseOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if confirmationType == .transport {
			let cell = tableView.dequeueReusableCell(withIdentifier: "TransportConfirmationListCellIdentifier", for: indexPath) as! ConfirmationListViewCell
			cell.setTransportData(self.arrPurchaseOrders[indexPath.row])
			return cell
		} else {
			let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmationListCellIdentifier", for: indexPath) as! ConfirmationListViewCell
			cell.setData(self.arrPurchaseOrders[indexPath.row])
			return cell
		}
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pushToUpdateConfirmation(indexPath)
    }
}
