//
//  AddConfirmation.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 16/06/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import DropDown
import TagListView
import ALCameraViewController

class AddConfirmation: BaseViewController {
    @IBOutlet weak var checkinDateTextField: UITextField!
    @IBOutlet weak var checkoutDateTextField: UITextField!
    @IBOutlet weak var vendorTextField: UITextField!
    @IBOutlet weak var contactPersonTextField: UITextField!
    @IBOutlet weak var accountTextField: UITextField!
    @IBOutlet weak var confirmationTextField: UITextField!
    @IBOutlet weak var internalConfirmationTextField: UITextField!
    @IBOutlet weak var externalConfirmationTextField: UITextField!
    @IBOutlet weak var purchasePriceTextField: UITextField!
	@IBOutlet weak var vatTextField: UITextField!
	@IBOutlet weak var munTaxTextField: UITextField!
    @IBOutlet weak var memoTextView: UITextView!
    @IBOutlet weak var vendorMessageTextView: UITextView!
    @IBOutlet weak var documentImageView: UIImageView!
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var tagListWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tagListContainerView: UIView!
    @IBOutlet weak var contactPersonView: UIView!
    @IBOutlet weak var documentView: UIView!
    @IBOutlet weak var submitButton: UIButton!
	@IBOutlet weak var exclusiveTaxButton: UIButton!
	@IBOutlet weak var checkoutDateView: UIView!
    
    var activeField: UITextField?
    var documentImage: UIImage?
    var vendorDropDown: DropDown!
    var accountDropDown: DropDown!
    var contactPersonDropDown: DropDown!
    
    var arrVendors: [VendorModel] = Array()
    var arrAccounts: [AccountModel] = Array()
    
    var isEditingMode: Bool = false
	var isPackageBookingType: Bool = false
	var confirmationType: PurchaseOrderType = .accomodation

	var bookingId: String?
    var purchaseOrder: PurchaseOrderModel?
    var selectedVender: VendorModel?
    var selectedAccount: AccountModel?
    var selectedPersons: [Int] = Array()
	
	let vatPercentage = 5.0
	let munTaxPercentage = 5.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setTagListView()
        self.setDatePicker(checkinDateTextField)
        self.setDatePicker(checkoutDateTextField)
        
        self.vendorDropDown = setDropDownTextField(vendorTextField, dataSource: [])
        self.accountDropDown = setDropDownTextField(accountTextField, dataSource: [])
        self.contactPersonDropDown = setDropDownTextField(contactPersonTextField, dataSource: [])
        
        self.setData()
        self.updateUI()
        
        if self.isEditingMode == false {
            self.getPurchaseOrderFields()
        }
    }
    
    private func setDatePicker(_ textField: UITextField) {
        textField.delegate = self
        
        if let image = UIImage(named: "icon_calendar") {
            textField.addPaddingRightIcon(image, padding: 8.0)
        }
        
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
        
        textField.inputView = picker
    }
    
    private func setDropDownTextField(_ textField: UITextField, dataSource: [String]) -> DropDown {
        textField.delegate = self
        
        if let image = UIImage(named: "ic_dropdown") {
            textField.addPaddingRightIcon(image, padding: 8.0)
        }
        
        let dropdown = DropDown()
        dropdown.dataSource = dataSource
        dropdown.anchorView = textField
        dropdown.bottomOffset = CGPoint(x: 0, y: (dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.selectionAction = { (index: Int, item: String) in
            switch textField {
            case self.vendorTextField:
                self.onChangedVendorSelection(index, item: item)
                break
            case self.accountTextField:
                self.accountTextField.text = item
                self.selectedAccount = self.arrAccounts[index]
                break
            case self.contactPersonTextField:
                self.onSelectedContactPerson(index, item: item)
                break
            default:
                break
            }
        }
        
        return dropdown
    }
    
    private func setTagListView() {
        self.tagListView.delegate = self
        self.tagListView.enableRemoveButton = !(self.isEditingMode)
        self.tagListView.textFont = UIFont.systemFont(ofSize: 12)
    }
    
    private func setData() {
        if let data = self.purchaseOrder {
			self.checkinDateTextField.text = (self.confirmationType == .transport) ? data.pickupDate : data.checkinDate
            self.checkoutDateTextField.text = data.checkoutDate
            self.vendorTextField.text = data.vendorName
            self.accountTextField.text = data.accountName
            self.confirmationTextField.text = data.confirmationNumber
            self.internalConfirmationTextField.text = data.internalConfirmationNumber
            self.externalConfirmationTextField.text = data.externalConfirmationNumber
            self.purchasePriceTextField.text = data.purchasePrice
            self.memoTextView.text = data.memo
            self.vendorMessageTextView.text = data.venderMessage
			
			// Update tax prices
			self.calculateInclusiveTax(data.purchasePrice)
            
            if let contactPersons = data.contactPersons {
                for item in contactPersons {
                    self.tagListView.addTag(item.name ?? "")
                }
                
                self.updateTagListWidthConstraint()
            }
        }
    }
    
    private func setVendorDropDown() {
        self.vendorTextField.delegate = self
        
        if let image = UIImage(named: "ic_dropdown") {
            self.vendorTextField.addPaddingRightIcon(image, padding: 8.0)
        }
        
        let dropdown = DropDown()
        dropdown.anchorView = vendorTextField
        dropdown.dataSource = ["medina office"]
        dropdown.bottomOffset = CGPoint(x: 0, y: (dropdown.anchorView?.plainView.bounds.height)!)
        dropdown.selectionAction = { (index: Int, item: String) in
            self.vendorTextField.text = item
        }
        
        self.vendorDropDown = dropdown
    }
    
    private func updateUI() {
		if self.confirmationType == .transport {
			self.checkinDateTextField.placeholder = "Pickup Date"
			self.checkoutDateView.isHidden = true
		} else {
			self.checkinDateTextField.placeholder = "Checkin Date"
			self.checkoutDateView.isHidden = false
		}
				
        if self.isEditingMode == true {
            self.contactPersonView.isHidden = true
            self.documentView.isHidden = true
            self.submitButton.isHidden = true
            
            self.checkinDateTextField.isUserInteractionEnabled = false
            self.checkoutDateTextField.isUserInteractionEnabled = false
            self.vendorTextField.isUserInteractionEnabled = false
            self.contactPersonTextField.isUserInteractionEnabled = false
            self.accountTextField.isUserInteractionEnabled = false
            self.confirmationTextField.isUserInteractionEnabled = false
            self.internalConfirmationTextField.isUserInteractionEnabled = false
            self.externalConfirmationTextField.isUserInteractionEnabled = false
            self.purchasePriceTextField.isUserInteractionEnabled = false
            self.memoTextView.isUserInteractionEnabled = false
            self.vendorMessageTextView.isUserInteractionEnabled = false
        }
    }
    
    private func updateTagListWidthConstraint() {
        let width = tagListView.tagViews.map { $0.frame.width }.reduce(0, +)
        self.tagListWidthConstraint.constant = max(width + CGFloat(10 * tagListView.tagViews.count), self.tagListContainerView.frame.size.width)
        view.layoutIfNeeded()
        
        // To hide/show TagListView
        if self.tagListView.tagViews.count == 0 {
            self.tagListContainerView.isHidden = true
        } else {
            self.tagListContainerView.isHidden = false
        }
    }
    
    private func showImagePicker(_ sender: UIButton) {
        let croppingParams = CroppingParameters(isEnabled: true, allowResizing: true, allowMoving: true, minimumSize: .zero)

        let imagePicker = CameraViewController.imagePickerViewController(croppingParameters: croppingParams) { [weak self] image, asset in
            // Do something with your image here.
            // If cropping is enabled this image will be the cropped version
            self?.dismiss(animated: true, completion: {
                self?.documentImage = image
                self?.documentImageView.image = image
            })
        }

        self.present(imagePicker, animated: true, completion: nil)
    }
    
    private func validatePurchaseOrderFields() -> Bool {
        if (self.checkinDateTextField.text?.isEmpty)! || (self.checkoutDateTextField.text?.isEmpty)! {
            Utility.showAlert(title: "Message", message: "Checkin or Checkout dates are not provided")
            return false
        }

        if (self.purchasePriceTextField.text?.isEmpty)! {
            Utility.showAlert(title: "Message", message: "Purchase price not provided")
            return false
        }
        
        if self.selectedVender == nil {
            Utility.showAlert(title: "Message", message: "Vendor not selected")
            return false
        }
        
        if self.selectedAccount == nil {
            Utility.showAlert(title: "Message", message: "Account not selected")
            return false
        }
        
        return true
    }
	
	private func validateTransportPurchaseOrderFields() -> Bool {
		if (self.checkinDateTextField.text?.isEmpty)! {
			Utility.showAlert(title: "Message", message: "pickup date not provided")
			return false
		}
		
		if (self.purchasePriceTextField.text?.isEmpty)! {
			Utility.showAlert(title: "Message", message: "Purchase price not provided")
			return false
		}
		
		if self.selectedVender == nil {
			Utility.showAlert(title: "Message", message: "Vendor not selected")
			return false
		}
		
		if self.selectedAccount == nil {
			Utility.showAlert(title: "Message", message: "Account not selected")
			return false
		}
		
		return true
	}
	
	private func calculateInclusiveTax(_ purchasePrice: String?) {
		var price = Double(purchasePrice ?? "0") ?? 0
		
		if price >= 0.0 {
			let vat = price * (self.vatPercentage / ( 100.0 + self.vatPercentage))
			self.vatTextField.text = String(format: "%.2f", vat)
			
			price = price + vat
			let munTax = price * (self.munTaxPercentage / ( 100 + self.munTaxPercentage))
			self.munTaxTextField.text = String(format: "%.2f", munTax)
		}
	}
	
	private func calculateExclusiveTax(_ purchasePrice: String?) {
		var price = Double(purchasePrice ?? "0") ?? 0
		
		if price >= 0.0 {
			let vat = (price / 100.0) * self.vatPercentage
			self.vatTextField.text = String(format: "%.2f", vat)
			
			price = price + vat
			let munTax = (price / 100.0) * self.munTaxPercentage
			self.munTaxTextField.text = String(format: "%.2f", munTax)
		}
	}
    
    private func onChangedVendorSelection(_ index: Int, item: String) {
        self.vendorTextField.text = item
        self.selectedVender = self.arrVendors[index]
        self.contactPersonDropDown.dataSource = self.selectedVender!.contactPersons!.map {$0.name ?? ""}

        self.tagListView.removeAllTags()
        self.selectedPersons.removeAll()
        self.updateTagListWidthConstraint()
    }
    
    private func onSelectedContactPerson(_ index: Int, item: String) {
        if let contactPerson = self.selectedVender?.contactPersons?[index] {
            guard !self.selectedPersons.contains(contactPerson.id) else { return }
            
            self.selectedPersons.append(contactPerson.id)
            
            let tagView = self.tagListView.addTag(item)
            tagView.tag = self.tagListView.tagViews.count - 1
            
            self.updateTagListWidthConstraint()
        }
    }
    
    @objc private func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        if let textField = self.activeField {
            textField.text = dateFormatter.string(from: sender.date)
        }
    }
    
    @IBAction func onBtnAddImage(_ sender: UIButton) {
        self.showImagePicker(sender)
    }
    
    @IBAction func onBtnSubmit(_ sender: UIButton) {
		guard self.isEditingMode == false else { return }
		
		if self.confirmationType == .transport {
			guard self.validateTransportPurchaseOrderFields() else { return }
			self.addTransportPurchaseOrder()
		} else {
			guard self.validatePurchaseOrderFields() else { return }
			self.addPurchaseOrder()
		}
    }
	
	@IBAction func onBtnExclusiveTax(_ sender: UIButton) {
		sender.isSelected = !sender.isSelected
		
		if sender.isSelected {
			self.calculateExclusiveTax(self.purchasePriceTextField.text)
		} else {
			self.calculateInclusiveTax(self.purchasePriceTextField.text)
		}
	}
    
    // MARK: - API Methods
    
    private func getPurchaseOrderFields() {
        Utility.showLoader()
		if self.confirmationType == .transport {
			APIManager.sharedInstance.apiManagerServicesWordPress.getTransportPurchaseOrderFields(params: [:], success: { (response) in
				Utility.hideLoader()
				self.onPurchaseOrderFieldsSuccessResponse(response)
			}) { (error) in
				Utility.hideLoader()
				Utility.showAlert(title: "Error", message: error.localizedDescription)
			}
		} else {
			APIManager.sharedInstance.apiManagerServicesWordPress.getPurchaseOrderFields(params: [:], success: { (response) in
				Utility.hideLoader()
				self.onPurchaseOrderFieldsSuccessResponse(response)
			}) { (error) in
				Utility.hideLoader()
				Utility.showAlert(title: "Error", message: error.localizedDescription)
			}
		}
    }
	
	private func onPurchaseOrderFieldsSuccessResponse(_ response: AnyObject) {
		if let data = response as? Dictionary<String, Any> {
			if let vendors = data["vendors"] as? Array<AnyObject> {
				self.arrVendors.removeAll()
				for item in vendors {
					let model = VendorModel(JSON: item as! [String: Any])
					self.arrVendors.append(model!)
				}
				let dataSource = self.arrVendors.map {$0.name ?? ""}
				self.vendorDropDown.dataSource = dataSource
			}
			
			if let accounts = data["accounts"] as? Array<AnyObject> {
				self.arrAccounts.removeAll()
				for item in accounts {
					let model = AccountModel(JSON: item as! [String: Any])
					self.arrAccounts.append(model!)
				}
				let dataSource = self.arrAccounts.map {$0.name ?? ""}
				self.accountDropDown.dataSource = dataSource
			}
		} else {
			Utility.showAlert(title: "Error", message: "Failed to get vendors and accoounts details")
		}
	}
    
    private func addPurchaseOrder() {
        Utility.showLoader()
		let params: [String: Any] = [
			"booking_id": self.bookingId ?? "",
			"memo": self.memoTextView.text ?? "",
			"message_to_vendor": self.vendorMessageTextView.text ?? "",
			"purchase_price": self.purchasePriceTextField.text ?? "",
			"vendor_name": self.selectedVender?.name ?? "",
			"vendor_id": self.selectedVender?.id ?? "",
			"account_name": self.selectedAccount?.name ?? "",
			"account_id": self.selectedAccount?.id ?? "",
			"checkin_date": self.checkinDateTextField.text ?? "",
			"checkout_date": self.checkoutDateTextField.text ?? "",
			"confirmation_number": self.confirmationTextField.text ?? "",
			"internal_confirmation_number": self.internalConfirmationTextField.text ?? "",
			"external_confirmation_number": self.externalConfirmationTextField.text ?? "",
			"vendor_contact_persons": self.selectedPersons.map { String($0) },
			"type": self.isPackageBookingType ? "package" : "normal",
			"tax_vat": self.vatTextField.text ?? "",
			"tax_municiparity": self.munTaxTextField.text ?? ""
		]
		
		APIManager.sharedInstance.apiManagerServicesWordPress.addPurchaseOrder(params: params, success: { (response) in
			Utility.hideLoader()
			Utility.showAlert(title: "Message", message: "Puchased order added successfully", closure: { (action) in
				self.navigationController?.popViewController(animated: true)
			})
		}) { (error) in
			Utility.hideLoader()
			Utility.showAlert(title: "Error", message: error.localizedDescription)
		}
    }
	
	private func addTransportPurchaseOrder() {
		Utility.showLoader()
		
		let params: [String: Any] = [
			"booking_id": self.bookingId ?? "",
			"memo": self.memoTextView.text ?? "",
			"message_to_vendor": self.vendorMessageTextView.text ?? "",
			"purchase_price": self.purchasePriceTextField.text ?? "",
			"vendor_name": self.selectedVender?.name ?? "",
			"vendor_id": self.selectedVender?.id ?? "",
			"account_name": self.selectedAccount?.name ?? "",
			"account_id": self.selectedAccount?.id ?? "",
			"pickup_date": self.checkinDateTextField.text ?? "",
			"confirmation_number": self.confirmationTextField.text ?? "",
			"internal_confirmation_number": self.internalConfirmationTextField.text ?? "",
			"external_confirmation_number": self.externalConfirmationTextField.text ?? "",
			"vendor_contact_persons": self.selectedPersons.map { String($0) },
			"type": self.isPackageBookingType ? "package" : "normal",
			"tax_vat": self.vatTextField.text ?? "",
			"tax_municiparity": self.munTaxTextField.text ?? ""
		]
		
		APIManager.sharedInstance.apiManagerServicesWordPress.addTransportPurchaseOrder(params: params, success: { (response) in
			Utility.hideLoader()
			Utility.showAlert(title: "Message", message: "Puchased order added successfully", closure: { (action) in
				self.navigationController?.popViewController(animated: true)
			})
		}) { (error) in
			Utility.hideLoader()
			Utility.showAlert(title: "Error", message: error.localizedDescription)
		}
	}
    
    private func updatePurchaseOrder() {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddConfirmation: UITextFieldDelegate {
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.activeField = textField
        if textField == self.vendorTextField {
            self.vendorDropDown.show()
            return false
        } else if textField == self.accountTextField {
            self.accountDropDown.show()
            return false
        } else if textField == self.contactPersonTextField {
            self.contactPersonDropDown.show()
            return false
        }
        
        return true
    }
	
	func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
		if textField == self.purchasePriceTextField {
			if self.exclusiveTaxButton.isSelected {
				self.calculateExclusiveTax(textField.text)
			} else {
				self.calculateInclusiveTax(textField.text)
			}
		}
		
		return true
	}
}

extension AddConfirmation: TagListViewDelegate {
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void {
        sender.removeTagView(tagView)
        self.selectedPersons.remove(at: tagView.tag)
        
        self.updateTagListWidthConstraint()
    }
}
