//
//  CheckIn.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 07/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class CheckIn: BaseViewController {

    @IBOutlet weak var viewRoom1: UIView!
    @IBOutlet weak var viewRoom2: UIView!
    @IBOutlet weak var tfRoom1: UITextField!
    @IBOutlet weak var tfRoom2: UITextField!
    @IBOutlet weak var tvMessage: UITextView!
    
    var hotel = HotelModel()
    let notificationName = Notification.Name("Checked_In")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        // Do any additional setup after loading the view.
    }
    
    private func getRoomNumbers() -> String {
        var room_numbers = ""
        
        if (Int(self.hotel.totalRooms ?? "0")) == 1{
            room_numbers = self.tfRoom1.text ?? ""
        }
        else{
            room_numbers = (self.tfRoom1.text ?? "") + "," + (self.tfRoom2.text ?? "")
        }
        
        return room_numbers
    }
    
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        self.validateAndProcessCheckIn()
    }
}
//MARK:- Helper Methods
extension CheckIn{
    private func setData(){
        if (Int(self.hotel.totalRooms ?? "0")) == 1{
            self.viewRoom2.isHidden = true
        }
    }
    private func validateAndProcessCheckIn(){
        if (Int(self.hotel.totalRooms ?? "0")) == 1{
            if !Validation.validateStringLength(self.tfRoom1.text ?? ""){
                Utility.showAlert(title: "Error", message: "Enter room number 1")
                return
            }
            self.processCheckIn()
        }
        else{
            if !Validation.validateStringLength(self.tfRoom1.text ?? ""){
                Utility.showAlert(title: "Error", message: "Enter room number 1")
                return
            }
            if !Validation.validateStringLength(self.tfRoom2.text ?? ""){
                Utility.showAlert(title: "Error", message: "Enter room number 2")
                return
            }
            self.processCheckIn()
        }
    }
}
//MARK:- Services
extension CheckIn{
    private func processCheckIn(){
        Utility.showLoader()
        
        let bookingId = self.hotel.isPackageBooking ?
            (self.hotel.packageAccommodationId ?? "") : (self.hotel.bookingId ?? "0")
        
        let params: [String: Any] = [
            "booking_id": bookingId,
            "room_numbers": self.getRoomNumbers(),
            "check_in_comments": self.tvMessage.text ?? "",
            "type": self.hotel.isPackageBooking ? "package" : "normal"
        ]
        
        APIManager.sharedInstance.usersAPIManager.CheckIn(params: params, success: { (responseObject) in
            Utility.hideLoader()
            NotificationCenter.default.post(name: self.notificationName, object: nil)
            Utility.showAlert(title: "Alert", message: "Check-in successfully", closure: { (Ok) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            print(error.localizedDescription)
            Utility.hideLoader()
        }
    }
}

