//
//  HotelDetails.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 07/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import ObjectMapper

class HotelDetails: BaseViewController {

    @IBOutlet weak var lblHotelName: UILabel!
    @IBOutlet weak var lblCheckIn: UILabel!
    @IBOutlet weak var lblCheckOut: UILabel!
    @IBOutlet weak var lblNumberOfGuests: UILabel!
    @IBOutlet weak var lblTotalRooms: UILabel!
    @IBOutlet weak var lblRoomType: UILabel!
    @IBOutlet weak var lblRoomNumber: UILabel!
    @IBOutlet weak var btnCheckIn: UIButton!
    @IBOutlet weak var btnGuestDetails: UIButton!
    @IBOutlet weak var btnMeetAndGreet: UIButton!
    @IBOutlet weak var btnAddDelivery: UIButton!
	@IBOutlet weak var confirmationButton: UIButton!

    var hotel = HotelModel()
    let notificationName = Notification.Name("Checked_In")
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
        // Do any additional setup after loading the view.
		self.setData()
		self.updateUI()
		
		// Add notification observers
		NotificationCenter.default.addObserver(self, selector: #selector(self.checkedIn), name: self.notificationName, object: nil)
    }
	
	private func updateUI() {
		let user = Mapper<User>().map(JSONObject: Utility.convertToDictionary(text: (Defaults[.loginData]?.toString())!))
		if let role = user?.userRole{
			if role == "rm_trade_partner" || role == "rm_main"{
				self.btnCheckIn.isHidden = true
				self.btnAddDelivery.isHidden = true
			}
		}
		
		// Hide confirmation button if user is not admin
		if !(isAdminUserLoggedIn()) {
			self.confirmationButton.isHidden = true
		}
	}
	
	private func setData(){
		self.lblHotelName.text = self.hotel.name ?? "-"
		self.lblCheckIn.text = "Check-in: " + (self.hotel.date ?? "-")
		self.lblCheckOut.text = "Check-out: " + (self.hotel.checkoutDate ?? "-")
		self.lblNumberOfGuests.text = "Number of Guests: " + "\(self.hotel.numberOfGuests)"
		self.lblTotalRooms.text = "Total Rooms: " + (self.hotel.totalRooms ?? "-")
		self.lblRoomType.text = "Room Type: " + (self.hotel.type ?? "-")
		self.lblRoomNumber.text = "Room Number: " + (self.hotel.roomNumber ?? "-")
		
		if let checkinStatus = self.hotel.checkInStatus {
			self.btnCheckIn.setTitle("Check-out", for: .normal)
			self.btnCheckIn.isEnabled = checkinStatus
		} else {
			self.btnCheckIn.setTitle("Check-in", for: .normal)
		}
	}
    
    private func isOrderCancelledOrPending() -> Bool {
        if let status = self.hotel.status {
            if status.caseInsensitiveCompare("Cancelled") == .orderedSame
                || status.caseInsensitiveCompare("Pending payment") == .orderedSame {
                Utility.showAlert(title: "Error", message: "User action is not allowed. Please check your order status")
                return true
            }
        }
        
        return false
    }
    
    @IBAction func onBtnCheckIn(_ sender: UIButton) {
        guard !self.isOrderCancelledOrPending() else { return }
        if let checkinStatus = self.hotel.checkInStatus {
            if checkinStatus == true {
                self.processCheckOut()
            }
        } else {
            self.pushToCheckIn()
        }
    }
	
    @IBAction func onBtnGuestDetails(_ sender: UIButton) {
        guard !self.isOrderCancelledOrPending() else { return }
        self.pushToGuestDetails()
    }
	
    @IBAction func onBtnMeetNGreet(_ sender: UIButton) {
        guard !self.isOrderCancelledOrPending() else { return }
        self.pushToMeetAndGreet()
    }
	
    @IBAction func onBtnAddDelivery(_ sender: UIButton) {
        guard !self.isOrderCancelledOrPending() else { return }
        self.pushToAddDelivery()
    }
	
    @IBAction func onBtnConfirmation(_ sender: UIButton) {
		guard !self.isOrderCancelledOrPending() else { return }
		self.pushToConfirmationList()
	}
}

//MARK:- Helper Methods
extension HotelDetails{
    @objc func checkedIn(){
        self.hotel.checkInStatus = true
        self.setData()
    }
}
//MARK:- Helper Methods
extension HotelDetails{
    private func pushToCheckIn(){
        let storyboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "CheckIn") as? CheckIn{
            controller.hotel = self.hotel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
	
    private func pushToGuestDetails(){
        let storyboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "GuestDetails") as? GuestDetails{
            controller.hotel = self.hotel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
	
    private func pushToMeetAndGreet(){
        let storyboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "MeetAndGreet") as? MeetAndGreet{
            controller.hotel = self.hotel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
	
    private func pushToAddDelivery(){
        let storyboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "AddDelivery") as? AddDelivery{
            controller.hotel = self.hotel
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }

    private func pushToConfirmationList() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationList") as? ConfirmationList {
            controller.bookingId = self.hotel.bookingId
			controller.isPackageBookingType = self.hotel.isPackageBooking
			controller.confirmationType = .accomodation
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
//MARK:- Services
extension HotelDetails{
    private func processCheckOut(){
        Utility.showLoader()

        let bookingId = self.hotel.isPackageBooking ?
            (self.hotel.packageAccommodationId ?? "") : (self.hotel.bookingId ?? "0")

        let params: [String: Any] = [
            "booking_id": bookingId,
            "type": self.hotel.isPackageBooking ? "package" : "normal"
        ]
        
        APIManager.sharedInstance.usersAPIManager.CheckOut(params: params, success: { (responseObject) in
            Utility.hideLoader()
            self.hotel.checkInStatus = false
            self.setData()
            Utility.showAlert(title: "Message", message: "Check-out successfully")
        }) { (error) in
            print(error.localizedDescription)
            Utility.hideLoader()
        }
    }
}

