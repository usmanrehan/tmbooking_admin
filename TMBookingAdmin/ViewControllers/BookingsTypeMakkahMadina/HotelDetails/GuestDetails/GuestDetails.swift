//
//  GuestDetails.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 07/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class GuestDetails: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var arrGuests = [GuestDetailModel]()
    var flagIsTransport = false
    var hotel = HotelModel()
    var transport = TransportModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAllGuests()
    }
}
extension GuestDetails:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrGuests.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GuestDetailsTVC", for: indexPath) as! GuestDetailsTVC
        cell.setData(data: self.arrGuests[indexPath.row])
        return cell
    }
}
extension GuestDetails:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pushToEditGuestDetail(index: indexPath.row)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
}
//MARK:- Services
extension GuestDetails{
    private func pushToEditGuestDetail(index:Int){
        let storyboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "EditGuestDetails") as? EditGuestDetails{
            controller.guest = self.arrGuests[index]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
//MARK:- Services
extension GuestDetails{
    private func getAllGuests(){
        var order_id = ""
        if !self.flagIsTransport{
            order_id = self.hotel.orderId ?? "0"
        }
        else{
            order_id = self.transport.orderId ?? "0"
        }
        let param:[String:Any] = ["order_id":order_id]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.GetAllGuests(params: param, success: { (responseObject) in
            Utility.hideLoader()
            self.arrGuests = Mapper<GuestDetailModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.tableView.reloadData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
