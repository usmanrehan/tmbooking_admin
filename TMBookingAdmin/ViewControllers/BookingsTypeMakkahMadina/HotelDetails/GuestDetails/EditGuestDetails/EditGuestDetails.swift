    //
//  EditGuestDetails.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 07/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import  ObjectMapper
enum PickDateFor{
    case passport_expiry
    case date_of_birth
}

class EditGuestDetails: BaseViewController {

    @IBOutlet weak var tfGuestName: UITextField!
    @IBOutlet weak var tfPassportNumber: UITextField!
    @IBOutlet weak var tfPassportExpiry: UITextField!
    @IBOutlet weak var tfVisaNumber: UITextField!
    @IBOutlet weak var tfLocalContactNumber: UITextField!
    @IBOutlet weak var tfSaudiContactNumber: UITextField!
    @IBOutlet weak var tfDateOfBirth: UITextField!
    @IBOutlet weak var tfEmailAddress: UITextField!
    @IBOutlet weak var tfVisaSponsor: UITextField!
    @IBOutlet weak var btnEdit: UIButton!
    
    var guest = GuestDetailModel()
    var flagCanEdit = false
    var selectedTypeName = ""
    var selectedEditHistory = EditHistoryModel()
    var pickDateFor = PickDateFor.date_of_birth
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnEdit(_ sender: UIButton) {
        if !self.flagCanEdit{
            self.btnEdit.setTitle("Save", for: .normal)
            self.enableUserInteraction()
            self.flagCanEdit = true
        }
        else{
            self.validate()
        }
    }
    
    @IBAction func onTfPassportExpiry(_ sender: UITextField) {
        pickDateFor = PickDateFor.passport_expiry
        self.getPickedDate(sender: sender)
    }
    @IBAction func onTfDateOfBirth(_ sender: UITextField) {
        pickDateFor = PickDateFor.date_of_birth
        self.getPickedDate(sender: sender)
    }
    @IBAction func onBtnViewEditHistory(_ sender: UIButton) {
        self.getEditHistoryFor(tag:sender.tag)
    }
}
//MARK:- Helper Methods
extension EditGuestDetails{
    private func setData(){
        let data = self.guest
        self.tfGuestName.text = data.name
        self.tfPassportNumber.text = data.passportNumber
        self.tfPassportExpiry.text = data.passportExpiry
        self.tfVisaNumber.text = data.visaNumber
        self.tfLocalContactNumber.text = data.localContactNumber
        self.tfSaudiContactNumber.text = data.saudiContactNumber
        self.tfDateOfBirth.text = data.dateOfBirth
        self.tfEmailAddress.text = data.email
        self.tfVisaSponsor.text = data.visaSponser
        
        let user = Mapper<User>().map(JSONObject: Utility.convertToDictionary(text: (Defaults[.loginData]?.toString())!))
        if let role = user?.userRole{
            if role == "rm_trade_partner" || role == "rm_main"{
                self.btnEdit.isHidden = true
            }
        }
    }
    private func enableUserInteraction(){
        let user = Mapper<User>().map(JSONObject: Utility.convertToDictionary(text: (Defaults[.loginData]?.toString())!))
        if let role = user?.userRole{
            if role == "administrator"{
                self.tfGuestName.isUserInteractionEnabled = true
            }
        }
        self.tfPassportNumber.isUserInteractionEnabled = true
        self.tfPassportExpiry.isUserInteractionEnabled = true
        self.tfVisaNumber.isUserInteractionEnabled = true
        self.tfLocalContactNumber.isUserInteractionEnabled = true
        self.tfSaudiContactNumber.isUserInteractionEnabled = true
        self.tfDateOfBirth.isUserInteractionEnabled = true
        self.tfEmailAddress.isUserInteractionEnabled = true
        self.tfVisaSponsor.isUserInteractionEnabled = true
    }
    private func disableUserInteraction(){
        let user = Mapper<User>().map(JSONObject: Utility.convertToDictionary(text: (Defaults[.loginData]?.toString())!))
        if let role = user?.userRole{
            if role == "administrator"{
                self.tfGuestName.isUserInteractionEnabled = false
            }
        }
        self.tfPassportNumber.isUserInteractionEnabled = false
        self.tfPassportExpiry.isUserInteractionEnabled = false
        self.tfVisaNumber.isUserInteractionEnabled = false
        self.tfLocalContactNumber.isUserInteractionEnabled = false
        self.tfSaudiContactNumber.isUserInteractionEnabled = false
        self.tfDateOfBirth.isUserInteractionEnabled = false
        self.tfEmailAddress.isUserInteractionEnabled = false
        self.tfVisaSponsor.isUserInteractionEnabled = false
    }
    private func validate(){
        if (self.tfGuestName.text ?? "").isEmpty{
            Utility.showAlert(title: "Error", message: "Guest name is required")
            return
        }
        self.updateGuestRecord()
    }
    private func sortEditHistory(tag:Int,response:NSDictionary){
        switch tag {
        case 1://Guest Name
            //name
            if let name = response["name"] as? [String:Any]{
                self.selectedEditHistory = Mapper<EditHistoryModel>().map(JSON: name) ?? EditHistoryModel()
                self.selectedTypeName = "Guest Name"
            }
        case 2://Passport Number
            //passport_number
            if let passport_number = response["passport_number"] as? [String:Any]{
                self.selectedEditHistory = Mapper<EditHistoryModel>().map(JSON: passport_number) ?? EditHistoryModel()
                self.selectedTypeName = "Passport Number"
            }
        case 3://Passport Expiry
            //passport_expiry
            if let passport_expiry = response["passport_expiry"] as? [String:Any]{
                self.selectedEditHistory = Mapper<EditHistoryModel>().map(JSON: passport_expiry) ?? EditHistoryModel()
                self.selectedTypeName = "Passport Expiry"
            }
        case 4://Visa Number
            //visa_number
            if let visa_number = response["visa_number"] as? [String:Any]{
                self.selectedEditHistory = Mapper<EditHistoryModel>().map(JSON: visa_number) ?? EditHistoryModel()
                self.selectedTypeName = "Visa Number"
            }
        case 5://Local Contact Number
            //local_contact_number
            if let local_contact_number = response["local_contact_number"] as? [String:Any]{
                self.selectedEditHistory = Mapper<EditHistoryModel>().map(JSON: local_contact_number) ?? EditHistoryModel()
                self.selectedTypeName = "Local Contact Number"
            }
        case 6://Saudi Contact Number
            //saudi_contact_number
            if let saudi_contact_number = response["saudi_contact_number"] as? [String:Any]{
                self.selectedEditHistory = Mapper<EditHistoryModel>().map(JSON: saudi_contact_number) ?? EditHistoryModel()
                self.selectedTypeName = "Saudi Contact Number"
            }
        case 7://Date of Birth
            //date_of_birth
            if let date_of_birth = response["date_of_birth"] as? [String:Any]{
                self.selectedEditHistory = Mapper<EditHistoryModel>().map(JSON: date_of_birth) ?? EditHistoryModel()
                self.selectedTypeName = "Date of Birth"
            }
        case 8://Email Address
            //email
            if let email = response["email"] as? [String:Any]{
                self.selectedEditHistory = Mapper<EditHistoryModel>().map(JSON: email) ?? EditHistoryModel()
                self.selectedTypeName = "Email Address"
            }
        case 9://Visa Sponsor
            //visa_sponser
            if let visa_sponser = response["visa_sponser"] as? [String:Any]{
                self.selectedEditHistory = Mapper<EditHistoryModel>().map(JSON: visa_sponser) ?? EditHistoryModel()
                self.selectedTypeName = "Visa Sponsor"
            }
        default:
            break
        }
        if self.selectedEditHistory.editHistory.count > 0{
            self.presentHistory()
        }
        else{
            Utility.showAlert(title: "Alert", message: "No history available")
        }
    }
    private func presentHistory(){
        let storyboard = UIStoryboard(name: "Booking", bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(withIdentifier: "GuestEditHistory") as! GuestEditHistory
        self.present(controller, animated: true, completion: nil)
        controller.selectedEditHistory = self.selectedEditHistory
        controller.typeName = self.selectedTypeName
    }
}
//MARK:- Services
extension EditGuestDetails{
    private func updateGuestRecord(){
        let guest_id = self.guest.guestId ?? "0"
        let order_id = self.guest.orderId ?? "0"
        let name = self.tfGuestName.text ?? ""
        let passport_expiry = self.tfPassportExpiry.text ?? ""
        let passport_number = self.tfPassportNumber.text ?? ""
        let local_contact_number = self.tfLocalContactNumber.text ?? ""
        let visa_number = self.tfVisaNumber.text ?? ""
        let visa_sponser = self.tfVisaSponsor.text ?? ""
        let saudi_contact_number = self.tfSaudiContactNumber.text ?? ""
        let date_of_birth = self.tfDateOfBirth.text ?? ""
        let userId = Defaults[.loginUserId]
        let params:[String:Any] = ["user_id":userId,
                                   "guest_id":guest_id,
                                   "order_id":order_id,
                                   "name":name,
                                   "passport_expiry":passport_expiry,
                                   "passport_number":passport_number,
                                   "local_contact_number":local_contact_number,
                                   "visa_number":visa_number,
                                   "visa_sponser":visa_sponser,
                                   "saudi_contact_number":saudi_contact_number,
                                   "date_of_birth":date_of_birth]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.UpdateGuest(params: params, success: { (responseObject) in
            Utility.hideLoader()
            self.btnEdit.setTitle("Edit", for: .normal)
            self.disableUserInteraction()
            self.flagCanEdit = false
            Utility.showAlert(title: "Alert", message: "Guest details updated")
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func getEditHistoryFor(tag:Int){
        let userId = Defaults[.loginUserId]
        let order_id = self.guest.orderId ?? "0"
        let guest_id = self.guest.guestId ?? "0"
        let params:[String:Any] = ["userId":userId,
                                   "order_id":order_id,
                                   "guest_id":guest_id]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.GetEditHistory(params: params, success: { (responseObject) in
            Utility.hideLoader()
            let response = responseObject as NSDictionary
            self.sortEditHistory(tag: tag, response: response)
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
//MARK:- Date Picker and setter
extension EditGuestDetails{
    private func getPickedDate(sender: UITextField){
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        switch self.pickDateFor{
        case .date_of_birth:
            self.tfDateOfBirth.text = dateFormatter.string(from: sender.date)
        case .passport_expiry:
            self.tfPassportExpiry.text = dateFormatter.string(from: sender.date)
        }
    }
}
    extension EditGuestDetails:UITextFieldDelegate{
        func textFieldDidEndEditing(_ textField: UITextField) {
            self.btnEdit.isEnabled = true
        }
        func textFieldDidBeginEditing(_ textField: UITextField) {
            self.btnEdit.isEnabled = false
        }
    }
