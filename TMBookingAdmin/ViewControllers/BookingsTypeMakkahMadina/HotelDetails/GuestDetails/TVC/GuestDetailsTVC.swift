//
//  GuestDetailsTVC.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 07/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class GuestDetailsTVC: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    
    func setData(data:GuestDetailModel){
        self.selectionStyle = .none
        self.lblName.text = data.name ?? "-"
    }

}
