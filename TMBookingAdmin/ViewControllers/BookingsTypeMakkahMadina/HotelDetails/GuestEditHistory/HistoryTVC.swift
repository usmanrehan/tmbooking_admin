//
//  HistoryTVC.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 12/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class HistoryTVC: UITableViewCell {

    @IBOutlet weak var lblEditorName: UILabel!
    @IBOutlet weak var lblOldValue: UILabel!
    @IBOutlet weak var lblNewValue: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    func setData(data:EditHistory){
        self.selectionStyle = .none
        self.lblEditorName.text = "Updated by: " + (data.user ?? "-")
        self.lblOldValue.text = "Old value: " + (data.oldValue ?? "-")
        self.lblNewValue.text = "New value: " + (data.value ?? "-")
        self.lblDate.text = (data.dateTime ?? "-")
    }

}
