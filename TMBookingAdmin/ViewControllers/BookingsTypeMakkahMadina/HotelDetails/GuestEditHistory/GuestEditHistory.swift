//
//  GuestEditHistory.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 12/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class GuestEditHistory: UIViewController {

    var selectedEditHistory = EditHistoryModel()
    
    @IBOutlet weak var lblTypeName: UILabel!
    @IBOutlet weak var tableview: UITableView!
    var typeName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.lblTypeName.text = self.typeName
    }

    @IBAction func onBtnOk(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension GuestEditHistory:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.selectedEditHistory.editHistory.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTVC", for: indexPath) as! HistoryTVC
        cell.setData(data: self.selectedEditHistory.editHistory[indexPath.row])
        return cell
    }
}
