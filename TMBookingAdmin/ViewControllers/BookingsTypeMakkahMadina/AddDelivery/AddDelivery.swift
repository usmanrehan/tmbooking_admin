//
//  AddDelivery.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 08/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper
import DropDown
import SwiftyJSON
import SwiftDate

class AddDelivery: BaseViewController {

    @IBOutlet weak var tfItem: UITextField!
    @IBOutlet weak var tfDateTime: UITextField!
    @IBOutlet weak var tfDeliveryBoy: UITextField!
    @IBOutlet weak var tfRoomNumber: UITextField!
    @IBOutlet weak var tfQuantity: UITextField!
    
    var hotel = HotelModel()
    var arrGifts = [String]()
    var arrAssigneeStrings = [String]()
    var arrAssignee = [AssigneeModel]()
    let dropDownGifts = DropDown()
    let dropDownAssignee = DropDown()
    var assigneeId = ""
    private let pickerViewData = Array(1...30)
    let picker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        self.getGiftItemsAndAssignee()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnGift(_ sender: UIButton) {
        self.dropDownGifts.show()
    }
    @IBAction func onTfDateTime(_ sender: UITextField) {
        self.getPickedDate(sender: sender)
    }
    @IBAction func onBtnAssignee(_ sender: UIButton) {
        self.dropDownAssignee.show()
    }
    @IBAction func onTfQuantity(_ sender: UITextField) {
        self.getQuantity(sender: sender)
    }
    @IBAction func onBtnSubmit(_ sender: UIButton) {
        self.validateAndProcessAddDelivery()
    }
}
//MARK:- DatePicker
extension AddDelivery{
    private func getPickedDate(sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.dateAndTime
        sender.inputView = datePickerView
        
        if let checkInDate = self.hotel.date {
            if let date = checkInDate.toDate("dd-MM-yyyy", region: Region.local) {
                datePickerView.minimumDate = date.date
            }
        }
        
        if let checkoutDate = self.hotel.checkoutDate {
            if let date = checkoutDate.toDate("dd-MM-yyyy", region: Region.local) {
                datePickerView.maximumDate = (date + 1.days).date
            }
        }
        
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: .valueChanged)
    }
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.medium
        self.tfDateTime.text = dateFormatter.string(from: sender.date)
    }
    private func getQuantity(sender:UITextField){
        sender.inputView = self.picker
    }
}
//MARK:- Helper Methods
extension AddDelivery{
    private func setUpUI(){
        self.dropDownGifts.anchorView = self.tfItem
        self.dropDownAssignee.anchorView = self.tfDeliveryBoy
        self.dropDownGifts.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfItem.text = self.arrGifts[index]
        }
        self.dropDownAssignee.selectionAction = { [unowned self] (index: Int, item: String) in
            self.assigneeId = self.arrAssignee[index].userId ?? ""
            self.tfDeliveryBoy.text = self.arrAssignee[index].name ?? ""
        }
        self.picker.delegate = self
        self.picker.dataSource = self
    }
    private func validateAndProcessAddDelivery(){
        if (self.tfItem.text ?? "").isEmpty{
            Utility.showAlert(title: "Error", message: "Please select an item.")
            return
        }
        if (self.tfDateTime.text ?? "").isEmpty{
            Utility.showAlert(title: "Error", message: "Please select delivery date.")
            return
        }
        if (self.tfDeliveryBoy.text ?? "").isEmpty{
            Utility.showAlert(title: "Error", message: "Please select the delivery boy.")
            return
        }
        if (self.tfRoomNumber.text ?? "").isEmpty{
            Utility.showAlert(title: "Error", message: "Please enter room number.")
            return
        }
        if (self.tfQuantity.text ?? "").isEmpty{
            Utility.showAlert(title: "Error", message: "Please enter quantity.")
            return
        }
        self.addDelivery()
    }
    private func getFormattedDate()->String{
        let selectedDate = self.tfDateTime.text ?? ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.medium
        
        let date = dateFormatter.date(from: selectedDate)
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: date!)
    }
}
//MARK:- Services
extension AddDelivery{
    private func getGiftItemsAndAssignee(){
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.GetGiftsAndAssignee(params: [:], success: { (responseObject) in
            Utility.hideLoader()
            let response = responseObject as NSDictionary
            if let gift_items = response["gift_items"] as? [String]{
                self.arrGifts = gift_items
            }
            if let assignee = response["assignee"] as? [[String : Any]]{
                self.arrAssignee = Mapper<AssigneeModel>().mapArray(JSONArray: assignee)
                for item in self.arrAssignee{
                    self.arrAssigneeStrings.append(item.name ?? "-")
                }
            }
            self.dropDownGifts.dataSource = self.arrGifts
            self.dropDownAssignee.dataSource = self.arrAssigneeStrings
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func addDelivery(){
        Utility.showLoader()
        let params:[String: Any] = [
            "booking_id": self.hotel.bookingId ?? "0",
            "item_name": self.tfItem.text ?? "",
            "assigned_to": self.assigneeId,
            "delivery_time": self.getFormattedDate(),
            "room_number": self.tfRoomNumber.text ?? "",
            "quantity":self.tfQuantity.text ?? ""
        ]
        print(params)
        APIManager.sharedInstance.usersAPIManager.AddDelivery(params: params, success: { (responseObject) in
            Utility.hideLoader()
            Utility.showAlert(title: "Message", message: "Delivery item added successfully", closure: { (Ok) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
}
//MARK:- UITextFieldDelegate
extension AddDelivery: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.medium
        self.tfDateTime.text = dateFormatter.string(from: Date())
    }
}
extension AddDelivery: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerViewData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(self.pickerViewData[row])"
    }
}
extension AddDelivery: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.tfQuantity.text = "\(self.pickerViewData[row])"
    }
}
