//
//  HeaderDate.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 06/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class HeaderDate: UIView {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnHeader: UIButton!
    
    
    class func instanceFromNib() -> UIView
    {
        return UINib(nibName: "HeaderDate", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
