//
//  BookingTypeTVC.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 06/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class BookingTypeTVC: UITableViewCell {

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblShortDesc: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    func setData(data:Any?){
        self.selectionStyle = .none
        if let gift = data as? DeliveryModel{
            self.imgIcon.image = #imageLiteral(resourceName: "gift1")
            self.lblTime.text = gift.time ?? "-"
            self.lblShortDesc.text = gift.name ?? "-"
            self.lblStatus.text = gift.status ?? "-"
        }
        if let hotel = data as? HotelModel{
            self.imgIcon.image = #imageLiteral(resourceName: "hotel1")
            self.lblTime.text = hotel.time ?? "-"
            self.lblShortDesc.text = hotel.name ?? "-"
            self.lblStatus.text = hotel.status ?? "-"
        }
        if let transport = data as? TransportModel{
            self.imgIcon.image = #imageLiteral(resourceName: "car1")
            self.lblTime.text = transport.time ?? "-"
            self.lblShortDesc.text = transport.name ?? "-"
            self.lblStatus.text = transport.status ?? "-"
        }
    }
}
