//
//  GiftDelivery.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 06/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import ObjectMapper

class GiftDelivery: BaseViewController {

    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblOwnerName: UILabel!
    @IBOutlet weak var lblHotelName: UILabel!
    @IBOutlet weak var lblRoomNumber: UILabel!
    @IBOutlet weak var lblContactNumber: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblDeliveryDate: UILabel!
    @IBOutlet weak var lblDeliveryTime: UILabel!
    @IBOutlet weak var btnDelivery: UIButton!
    
    var gift = DeliveryModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        // Do any additional setup after loading the view.
    }

    @IBAction func onBtnDelivered(_ sender: UIButton) {
        self.processDelivered()
    }
    
}
//MARK:- Helper Methods
extension GiftDelivery{
    private func setData(){
        self.lblItemName.text = (self.gift.name ?? "-")
        self.lblOwnerName.text = (self.gift.guestName ?? "-")
        self.lblHotelName.text = "Hotel: " + (self.gift.hotelName ?? "-")
        self.lblRoomNumber.text = "Room #: " + "\(self.gift.roomNumber)"
        self.lblContactNumber.text = "Saudi Contact Number: " + (self.gift.contactNumber ?? "-")
        self.lblQuantity.text = "Quantity: " + (self.gift.quantity ?? "-")
        self.lblDeliveryDate.text = "Delivery Date: " + (self.gift.date ?? "-")
        self.lblDeliveryTime.text = "Delivery Time: " + (self.gift.time ?? "-")
        
        let user = Mapper<User>().map(JSONObject: Utility.convertToDictionary(text: (Defaults[.loginData]?.toString())!))
        if let role = user?.userRole{
            if role == "mandoob_delivery" || role == "administrator"{
                self.btnDelivery.isHidden = false
            }
            else{
                self.btnDelivery.isHidden = true
            }
        }
        
        if let status = self.gift.status, status.caseInsensitiveCompare("Delivered") == .orderedSame {
            self.btnDelivery.isHidden = true
        }
    }
}
//MARK:- Helper Methods
extension GiftDelivery{
    private func processDelivered(){
        let delivery_id = self.gift.deliveryId ?? ""
        let user_id = Defaults[.loginUserId]
        let params:[String:Any] = ["delivery_id":delivery_id,
                                   "user_id":user_id]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.DeliverGift(params: params, success: { (responseObject) in
            Utility.hideLoader()
            Utility.showAlert(title: "Success", message: "Item delivered", closure: { (Ok) in
                self.navigationController?.popViewController(animated: true)
            })
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
