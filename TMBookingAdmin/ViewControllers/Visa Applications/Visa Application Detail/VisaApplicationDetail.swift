//
//  VisaApplicationDetail.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 07/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import DropDown
import ObjectMapper

class VisaApplicationDetail: BaseViewController {

    @IBOutlet weak var imgProfilePhoto: UIImageView!
    @IBOutlet weak var imgPassportPhoto: UIImageView!
    @IBOutlet weak var btnProfilePhotoPicker: UIButton!
    @IBOutlet weak var btnPassportPhotoPicker: UIButton!
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfNationality: UITextField!
    @IBOutlet weak var tfLivingIn: UITextField!
    @IBOutlet weak var tfFullName: UITextField!
    @IBOutlet weak var tfMobileNumber: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfReferalCode: UITextField!
    @IBOutlet weak var tfStatus: UITextField!
    @IBOutlet weak var tfReason: UITextField!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var btnMember: UIButton!
    @IBOutlet weak var btnSave_Edit: UIButton!
    @IBOutlet weak var dropDownAnchor: UIView!
    
    var visaApplicationModel = VisaApplicationModel()
    
    let dropDown = DropDown()
    let dataSource = ["Pending","Approved","Rejected"]
    var selecteStatusCode = "0"
    var isPushFromMember = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Visa Application Detail"
        if self.isPushFromMember{self.btnMember.isHidden = true}
        else{self.btnMember.isHidden = false}
        self.setData()
        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnStatus(_ sender: UIButton) {
        dropDown.anchorView = self.dropDownAnchor
        dropDown.frame = self.dropDownAnchor.frame
        dropDown.dataSource = self.dataSource
        dropDown.show()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.tfStatus.text = item
            self.selecteStatusCode = "\(index)"
        }
    }
    @IBAction func onBtnMember(_ sender: UIButton) {
        self.pushToVisaApplications()
    }
    @IBAction func onBtnSave_Edit(_ sender: UIButton) {
        if sender.isSelected{
            if !self.isPushFromMember{
                self.updateVisaApplication()
            }
            else{
                self.updateMemberVisaStatus()
            }
            return
        }
        sender.isSelected = !sender.isSelected
        self.enableUserInteraction()
    }
}
//MARK:- Helper Methods
extension VisaApplicationDetail{
    private func pushToVisaApplications(){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "VisaApplications") as? VisaApplications{
            controller.isPushFromMember = true
            controller.userid = self.visaApplicationModel.userid ?? "0"
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    private func setData(){
        let data = self.visaApplicationModel
        let profilePhoto = data.photo ?? "http://tmbooking.com/tmbooking/public/img/placeholder.png"
        self.imgProfilePhoto.sd_setImage(with: Utility.stringToUrl(string: profilePhoto), completed: nil)
        let passportPhoto = data.fileUrl ?? "http://tmbooking.com/tmbooking/public/img/placeholder.png"
        self.imgPassportPhoto.sd_setImage(with: Utility.stringToUrl(string: passportPhoto), completed: nil)
        self.tfDate.text = data.date ?? "-"
        self.tfNationality.text = data.nationality ?? "-"
        self.tfLivingIn.text = data.livingin ?? "-"
        self.tfFullName.text = data.fullname ?? "-"
        self.tfMobileNumber.text = data.mobile ?? "-"
        self.tfEmail.text = data.email ?? "-"
        self.tfReferalCode.text = data.referalcode ?? "-"
        self.tfStatus.text = Utility.returnVisaApplicationStatusStringFromCode(code: data.status ?? "0")
        self.selecteStatusCode = data.status ?? "0"
        self.tfReason.text = data.reason ?? "-"
    }
    private func disableUserInteraction(){
        self.btnStatus.isUserInteractionEnabled = false
        self.tfReason.isUserInteractionEnabled = false
    }
    private func enableUserInteraction(){
        self.btnStatus.isUserInteractionEnabled = true
        self.tfReason.isUserInteractionEnabled = true
    }
}
//MARK:- Services
extension VisaApplicationDetail{
    private func updateVisaApplication(){
        let id = self.visaApplicationModel.id ?? "0"
        let status = self.selecteStatusCode
        let reason = self.tfReason.text ?? ""
        let params: [String:Any] = ["status":status,
                                    "reason":reason,
                                    "visaformid":id]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.UpdateParentVisaStatus(params: params, success: { (responseObject) in
            Utility.hideLoader()
            let response = Mapper<VisaApplicationModel>().map(JSON: responseObject as [String : Any]) ?? VisaApplicationModel()
            self.visaApplicationModel = response
            self.setData()
            self.btnSave_Edit.isSelected = false
            self.disableUserInteraction()
            Utility.showAlert(title: "Confirmation", message: "Record updated successfully.")
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func updateMemberVisaStatus(){
        let id = self.visaApplicationModel.id ?? "0"
        let status = self.selecteStatusCode
        let reason = self.tfReason.text ?? ""
        let params: [String:Any] = ["status":status,
                                    "reason":reason,
                                    "id":id]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.UpdateMemberVisaStatus(params: params, success: { (responseObject) in
            Utility.hideLoader()
            let response = Mapper<VisaApplicationModel>().map(JSON: responseObject as [String : Any]) ?? VisaApplicationModel()
            self.visaApplicationModel = response
            self.setData()
            self.btnSave_Edit.isSelected = false
            self.disableUserInteraction()
            Utility.showAlert(title: "Confirmation", message: "Record updated successfully.")
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}






