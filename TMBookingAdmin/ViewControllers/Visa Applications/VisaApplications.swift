//
//  VisaApplications.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 07/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class VisaApplications: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var arrVisaApplications = [VisaApplicationModel]()
    var isPushFromMember = false
    var userid = "0"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Visa Applications"
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !self.isPushFromMember{
            self.getVisaApplicationList()
        }
        else{
            self.getMembers()
        }
    }
}
//MARK:- Helper Methods
extension VisaApplications{
    private func pushToVisaApplicationDetail(index: Int){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let controller = storyboard.instantiateViewController(withIdentifier: "VisaApplicationDetail") as? VisaApplicationDetail{
            controller.isPushFromMember = self.isPushFromMember
            controller.visaApplicationModel = self.arrVisaApplications[index]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
extension VisaApplications: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrVisaApplications.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VisaApplicationTVC", for: indexPath) as! VisaApplicationTVC
        cell.setData(data: self.arrVisaApplications[indexPath.row])
        return cell
    }
}
extension VisaApplications: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pushToVisaApplicationDetail(index: indexPath.row)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
//MARK:- Services
extension VisaApplications{
    private func getVisaApplicationList(){
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.VisaApplicationList(params: [String:Any](), success: { (responseArray) in
            Utility.hideLoader()
            let response = Mapper<VisaApplicationModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            self.arrVisaApplications = response
            self.tableView.reloadData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
    private func getMembers(){
        let param: [String:Any] = ["userid":self.userid]
        Utility.showLoader()
        APIManager.sharedInstance.usersAPIManager.GetMembers(params: param, success: { (responseArray) in
            Utility.hideLoader()
            let response = Mapper<VisaApplicationModel>().mapArray(JSONArray: responseArray as! [[String : Any]])
            self.arrVisaApplications = response
            self.tableView.reloadData()
        }) { (error) in
            Utility.hideLoader()
            print(error.localizedDescription)
        }
    }
}
