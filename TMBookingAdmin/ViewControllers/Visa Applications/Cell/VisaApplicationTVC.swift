//
//  VisaApplicationTVC.swift
//  TMBookingAdmin
//
//  Created by M Usman Bin Rehan on 07/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class VisaApplicationTVC: UITableViewCell {

    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    func setData(data: VisaApplicationModel){
        self.selectionStyle = .none
        self.lblFullName.text = data.fullname ?? "-"
        self.lblDate.text = data.date ?? "-"
        self.lblStatus.text = Utility.returnVisaApplicationStatusStringFromCode(code: data.status ?? "0")  
    }
}
