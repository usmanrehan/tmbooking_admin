//
//  APIManagerBase.swift
//  The Court Lawyer
//
//  Created by Ahmed Shahid on 5/3/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.


import Foundation
import UIKit
import Alamofire
import SwiftyJSON

enum Route: String {
    // MARK: - Users Route
    case DriverList = "tmbooking/api/driverlist"
    case UpdateDriver = "tmbooking/api/updatedriver"
    case DriverRegistration = "tmbooking/api/driverregistration"
    case RemoveDriver = "tmbooking/api/removedriver"
    case GetBookingRides = "tmbooking/api/getRidesBooking"
    case PendingRides = "tmbooking/api/pendingrides"
    case ConfirmationBooking = "tmbooking/api/confirmationBooking"
    case UnbookDriver = "tmbooking/api/unbookdriver"
    case AssignDriver = "tmbooking/api/assigndriver"
    case ActiveRides = "tmbooking/api/activerides"
    case CancelRidesList = "tmbooking/api/cancelrideslist"
    case CancelBooking = "tmbooking/api/cancelbooking"
    case AssignedDrivers = "tmbooking/api/getassigneddrivers"
    case BookingHistory = "tmbooking/api/bookinghistory"
    case VisaApplicationList = "tmbooking/api/visaapplicationlist"
    case VisaCountryList = "tmbooking/api/countrylist"
    case AddVisaCountry = "tmbooking/api/addcountry"
    case UpdateCountry = "tmbooking/api/updatecountry"
    case RemoveVisaCountry = "tmbooking/api/removecountry"
    case VisaTypeList = "tmbooking/api/visatypelist"
    case RemoveVisaType = "tmbooking/api/removecountryvisatype"
    case UpdateVisaType = "tmbooking/api/updatecountryvisatype"
    case AddVisaType = "tmbooking/api/addcountryvisatype"
    case Block_UnblockDriver = "tmbooking/api/block_unblockdriver"
    case UpdateVisaForm = "tmbooking/api/updatevisaform"
    case UpdateParentVisaStatus = "tmbooking/api/updateparentvisastatus"
    case UpdateMemberVisaStatus = "tmbooking/api/updatemembervisastatus"
    case GetMembers = "tmbooking/api/getmembers"
    
    case GetAllVisas = "tmbooking/api/getAllVisaApplications"
    case GetAllSaudiVisas = "tmbooking/api/getsaudivisa"
    case ListSingleVisa = "tmbooking/api/getallvisas"
    case GetAllGroup = "tmbooking/api/getallgroup"
    case GetGroupMembers = "tmbooking/api/getgroupmembers"
    case GetMember = "tmbooking/api/getmember"
    case UpdateMember = "tmbooking/api/updatemember"
    case GetStatus = "tmbooking/api/getstatus"
    case UpdateVisaStatus = "tmbooking/api/updatevisatstatus"
    case UpdateSaudiVisaStatus = "tmbooking/api/updatesaudivisatstatus"
    case GetVisatStatus = "tmbooking/api/getvisatstatus"
    
    // MARK: - OkToBoard API Constants
    
    case GetOkToBoardVisas = "travelmate/api/admin/getPendignOkToBoard"
    case OkToBoardVisaApproval = "travelmate/api/Admin/okToBoardApproval"
    case GetOkToBoardStamps = "travelmate/api/Admin/stampList"
    
    //MARK:- Wordpress api end points
    case auth = "jwt-auth/v1/token"
    case login = "wp/v2/users/me"
    
    //MARK:- Custom wordpress api
    case AddToken = "tmbooking/api/addtoken"
    
    //MARK:- Custom wordpress php end points
    case ListBookings = "tmbooking-api/v1/route/list_bookings"
    case DeliverGift = "tmbooking-api/v1/route/deliveries/delivered"
    case CheckIn = "tmbooking-api/v1/route/checkin"
    case CheckOut = "tmbooking-api/v1/route/checkout"
    case AllGuests = "tmbooking-api/v1/route/all_guests"
    case UpdateGuest = "tmbooking-api/v1/route/update_guest"
    case MeetGreet = "tmbooking-api/v1/route/meetgreet"
    case ListBookingsCount = "tmbooking-api/v1/route/list_bookings_count"
    case GiftsAndAssignee = "tmbooking-api/v1/route/deliveries/items"
    case AddDelivery = "tmbooking-api/v1/route/deliveries"
    case Logout = "tmbooking-api/v1/route/user_logout"
    case EditHistory = "tmbooking-api/v1/route/guest_detail"
    case getUserNotifications = "tmbooking-api/v1/route/get_user_notifications"
    case SendVisaEmail = "tmbooking-api/v1/route/send_visa_email"
    case getVisaFields = "tmbooking-api/v1/route/visa_fields"
    case getPurchaseOrders = "tmbooking-api/v1/route/accommodation_purchase_orders"
	case getTransportPurchaseOrders = "tmbooking-api/v1/route/transport_purchase_orders"
    case addPurchaseOrders = "tmbooking-api/v1/route/create_accommodation_purchase_orders"
	case addTransportPurchaseOrders = "tmbooking-api/v1/route/create_transport_purchase_orders"
    case getPurchaseOrderFields = "tmbooking-api/v1/route/purchase_order_fields"
	case getTransportPurchaseOrderFields = "tmbooking-api/v1/route/transport_purchase_order_fields"
    case getOkToBoardStamp = "tmbooking-api/v1/route/okay_to_board_stamp"
    case getPendingUsers = "tmbooking-api/v1/route/get_users"
    case updateUserStatus = "tmbooking-api/v1/route/update_user_status"
    case GetTransportVendors = "tmbooking-api/v1/route/transport_vendors_drivers"
    case getAllOrderStatus = "tmbooking-api/v1/route/get_all_order_status"
    case updateOrderStatus = "tmbooking-api/v1/route/update_order_status"
    case getOrderPaymentSlips = "tmbooking-api/v1/route/get_order_payment_slips"
}

class APIManagerBase: NSObject {
    
    let baseURL = Constants.BaseURL
    let defaultRequestHeader = ["Content-Type": "application/json"]
    let defaultError = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey: "Request Failed."])
    
    func getAuthorizationHeader () -> Dictionary<String,String> {
        
        if let token = APIManager.sharedInstance.serverToken {
            return ["token":"\(token)"]
        }
        
        return ["Content-Type":"application/json"]
    }
    
    func getErrorFromResponseData(data: Data) -> NSError? {
        do{
            let result = try JSONSerialization.jsonObject(with: data,options: JSONSerialization.ReadingOptions.mutableContainers) as? Array<Dictionary<String,AnyObject>>
            if let message = result?[0]["message"] as? String{
                let error = NSError(domain: "Error", code: 0, userInfo: [NSLocalizedDescriptionKey: message])
                return error;
            }
        }catch{
            NSLog("Error: \(error)")
        }
        return nil
    }
    
    func URLforRoute(route: String,params:[String: Any]) -> NSURL? {
        
        if let components: NSURLComponents  = NSURLComponents(string: (Constants.BaseURL+route)){
            var queryItems = [NSURLQueryItem]()
            for(key,value) in params {
                queryItems.append(NSURLQueryItem(name:key,value: "\(value)"))
            }
            components.queryItems = queryItems as [URLQueryItem]?
            
            return components.url as NSURL?
        }
        return nil;
    }
    
    func WPURLforRoute(route: String,params:[String: Any]) -> NSURL? {        
        if let components: NSURLComponents  = NSURLComponents(string: (Constants.WpBaseURL+route)){
            var queryItems = [NSURLQueryItem]()
            for(key,value) in params {
                queryItems.append(NSURLQueryItem(name:key,value: "\(value)"))
            }
            components.queryItems = queryItems as [URLQueryItem]?
            
            return components.url as NSURL?
        }
        return nil;
    }
    
    func POSTURLforRoute(route:String) -> URL?{
        if let components: NSURLComponents = NSURLComponents(string: (Constants.BaseURL+route)){
            return components.url! as URL
        }
        return nil
    }
    
    func WPPOSTURLforRoute(route:String) -> URL?{
        if let components: NSURLComponents = NSURLComponents(string: (Constants.WpBaseURL+route)){
            return components.url! as URL
        }
        return nil
    }

    func GETURLfor(route:String) -> URL?{
        if let components: NSURLComponents = NSURLComponents(string: (Constants.BaseURL+route)){
            return components.url! as URL
        }
        return nil
    }

    func postRequestWithMultipartForCreateRequest(route: URL,parameters: Parameters, images: [UIImage],
                                                  success:@escaping DefaultAPISuccessClosure,
                                                  failure:@escaping DefaultAPIFailureClosure){
        Alamofire.upload (
            multipartFormData: { multipartFormData in
                for (key , value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                }
                for item in images {
                    let image = UIImageJPEGRepresentation(item, 0.2)
                    if let data:Data = image {
                        multipartFormData.append(data, withName: "images[]", fileName: "fileName.jpg", mimeType: data.mimeType)
                    }
                }
                
        },
            to: route,
            encodingCompletion: { result in
                switch result {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        self.responseResult(response, success: {result in
                            
                            success(result as! Dictionary<String, AnyObject>)
                        }, failure: {error in
                            
                            failure(error)
                        })
                    }
                case .failure(let encodingError):
                    failure(encodingError as NSError)
                }
        }
        )
    }
    // For Multipart
    func postRequestWithMultipart(route: URL,parameters: Parameters,
                                  success:@escaping DefaultAPISuccessClosure,
                                  failure:@escaping DefaultAPIFailureClosure , withHeader: Bool){
        if withHeader{
            Alamofire.upload(multipartFormData:{ multipartFormData in
                for (key , value) in parameters {
                    if let data:Data = value as? Data {
                        //multipartFormData.append(data, withName: key, fileName: "fileName.jpeg", mimeType: "image/jpeg")
                        multipartFormData.append(data, withName: key, fileName: "\(key)\(data.getExtension)", mimeType: "\(data.mimeType)")
                    } else {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                }
            },
                             usingThreshold:UInt64.init(),
                             to: route,
                             method:.post,
                             headers: getAuthorizationHeader(),
                             encodingCompletion: { result in
                                switch result {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        self.responseResult(response, success: {result in
                                            
                                            success(result as! Dictionary<String, AnyObject>)
                                        }, failure: {error in
                                            
                                            failure(error)
                                        })
                                    }
                                case .failure(let encodingError):
                                    failure(encodingError as NSError)
                                }
            }
                
            )
            
            
        }else{
            Alamofire.upload (
                multipartFormData: { multipartFormData in
                    for (key , value) in parameters {
                        if let data:Data = value as? Data {
                            //multipartFormData.append(data, withName: key, fileName: "fileName.jpeg", mimeType: "image/jpeg")
                            multipartFormData.append(data, withName: key, fileName: "\(key)\(data.getExtension)", mimeType: "\(data.mimeType)")
                        } else {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                        }
                    }
            },
                to: route,
                encodingCompletion: { result in
                    switch result {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            self.responseResult(response, success: {result in
                                success(result as! Dictionary<String, AnyObject>)
                            }, failure: {error in
                                
                                failure(error)
                            })
                        }
                    case .failure(let encodingError):
                        failure(encodingError as NSError)
                    }
            }
            )
        }
    }
    
    func postWPRequestWithMultipart(route: URL,parameters: Parameters,
                                  success:@escaping DefaultAPISuccessClosure,
                                  failure:@escaping DefaultAPIFailureClosure , withHeader: Bool){
        if withHeader{
            Alamofire.upload(multipartFormData:{ multipartFormData in
                for (key , value) in parameters {
                    if let data:Data = value as? Data {
                        //multipartFormData.append(data, withName: key, fileName: "fileName.jpeg", mimeType: "image/jpeg")
                        multipartFormData.append(data, withName: key, fileName: "\(key)\(data.getExtension)", mimeType: "\(data.mimeType)")
                    } else {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                }
            },
                             usingThreshold:UInt64.init(),
                             to: route,
                             method:.post,
                             headers: getAuthorizationHeader(),
                             encodingCompletion: { result in
                                switch result {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        self.responseWPResult(response, success: {result in
                                            
                                            success(result as! Dictionary<String, AnyObject>)
                                        }, failure: {error in
                                            
                                            failure(error)
                                        })
                                    }
                                case .failure(let encodingError):
                                    failure(encodingError as NSError)
                                }
            }
                
            )
            
            
        }else{
            Alamofire.upload (
                multipartFormData: { multipartFormData in
                    for (key , value) in parameters {
                        if let data:Data = value as? Data {
                            //multipartFormData.append(data, withName: key, fileName: "fileName.jpeg", mimeType: "image/jpeg")
                            multipartFormData.append(data, withName: key, fileName: "\(key)\(data.getExtension)", mimeType: "\(data.mimeType)")
                        } else {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                        }
                    }
            },
                to: route,
                encodingCompletion: { result in
                    switch result {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            self.responseWPResult(response, success: {result in
                                success(result as! Dictionary<String, AnyObject>)
                            }, failure: {error in
                                
                                failure(error)
                            })
                        }
                    case .failure(let encodingError):
                        failure(encodingError as NSError)
                    }
            }
            )
        }
    }
    func postRequestArrayWith(route: URL,parameters: Parameters,
                              success:@escaping DefaultArrayResultAPISuccessClosure,
                              failure:@escaping DefaultAPIFailureClosure, withHeaders:Bool){
        
        if withHeaders {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                self.responseResult(response, success: {response in
                    
                    success(response as! [AnyObject])
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(response as! [AnyObject])
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }
    }
    func postRequestWithMultipart(route: URL,parameters: Parameters,
                                  success:@escaping DefaultArrayResultAPISuccessClosure,
                                  failure:@escaping DefaultAPIFailureClosure , withHeader: Bool){
        if withHeader{
            Alamofire.upload(multipartFormData:{ multipartFormData in
                for (key , value) in parameters {
                    if let data:Data = value as? Data {
                        multipartFormData.append(data, withName: key, fileName: "\(data.getExtension)", mimeType: "\(data.mimeType)")
                    } else {
                        multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                    }
                }
            },
                             usingThreshold:UInt64.init(),
                             to: route,
                             method:.post,
                             headers: getAuthorizationHeader(),
                             encodingCompletion: { result in
                                switch result {
                                case .success(let upload, _, _):
                                    upload.responseJSON { response in
                                        self.responseResult(response, success: {result in
                                            
                                            success(result as! Array<AnyObject>)
                                        }, failure: {error in
                                            
                                            failure(error)
                                        })
                                    }
                                case .failure(let encodingError):
                                    failure(encodingError as NSError)
                                }
            }
                
            )
            
            
        }else{
            Alamofire.upload (
                multipartFormData: { multipartFormData in
                    for (key , value) in parameters {
                        if let data:Data = value as? Data {
                            multipartFormData.append(data, withName: key, fileName: "fileName.jpeg", mimeType: "image/jpeg")
                        } else {
                            multipartFormData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                        }
                    }
                    
            },
                to: route,
                encodingCompletion: { result in
                    switch result {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            self.responseResult(response, success: {result in
                                
                                success(result as! Array<AnyObject>)
                            }, failure: {error in
                                
                                failure(error)
                            })
                        }
                    case .failure(let encodingError):
                        failure(encodingError as NSError)
                    }
            }
            )
        }
    }
    func postRequestWith(route: URL,parameters: Parameters,
                         success:@escaping DefaultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure, withHeaders:Bool){
        
        if withHeaders {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(response as! Dictionary<String, AnyObject>)
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    if let responseObject = response as? Dictionary<String, AnyObject>{
                        success(responseObject)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }
    }
    // for bool
    func postRequestForBoolWith(route: URL,parameters: Parameters,
                                success:@escaping DefaultBoolResultAPISuccesClosure,
                                failure:@escaping DefaultAPIFailureClosure, withHeaders:Bool){
        
        if withHeaders {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(true)
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(true)
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }
    }
    // for String
    func postRequestForIntWith(route: URL,parameters: Parameters,
                                success:@escaping DefaultIntResultAPISuccesClosure,
                                failure:@escaping DefaultAPIFailureClosure, withHeaders:Bool){
        
        if withHeaders {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(response as! Int)
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(response as! Int)
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }
    }
    
    // MARK: GET REQUESTES FOR ARRAY
    func getRequestForArray(route: URL,parameters: Parameters,
                        success:@escaping DefaultArrayResultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure,
                        withHeader: Bool)
    {
        if(withHeader)
        {
            Alamofire.request(route, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON {
                response in
                
                self.responseResult(response, success: {
                    response in
                    
                    success(response as! Array<AnyObject>)
                    
                }, failure: {error in
                    
                    failure(error as NSError)
                })}
        }
        else
        {
            Alamofire.request(route, method: .get, parameters: parameters, encoding: URLEncoding()).responseJSON {
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(response as! Array<AnyObject>)
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    // For Dictionary Responce
    func getRequestForDictionaryWith(route: URL,
                                     success:@escaping DefaultAPISuccessClosure,
                                     failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if(withHeader)
        {
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(response as! Dictionary<String, AnyObject>)
                    
                }, failure: {error in
                    failure(error as NSError)
                })
                
            }
            
        }
        else
        {
            Alamofire.request(route, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON {
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(response as! Dictionary<String, AnyObject>)
                    
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func getWPAnyResponseWith(route: URL, success:@escaping DefaultAPIAnyObject, failure:@escaping DefaultAPIFailureClosure) {
        Alamofire.request(route, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON {
            response in
            self.responseWPResult(response, success: {response in
                success(response)
            }, failure: {error in
                failure(error as NSError)
            })
        }
    }
    
    // For Dictionary Responce
    func getWPResponseWith(route: URL,
                                     success:@escaping DefaultAPISuccessClosure,
                                     failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if(withHeader)
        {
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                
                self.responseWPResult(response, success: {response in
                    
                    success(response as! Dictionary<String, AnyObject>)
                    
                }, failure: {error in
                    failure(error as NSError)
                })
                
            }
            
        }
        else
        {
            Alamofire.request(route, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON {
                response in
                
                self.responseWPResult(response, success: {response in
                    success(response as! Dictionary<String, AnyObject>)
                    
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    // For Dictionary Responce
    func getWPArrayResponseWith(route: URL,
                           success:@escaping DefaultArrayResultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure, withHeader: Bool){
        if(withHeader)
        {
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                
                self.responseWPResult(response, success: {response in
                    
                    success(response as! Array<AnyObject>)
                    
                }, failure: {error in
                    failure(error as NSError)
                })
                
            }
            
        }
        else
        {
            Alamofire.request(route, method: .get, parameters: nil, encoding: URLEncoding.default).responseJSON {
                response in
                
                self.responseWPResult(response, success: {response in
                    
                    success(response as! Array<AnyObject>)
                    
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
    }
    
    func postWPRequestWith(route: URL,parameters: Parameters,
                         success:@escaping DefaultAPISuccessClosure,
                         failure:@escaping DefaultAPIFailureClosure, withHeaders:Bool){
        
        if withHeaders {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                
                self.responseWPResult(response, success: {response in
                    
                    success(response as! Dictionary<String, AnyObject>)
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }else {
            Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
                response in
                
                self.responseWPResult(response, success: {response in
                    if let responseObject = response as? Dictionary<String, AnyObject>{
                        success(responseObject)
                    }
                    else{
                        success(Dictionary<String, AnyObject>())
                    }
                }, failure: {error in
                    
                    failure(error as NSError)
                })
            }
        }
    }
    // FOR BOOL RESPONCE
    func getRequestForBoolWith(route: URL,
                               success:@escaping DefaultBoolResultAPISuccesClosure,
                               failure:@escaping DefaultAPIFailureClosure, withHeader: Bool)
    {
        if(withHeader)
        {
            Alamofire.request(route, method: .get, encoding: JSONEncoding.default, headers: getAuthorizationHeader()).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(true)
                    
                }, failure: {error in
                    failure(error as NSError)
                })
                
            }
            
        }
        else
        {
            
            Alamofire.request(route, method: .get , parameters: nil, encoding: JSONEncoding.default).responseJSON{
                response in
                
                self.responseResult(response, success: {response in
                    
                    success(true)
                    
                }, failure: {error in
                    failure(error as NSError)
                })
            }
        }
        
    }
    
    
    func putRequestWith(route: URL,parameters: Parameters,
                        success:@escaping DefaultAPISuccessClosure,
                        failure:@escaping DefaultAPIFailureClosure){
        
        Alamofire.request(route, method: .put, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
            response in
            
            self.responseResult(response, success: {response in
                
                success(response as! Dictionary<String, AnyObject>)
            }, failure: {error in
                
                failure(error as NSError)
            })
        }
    }
    
    func deleteRequestWith(route: URL,parameters: Parameters,
                           success:@escaping DefaultAPISuccessClosure,
                           failure:@escaping DefaultAPIFailureClosure){
        
        Alamofire.request(route, method: .delete, parameters: parameters, encoding: JSONEncoding.default).responseJSON{
            response in
            
            self.responseResult(response, success: {response in
                
                success(response as! Dictionary<String, AnyObject>)
            }, failure: {error in
                
                failure(error as NSError)
            })
        }
    }
    func deleteRequestWithT(endPoint: String, success:@escaping DefaultAPISuccessClosure, failure:@escaping DefaultAPIFailureClosure) {
        Alamofire.request(endPoint, method: .delete, headers:getAuthorizationHeader())
            .responseJSON { response in
                self.responseResult(response, success: {response in
                    
                    success(response as! Dictionary<String, AnyObject>)
                }, failure: {error in
                    
                    failure(error as NSError)
                })
        }
    }
    // MARK: DOWNLOAD FILE
    func downloadWith(downloadUrl:String, saveUrl:String, index: Int, success: @escaping DefaultDownloadSuccessClosure, downloadProgress: @escaping DefaultDownloadProgressClosure, failure: @escaping DefaultDownloadFailureClosure) {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let file = directoryURL.appendingPathComponent(saveUrl + ".m3u8", isDirectory: false)
            return (file, [.createIntermediateDirectories, .removePreviousFile])
        }
        Alamofire.download(downloadUrl, to: destination)
            .downloadProgress { progress in
                print("Download Progress: \(progress.fractionCompleted)")
                downloadProgress(progress.fractionCompleted,index)
            }
            .responseData(completionHandler: {
                response in
                if response.result.error == nil {
                    //print("Success: Downloading file: \(response.request) completed.")
                    if let data = response.result.value {
                        success(data)
                    }
                }else {
                    print("Error : \(response.result.error.debugDescription)")
                    let errorMessage = response.result.error.debugDescription
                    let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                    let error = NSError(domain: "Domain", code: 1000 , userInfo: userInfo)
                    if response.resumeData != nil {
                        failure(error,response.resumeData!, true)
                    }else {
                        failure(error,Data(),false)
                    }
                    
                }
            })
    }
    func resumeWith (data: Data, index: Int, saveUrl:String, success: @escaping DefaultDownloadSuccessClosure, downloadProgress: @escaping DefaultDownloadProgressClosure, failure: @escaping DefaultDownloadFailureClosure) {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let file = directoryURL.appendingPathComponent(saveUrl + ".mp3", isDirectory: false)
            return (file, [.createIntermediateDirectories])
            
        }
        Alamofire.download(resumingWith: data, to: destination).downloadProgress { progress in
            print("Download Progress: \(progress.fractionCompleted)")
            downloadProgress(progress.fractionCompleted,index)
            }
            .responseData(completionHandler: {
                response in
                print(response.result.isSuccess)
                if response.result.error == nil {
                    //print("Success: Downloading file: \(response.request) completed.")
                    if let data = response.result.value {
                        success(data)
                    }
                }else {
                    print("Error : \(response.result.error.debugDescription)")
                    let errorMessage = response.result.error.debugDescription
                    let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                    let error = NSError(domain: "Domain", code: 1000 , userInfo: userInfo)
                    if response.resumeData != nil {
                        failure(error,response.resumeData!, true)
                    }else {
                        failure(error,Data(),false)
                    }
                }
            })
    }
    
    // MARK: Closing downloadImageT
    //    func downloadImageT(route: String, success:@escaping DefaultImageResultClosure) {
    //        Alamofire.request(route).responseData { response in
    //            self.responseImage(response, success: success)
    //        }
    //    }
    fileprivate func responseImage(_ response: DataResponse<UIImage>, success: @escaping (_ response: UIImage) -> Void) {
        if let image = response.result.value {
            print(image)
            success(image)
        }
    }
    
    // MARK: - Response Handling
    
    fileprivate func responseResult(_ response:DataResponse<Any>,
                                    success: @escaping (_ response: AnyObject) -> Void,
                                    failure: @escaping (_ error: NSError) -> Void
        ) {
        switch response.result
        {
        case .success:
            if let dictData = response.result.value as? NSDictionary {
                if (dictData["status"] as? String == "success" ||
                    dictData["code"] as? Int == 200) {
                    if let data = dictData["data"] {
                        success(data as AnyObject)
                    } else {
                        success(dictData as AnyObject)
                    }
                } else {
                    //Failure
                    let errorMessage: String = (dictData["reason"] as? String) ??
                    "There is something went wrong. Please try again later";
                    
                    let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                    let error = NSError(domain: "Error", code: 0, userInfo: userInfo)
                    failure(error)
                }
            } else {
                //API Failure
                let errorMessage: String = "There is something went wrong. Please try again later";
                let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                let error = NSError(domain: "Error", code: 0, userInfo: userInfo);
                failure(error)
            }
            
        case .failure(let error):
            failure(error as NSError)
        }
    }

    
    fileprivate func responseResultOld(_ response:DataResponse<Any>,
                                    success: @escaping (_ response: AnyObject) -> Void,
                                    failure: @escaping (_ error: NSError) -> Void) {
        switch response.result {
        case .success:
            if let dictData = response.result.value as? NSDictionary {
                let errorCode: Int = (dictData["Error"] as? Int) ?? 0;
                guard let status = dictData["status"] as? String else {
                    Utility.showAlert(title: "Error", message: "Unknown error")
                    Utility.hideLoader()
                    return
                }
                
                if status == "success"{
                    //Success
                    if dictData["data"] != nil{
                        success(dictData["data"] as AnyObject)
                    }
                    else{
                        success(true as AnyObject)
                    }
                } else {
                    //Failure
                    let errorMessage: String = (dictData["reason"] as? String) ?? "Unknown error";
                    let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                    
                    let error = NSError(domain: "Domain", code: errorCode, userInfo: userInfo)
                    failure(error)
                    Utility.hideLoader()
                    //Utility.showAlert(title: "Error", message: error.localizedDescription)
                }
            } else {
                //Failure
                let errorMessage: String = "Unknown error";
                let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                let error = NSError(domain: "Domain", code: 0, userInfo: userInfo);
                failure(error)
                Utility.hideLoader()
                //Utility.showAlert(title: "Error", message: error.localizedDescription)
            }
            
        case .failure(let error):
            failure(error as NSError)
            Utility.hideLoader()
            //Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    //MARK: - Response Handling
    fileprivate func responseWPResult(_ response:DataResponse<Any>,
                                    success: @escaping (_ response: AnyObject) -> Void,
                                    failure: @escaping (_ error: NSError) -> Void){
        switch response.result {
        case .success:
            if let dictData = response.result.value as? NSDictionary {
                
                let errorMessage:String = (dictData["message"] as? String) ?? ""
                
                if !errorMessage.isEmpty{
                    Utility.hideLoader()
                    Utility.showAlert(title: "Error", message: errorMessage)
                }
                else{
                     success(dictData as AnyObject)
                }
            }
            else{
                success(response.result.value as AnyObject)
            }
        case .failure(let error):
            failure(error as NSError)
            Utility.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
    fileprivate func multipartFormData(parameters: Parameters) {
        let formData: MultipartFormData = MultipartFormData()
        if let params:[String:AnyObject] = parameters as [String : AnyObject]? {
            for (key , value) in params {
                
                if let data:Data = value as? Data {
                    formData.append(data, withName: key, fileName: "fileName", mimeType: data.mimeType)
                } else {
                    formData.append("\(value)".data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
                }
            }
        }
    }
}


public extension Data {
    public var mimeType:String {
        get {
            var c = [UInt32](repeating: 0, count: 1)
            (self as NSData).getBytes(&c, length: 1)
            switch (c[0]) {
            case 0xFF:
                return "image/jpeg";
            case 0x89:
                return "image/png";
            case 0x47:
                return "image/gif";
            case 0x49, 0x4D:
                return "image/tiff";
            case 0x25:
                return "application/pdf";
            case 0xD0:
                return "application/vnd";
            case 0x46:
                return "text/plain";
            default:
                print("mimeType for \(c[0]) in available");
                return "application/octet-stream";
            }
        }
    }
    public var getExtension: String {
        get {
            var c = [UInt32](repeating: 0, count: 1)
            (self as NSData).getBytes(&c, length: 1)
            switch (c[0]) {
            case 0xFF:
                return "_IMG.jpeg";
            case 0x89:
                return "_IMG.png";
            case 0x47:
                return "_IMG.gif";
            case 0x49, 0x4D:
                return "_IMG.tiff";
            case 0x25:
                return "_FILE.pdf";
            case 0xD0:
                return "_FILE.vnd";
            case 0x46:
                return "_FILE.txt";
            default:
                print("mimeType for \(c[0]) in available");
                return "_video.mp4";
            }
        }
    }
}
