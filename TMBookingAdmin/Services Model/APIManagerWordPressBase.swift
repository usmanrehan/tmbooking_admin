//
//  APIManagerWordPressBase.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 08/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

class APIManagerWordPressBase: NSObject {
    let baseUrl = Constants.WpBaseURL
    
    func getAuthorizationHeader (addToken: Bool) -> Dictionary<String,String>{
        if addToken {
            return ["Authorization": "Bearer " +  Defaults[.token],//Default[.token] its default value is empty
                "Content-Type":"application/json"]
        } else {
            return [
                "Content-Type":"application/json"
            ]
        }
    }
    
    func GETURLfor(route:String) -> URL?{
        if let components: NSURLComponents = NSURLComponents(string: (baseUrl+route)){
            return components.url! as URL
        }
        return nil
    }
    
    func GETURLfor(route: String, params:[String: Any]) -> URL? {
        if let components: NSURLComponents  = NSURLComponents(string: (baseUrl+route)){
            var queryItems = [NSURLQueryItem]()
            for(key,value) in params{
                queryItems.append(NSURLQueryItem(name:key,value: "\(value)"))
            }
            components.queryItems = queryItems as [URLQueryItem]?
            
            return components.url as URL?
        }
        return nil;
    }
    
    func POSTURLfor(route:String) -> URL? {
        if let components: NSURLComponents = NSURLComponents(string: (baseUrl+route)){
            return components.url! as URL
        }
        return nil
    }

    func postRequestObjectWithToken(route: URL,parameters: Parameters, needToken: Bool,
                                    success:@escaping DefaultAPIAnyObject,
                                    failure:@escaping DefaultAPIFailureClosure){
        Alamofire.request(route, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: getAuthorizationHeader(addToken: needToken)).responseJSON{
            response in
            
            self.responseResult(response, success: {response in
                print("Success Response: ",response)
                success(response)
            }, failure: {error in
                print("Success Response: ",response)
                failure(error as NSError)
            })
            
        }
    }
    
    func getRequestObjectWith(route: URL,
                              success:@escaping DefaultAPIAnyObject,
                              failure:@escaping DefaultAPIFailureClosure) {
        Alamofire.request(route, method: .get, encoding: JSONEncoding.default).responseJSON{
            response in
            
            self.responseResult(response, success: {response in
                print("Success Response: ",response)
                success(response)
            }, failure: {error in
                print("Success Response: ",response)
                failure(error as NSError)
            })
        }
    }
    
    fileprivate func responseResult(_ response:DataResponse<Any>,
                                    success: @escaping (_ response: AnyObject) -> Void,
                                    failure: @escaping (_ error: NSError) -> Void
        ) {
        switch response.result
        {
        case .success:
            if let value = response.result.value {
                let dictData = value as AnyObject
                //Check if response contains success key
                if let status = dictData["success"] as? Bool {
                    if status {
                        //Get data from response
                        if let data = dictData["data"] {
                            success(data as AnyObject)
                        }
                    } else {
                        let errorMessage: String = (dictData["message"] as? String) ?? "There is something went wrong. Please try again later";
                        let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                        let error = NSError(domain: "Error", code: 0, userInfo: userInfo)
                        failure(error)
                    }
                } else {
                    //Check other case if success not found, message key contains error information
                    if let errorMessage = dictData["message"] as? String {
                        let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                        let error = NSError(domain: "Error", code: 0, userInfo: userInfo)
                        failure(error)
                    } else {
                        success(dictData as AnyObject)
                    }
                }
            } else {
                //Failure
                let errorMessage: String = "There is something went wrong. Please try again later";
                let userInfo = [NSLocalizedFailureReasonErrorKey: errorMessage]
                let error = NSError(domain: "Error", code: 0, userInfo: userInfo);
                failure(error)
            }
        case .failure(let error):
            failure(error as NSError)
        }
    }
}
