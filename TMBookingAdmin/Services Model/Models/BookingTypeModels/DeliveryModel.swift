//
//  DeliveryModel.swift
//
//  Created by Hamza Hasan on 06/12/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class DeliveryModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let bookingId = "booking_id"
    static let status = "status"
    static let name = "name"
    static let roomNumber = "room_number"
    static let guestName = "guest_name"
    static let date = "date"
    static let deliveryId = "delivery_id"
    static let timestamp = "timestamp"
    static let time = "time"
    static let contactNumber = "contact_number"
    static let quantity = "quantity"
    static let hotelName = "hotel_name"
  }

  // MARK: Properties
  @objc dynamic var bookingId: String? = ""
  @objc dynamic var status: String? = ""
  @objc dynamic var name: String? = ""
  @objc dynamic var roomNumber = 0
  @objc dynamic var guestName: String? = ""
  @objc dynamic var date: String? = ""
  @objc dynamic var deliveryId: String? = ""
  @objc dynamic var timestamp = 0
  @objc dynamic var time: String? = ""
  @objc dynamic var contactNumber: String? = ""
  @objc dynamic var quantity: String? = ""
  @objc dynamic var hotelName: String? = ""
    

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "bookingId"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    bookingId <- map[SerializationKeys.bookingId]
    status <- map[SerializationKeys.status]
    name <- map[SerializationKeys.name]
    roomNumber <- map[SerializationKeys.roomNumber]
    guestName <- map[SerializationKeys.guestName]
    date <- map[SerializationKeys.date]
    deliveryId <- map[SerializationKeys.deliveryId]
    timestamp <- map[SerializationKeys.timestamp]
    time <- map[SerializationKeys.time]
    contactNumber <- map[SerializationKeys.contactNumber]
    quantity <- map[SerializationKeys.quantity]
    hotelName <- map[SerializationKeys.hotelName]
  }


}
