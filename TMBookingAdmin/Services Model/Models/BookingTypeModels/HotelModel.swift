//
//  HotelModel.swift
//
//  Created by Hamza Hasan on 06/12/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class HotelModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let bookingId = "booking_id"
    static let name = "name"
    static let totalRooms = "total_rooms"
    static let numberOfGuests = "number_of_guests"
    static let date = "date"
    static let type = "type"
    static let timestamp = "timestamp"
    static let checkInStatus = "check_in_status"
    static let supplierCode = "supplier_code"
    static let checkoutDate = "checkout_date"
    static let status = "status"
    static let agentName = "agent_name"
    static let time = "time"
    static let confirmationNumber = "confirmation_number"
    static let agentRefferenceNumber = "agent_refference_number"
    static let orderId = "order_id"
    static let roomNumber = "room_number"
    static let isPackageBooking = "is_package_booking"
    static let packageAccommodationId = "package_booking_accommodation_id"
  }

  // MARK: Properties
  @objc dynamic var bookingId: String? = ""
  @objc dynamic var name: String? = ""
  @objc dynamic var totalRooms: String? = ""
  @objc dynamic var numberOfGuests = 0
  @objc dynamic var date: String? = ""
  @objc dynamic var type: String? = ""
  @objc dynamic var timestamp = 0
  @objc dynamic var supplierCode: String? = ""
  @objc dynamic var checkoutDate: String? = ""
  @objc dynamic var status: String? = ""
  @objc dynamic var agentName: String? = ""
  @objc dynamic var time: String? = ""
  @objc dynamic var confirmationNumber: String? = ""
  @objc dynamic var agentRefferenceNumber: String? = ""
  @objc dynamic var orderId: String? = ""
  @objc dynamic var roomNumber: String? = ""
  @objc dynamic var isPackageBooking: Bool = false
  @objc dynamic var packageAccommodationId: String? = ""

  var checkInStatus: Bool? = false

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "bookingId"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    bookingId <- map[SerializationKeys.bookingId]
    name <- map[SerializationKeys.name]
    totalRooms <- map[SerializationKeys.totalRooms]
    numberOfGuests <- map[SerializationKeys.numberOfGuests]
    date <- map[SerializationKeys.date]
    type <- map[SerializationKeys.type]
    timestamp <- map[SerializationKeys.timestamp]
    checkInStatus <- map[SerializationKeys.checkInStatus]
    supplierCode <- map[SerializationKeys.supplierCode]
    checkoutDate <- map[SerializationKeys.checkoutDate]
    status <- map[SerializationKeys.status]
    agentName <- map[SerializationKeys.agentName]
    time <- map[SerializationKeys.time]
    confirmationNumber <- map[SerializationKeys.confirmationNumber]
    agentRefferenceNumber <- map[SerializationKeys.agentRefferenceNumber]
    orderId <- map[SerializationKeys.orderId]
    roomNumber <- map[SerializationKeys.roomNumber]
    isPackageBooking <- map[SerializationKeys.isPackageBooking]
    packageAccommodationId <- map[SerializationKeys.packageAccommodationId]
  }
}
