//
//  TransportModel.swift
//
//  Created by Hamza Hasan on 06/12/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class TransportModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let bookingId = "booking_id"
    static let name = "name"
    static let date = "date"
    static let timestamp = "timestamp"
    static let type = "type"
    static let status = "status"
    static let dropoffPoint = "dropoff_point"
    static let supplierCode = "supplier_code"
    static let agentName = "agent_name"
    static let pickupPoint = "pickup_point"
    static let time = "time"
    static let agentRefferenceNumber = "agent_refference_number"
    static let confirmationNumber = "confirmation_number"
    static let orderId = "order_id"
	static let isPackageBooking = "is_package_booking"

  }

  // MARK: Properties
  @objc dynamic var bookingId: String? = "-"
  @objc dynamic var name: String? = "-"
  @objc dynamic var date: String? = "-"
  @objc dynamic var timestamp = 0
  @objc dynamic var type: String? = "-"
  @objc dynamic var status: String? = "-"
  @objc dynamic var dropoffPoint: String? = "-"
  @objc dynamic var supplierCode: String? = "-"
  @objc dynamic var agentName: String? = "-"
  @objc dynamic var pickupPoint: String? = "-"
  @objc dynamic var time: String? = "-"
  @objc dynamic var agentRefferenceNumber: String? = "-"
  @objc dynamic var confirmationNumber: String? = "-"
  @objc dynamic var orderId: String? = "-"
  @objc dynamic var isPackageBooking: Bool = false

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "bookingId"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    bookingId <- map[SerializationKeys.bookingId]
    name <- map[SerializationKeys.name]
    date <- map[SerializationKeys.date]
    timestamp <- map[SerializationKeys.timestamp]
    type <- map[SerializationKeys.type]
    status <- map[SerializationKeys.status]
    dropoffPoint <- map[SerializationKeys.dropoffPoint]
    supplierCode <- map[SerializationKeys.supplierCode]
    agentName <- map[SerializationKeys.agentName]
    pickupPoint <- map[SerializationKeys.pickupPoint]
    time <- map[SerializationKeys.time]
    agentRefferenceNumber <- map[SerializationKeys.agentRefferenceNumber]
    confirmationNumber <- map[SerializationKeys.confirmationNumber]
	isPackageBooking <- map[SerializationKeys.isPackageBooking]
    orderId <- map[SerializationKeys.orderId]
  }
}
