//
//  Madina.swift
//
//  Created by Hamza Hasan on 06/12/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class BookingTypeModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let selectedData = "selectedData"
    static let hotels = "hotels"
    static let transport = "transport"
    static let delivery = "delivery"
    static let id = "id"
  }

  // MARK: Properties
  @objc dynamic var selectedData: String? = ""
  var hotels = List<HotelModel>()
  var transport = List<TransportModel>()
  var delivery = List<DeliveryModel>()
  @objc dynamic var id: String? = ""
  var isSelected = false
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    selectedData <- map[SerializationKeys.selectedData]
    hotels <- (map[SerializationKeys.hotels], ListTransform<HotelModel>())
    transport <- (map[SerializationKeys.transport], ListTransform<TransportModel>())
    delivery <- (map[SerializationKeys.delivery], ListTransform<DeliveryModel>())
    id <- map[SerializationKeys.id]
  }
}
