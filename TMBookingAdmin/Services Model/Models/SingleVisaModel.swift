//
//  SingleVisaModel.swift
//
//  Created by Hamza Hasan on 06/01/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper

public class SingleVisaModel: NSObject, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let visatype = "visatype"
        static let fullname = "fullname"
        static let validity = "validity"
        static let orderid = "orderid"
        static let country = "country"
        static let countryId = "country_id"
        static let memberid = "memberid"
        static let formid = "formid"
        static let applicationId = "applicationid"
        static let date = "date"
        static let paymentStatus = "payment_status"
        static let isLead = "is_lead"
        static let relation = "relation"
        static let visaStatus = "status_name"
        static let applicationType = "type"
        static let price = "price"
        static let saudiFeeApplicable = "saudi_fee_applicable"
    }
    
    // MARK: Properties
    @objc dynamic var visatype: String? = ""
    @objc dynamic var fullname: String? = ""
    @objc dynamic var validity: String? = ""
    @objc dynamic var orderid: String? = ""
    @objc dynamic var country: String? = ""
    @objc dynamic var countryId: String? = ""
    @objc dynamic var memberid: String? = ""
    @objc dynamic var formid: String? = ""
    @objc dynamic var applicationId: String? = ""
    @objc dynamic var date: String? = ""
    @objc dynamic var paymentStatus: String? = ""
    @objc dynamic var isLead: String? = ""
    @objc dynamic var relation: String? = ""
    @objc dynamic var visaStatus: String? = ""
    @objc dynamic var applicationType: String? = ""
    @objc dynamic var price: String? = ""
    @objc dynamic var saudiFeeApplicable: String?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required public convenience init?(map : Map){
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        visatype <- map[SerializationKeys.visatype]
        fullname <- map[SerializationKeys.fullname]
        validity <- map[SerializationKeys.validity]
        orderid <- map[SerializationKeys.orderid]
        country <- map[SerializationKeys.country]
        countryId <- map[SerializationKeys.countryId]
        memberid <- map[SerializationKeys.memberid]
        formid <- map[SerializationKeys.formid]
        applicationId <- map[SerializationKeys.applicationId]
        date <- map[SerializationKeys.date]
        paymentStatus <- map[SerializationKeys.paymentStatus]
        isLead <- map[SerializationKeys.isLead]
        relation <- map[SerializationKeys.relation]
        visaStatus <- map[SerializationKeys.visaStatus]
        applicationType <- map[SerializationKeys.applicationType]
        price <- map[SerializationKeys.price]
        saudiFeeApplicable <- map[SerializationKeys.saudiFeeApplicable]
    }
}
