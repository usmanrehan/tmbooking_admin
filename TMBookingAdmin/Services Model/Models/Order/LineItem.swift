//
//  LineItem.swift
//  Template
//
//  Created by Akber Sayni on 12/05/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

public struct LineItem: Mappable{
    var id : Int?
    var name : String?
    var product_id : Int?
    var variation_id : Int?
    var quantity : Int?
    var tax_class : String?
    var subtotal : String?
    var subtotal_tax : String?
    var total : String?
    var total_tax : String?
    var meta_data : [MetaData]?
    var sku : String?
    var price : Float?

    public init?(map: Map) {}
    
    mutating public func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        product_id <- map["product_id"]
        variation_id <- map["variation_id"]
        quantity <- map["quantity"]
        tax_class <- map["tax_class"]
        subtotal <- map["subtotal"]
        subtotal_tax <- map["subtotal_tax"]
        total <- map["total"]
        total_tax <- map["total_tax"]
        meta_data <- map["meta_data"]
        sku <- map["sku"]
        price <- map["price"]
    }
}
