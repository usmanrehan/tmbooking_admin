//
//  OrderStatusModel.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 16/07/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class OrderStatusModel: NSObject, Mappable {
    var key: String?
    var value: String?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required public convenience init?(map : Map){
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        key <- map["key"]
        value <- map["value"]
    }
}
