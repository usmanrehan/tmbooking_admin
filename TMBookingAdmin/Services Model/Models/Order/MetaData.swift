//
//  MetaData.swift
//  Template
//
//  Created by Akber Sayni on 12/05/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

struct MetaData: Mappable {
    var id : Int?
    var key : String?
    var value : String?
    
    public init?(map: Map) {}
    
    mutating public func mapping(map: Map) {
        id <- map["id"]
        key <- map["key"]
        value <- map["value"]
    }
}
