//
//  PaymentDetails.swift
//  Template
//
//  Created by Akber Sayni on 12/05/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

public struct PaymentDetails: Mappable {
    var methodId: String?
    var methodTitle: String?
    var paid: Bool?
    
    public init?(map: Map) {}
    
    mutating public func mapping(map: Map) {
        methodId <- map["method_id"]
        methodTitle <- map["method_title"]
        paid <- map["paid"]
    }
}
