//
//  Order.swift
//  Template
//
//  Created by Akber Sayni on 12/05/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class Order: Mappable {
    var id: Int?
    var orderNumber: String?
    var createdAt: String?
    var updatedAt: String?
    var completedAt: String?
    var status: String?
    var currency: String?
    var total: String?
    var subtotal: String?
    var totalTax: String?
    var totalShipping: String?
    var cartTax: String?
    var shippingTax: String?
    var totalDiscount: String?
    var shippingMethods: String?
    var paymentMethod: String?
    var paymentMethodTitle: String?
    var billingAddress: Address?
    var shippingAddress: Address?
    var note: String?
    var customerIp: String?
    var customerUserAgent: String?
    var customerId: Int?
    var viewOrderUrl: NSURL?
    var lineItems: [LineItem]?
    var shippingLines: String?
    var taxLines: String?
    var feeLines: [OrderTax]?
    var couponLines: String?
    
    convenience required init?(map: Map) {
        self.init()
    }

    public func mapping(map: Map) {
        id <- map["id"]
        orderNumber <- map["order_key"]
        createdAt <- map["date_created"]
        updatedAt <- map["date_modified"]
        completedAt <- map["date_modified"]
        status <- map["status"]
        currency <- map["currency"]
        total <- map["total"]
        subtotal <- map["subtotal"]
        totalTax <- map["total_tax"]
        totalShipping <- map["shipping_total"]
        cartTax <- map["cart_tax"]
        shippingTax <- map["shipping_tax"]
        totalDiscount <- map["discount_total"]
        shippingMethods <- map["shipping_methods"]
        paymentMethod <- map["payment_method"]
        paymentMethodTitle <- map["payment_method_title"]
        billingAddress <- map["billing"]
        shippingAddress <- map["shipping"]
        lineItems <- map["line_items"]
        note <- map["note"]
        customerIp <- map["customer_ip"]
        customerUserAgent <- map["customer_user_agent"]
        customerId <- map["customer_id"]
        viewOrderUrl <- map["view_order_url"]
        shippingLines <- map["shipping_lines"]
        lineItems <- map["line_items"]
        taxLines <- map["tax_lines"]
        feeLines <- map["fee_lines"]
        couponLines <- map["coupon_lines"]
    }
}

class OrderTax: Mappable {
    var name: String?
    var total: String?
    
    convenience required init?(map: Map) {
        self.init()
    }
 
    public func mapping(map: Map) {
        name <- map["name"]
        total <- map["total"]
    }
}

/*
 [
 {
 "name":"VAT",
 "total":"0.00"
 }]
 */
