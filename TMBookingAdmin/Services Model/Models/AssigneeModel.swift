//
//  MeetGreetModel.swift
//
//  Created by Hamza Hasan on 07/12/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class AssigneeModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let userId = "user_id"
        static let name = "name"
    }
    
    // MARK: Properties
    @objc dynamic var userId: String? = ""
    @objc dynamic var name: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "userId"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        userId <- map[SerializationKeys.userId]
        name <- map[SerializationKeys.name]
    }
    
    
}
