//
//  VisaApplicationModel.swift
//
//  Created by Hamza Hasan on 29/08/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class VisaApplicationModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let reason = "reason"
    static let email = "email"
    static let fileUrl = "file_url"
    static let mobile = "mobile"
    static let livingin = "livingin"
    static let date = "date"
    static let nationality = "nationality"
    static let status = "status"
    static let id = "id"
    static let visatypeid = "visatypeid"
    static let photo = "photo"
    static let fullname = "fullname"
    static let userid = "userid"
    static let referalcode = "referalcode"
  }

  // MARK: Properties
  @objc dynamic var reason: String? = ""
  @objc dynamic var email: String? = ""
  @objc dynamic var fileUrl: String? = ""
  @objc dynamic var mobile: String? = ""
  @objc dynamic var livingin: String? = ""
  @objc dynamic var date: String? = ""
  @objc dynamic var nationality: String? = ""
  @objc dynamic var status: String? = ""
  @objc dynamic var id: String? = ""
  @objc dynamic var visatypeid: String? = ""
  @objc dynamic var photo: String? = ""
  @objc dynamic var fullname: String? = ""
  @objc dynamic var userid: String? = ""
  @objc dynamic var referalcode: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    reason <- map[SerializationKeys.reason]
    email <- map[SerializationKeys.email]
    fileUrl <- map[SerializationKeys.fileUrl]
    mobile <- map[SerializationKeys.mobile]
    livingin <- map[SerializationKeys.livingin]
    date <- map[SerializationKeys.date]
    nationality <- map[SerializationKeys.nationality]
    status <- map[SerializationKeys.status]
    id <- map[SerializationKeys.id]
    visatypeid <- map[SerializationKeys.visatypeid]
    photo <- map[SerializationKeys.photo]
    fullname <- map[SerializationKeys.fullname]
    userid <- map[SerializationKeys.userid]
    referalcode <- map[SerializationKeys.referalcode]
  }


}
