//
//  NotificationListResponse.swift
//  Template
//
//  Created by Akber Sayni on 07/11/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

struct NotificationListResponse : Codable {
    let id: String?
    let userId: String?
    let orderId: String?
    let title: String?
    let message: String?
    let comments: String?
    let guestName: String?
    let isSent: String?
    let createdAt: String?
    let updatedAt: String?
    let notificationType: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case userId = "user_id"
        case orderId = "reference_order_id"
        case title = "title"
        case message = "message"
        case comments = "comments"
        case isSent = "is_sent"
        case guestName = "guest_name"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case notificationType = "notification_type"
    }    
}

