//
//  DriverModel.swift
//
//  Created by Hamza Hasan on 26/08/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class DriverModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let state = "state"
    static let city = "city"
    static let emergencyContact = "emergency_contact"
    static let name = "name"
    static let joinning = "joinning"
    static let email = "email"
    static let licenceNo = "licence_no"
    static let address = "address"
    static let driversPhoto = "drivers_photo"
    static let licensePhoto = "license_photo"
    static let age = "age"
    static let licenseExpiry = "license_expiry"
    static let zipcode = "zipcode"
    static let assignedCar = "assigned_Car"
    static let nationality = "nationality"
    static let status = "status"
    static let homePhone = "home_phone"
    static let ikama = "ikama"
    static let id = "id"
    static let driverid = "driverid"
    static let kafilName = "kafil_name"
    static let workPhone = "work_phone"
    static let password = "password"
    static let registerationNumnber = "registeration_numnber"
    static let latitude = "latitude"
    static let longitude = "longitude"
    static let vendorId = "vendor_id"
    static let vendorName = "vendor_name"
  }

  // MARK: Properties
  @objc dynamic var state: String? = ""
  @objc dynamic var city: String? = ""
  @objc dynamic var emergencyContact: String? = ""
  @objc dynamic var name: String? = ""
  @objc dynamic var joinning: String? = ""
  @objc dynamic var email: String? = ""
  @objc dynamic var licenceNo: String? = ""
  @objc dynamic var address: String? = ""
  @objc dynamic var driversPhoto: String? = ""
  @objc dynamic var licensePhoto: String? = ""
  @objc dynamic var age: String? = ""
  @objc dynamic var licenseExpiry: String? = ""
  @objc dynamic var zipcode: String? = ""
  @objc dynamic var assignedCar: String? = ""
  @objc dynamic var nationality: String? = ""
  @objc dynamic var status: String? = ""
  @objc dynamic var homePhone: String? = ""
  @objc dynamic var ikama: String? = ""
  @objc dynamic var id: String? = ""
  @objc dynamic var driverid: String? = ""
  @objc dynamic var kafilName: String? = ""
  @objc dynamic var workPhone: String? = ""
  @objc dynamic var password: String? = ""
  @objc dynamic var registerationNumnber: String? = ""
  @objc dynamic var latitude: String? = ""
  @objc dynamic var longitude: String? = ""
  @objc dynamic var vendorId: String? = ""
  @objc dynamic var vendorName: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    state <- map[SerializationKeys.state]
    city <- map[SerializationKeys.city]
    emergencyContact <- map[SerializationKeys.emergencyContact]
    name <- map[SerializationKeys.name]
    joinning <- map[SerializationKeys.joinning]
    email <- map[SerializationKeys.email]
    licenceNo <- map[SerializationKeys.licenceNo]
    address <- map[SerializationKeys.address]
    driversPhoto <- map[SerializationKeys.driversPhoto]
    licensePhoto <- map[SerializationKeys.licensePhoto]
    age <- map[SerializationKeys.age]
    licenseExpiry <- map[SerializationKeys.licenseExpiry]
    zipcode <- map[SerializationKeys.zipcode]
    assignedCar <- map[SerializationKeys.assignedCar]
    nationality <- map[SerializationKeys.nationality]
    status <- map[SerializationKeys.status]
    homePhone <- map[SerializationKeys.homePhone]
    ikama <- map[SerializationKeys.ikama]
    id <- map[SerializationKeys.id]
    driverid <- map[SerializationKeys.driverid]
    kafilName <- map[SerializationKeys.kafilName]
    workPhone <- map[SerializationKeys.workPhone]
    password <- map[SerializationKeys.password]
    registerationNumnber <- map[SerializationKeys.registerationNumnber]
    latitude <- map[SerializationKeys.latitude]
    longitude <- map[SerializationKeys.longitude]
    vendorId <- map[SerializationKeys.vendorId]
    vendorName <- map[SerializationKeys.vendorName]
  }
}
