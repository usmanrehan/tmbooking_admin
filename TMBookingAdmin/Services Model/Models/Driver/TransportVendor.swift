//
//  TransportVendor.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 06/07/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class TransportVendor: NSObject, Mappable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String? = ""
    @objc dynamic var drivers: [DriverModel]?

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required public convenience init?(map : Map){
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        id <- map["vendor_id"]
        name <- map["vendor_name"]
        drivers <- map["drivers"]
    }
}
