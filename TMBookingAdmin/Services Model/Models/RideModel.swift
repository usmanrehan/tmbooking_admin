//
//  RideModel.swift
//
//  Created by Hamza Hasan on 28/08/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class RideModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let bookingStatus = "booking_status"
    static let carid = "carid"
    static let id = "id"
    static let pickup = "pickup"
    static let dropoff = "dropoff"
    static let carName = "car_name"
    static let pickuptime = "pickuptime"
    static let statusValue = "status_value"
    static let extrainfo = "extrainfo"
    static let orderId = "orderid"
    static let riderName = "rider_name"
  }

  // MARK: Properties
  @objc dynamic var bookingStatus: String? = ""
  @objc dynamic var carid: String? = ""
  @objc dynamic var id: String? = ""
  @objc dynamic var pickup: String? = ""
  @objc dynamic var dropoff: String? = ""
  @objc dynamic var carName: String? = ""
  @objc dynamic var pickuptime: String? = ""
  @objc dynamic var statusValue: String? = ""
  @objc dynamic var extrainfo: String? = ""
  @objc dynamic var orderId: String? = ""
  @objc dynamic var riderName: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    bookingStatus <- map[SerializationKeys.bookingStatus]
    carid <- map[SerializationKeys.carid]
    id <- map[SerializationKeys.id]
    pickup <- map[SerializationKeys.pickup]
    dropoff <- map[SerializationKeys.dropoff]
    carName <- map[SerializationKeys.carName]
    pickuptime <- map[SerializationKeys.pickuptime]
    statusValue <- map[SerializationKeys.statusValue]
    extrainfo <- map[SerializationKeys.extrainfo]
    orderId <- map[SerializationKeys.orderId]
    riderName <- map[SerializationKeys.riderName]
  }
}
