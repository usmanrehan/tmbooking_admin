//
//  AccountModel.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 17/06/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class AccountModel: NSObject, Mappable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        id <- map["account_id"]
        name <- map["account_name"]
    }
}
