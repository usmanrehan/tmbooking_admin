//
//  PurchaseOrderModel.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 17/06/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class PurchaseOrderModel: NSObject, Mappable {
    @objc dynamic var id: String? = ""
    @objc dynamic var orderId: String? = ""
    @objc dynamic var vendorId: String? = ""
    @objc dynamic var vendorName: String? = ""
    @objc dynamic var accountId: String? = ""
    @objc dynamic var accountName: String? = ""
    @objc dynamic var date: String? = ""
    @objc dynamic var checkinDate: String? = ""
    @objc dynamic var checkoutDate: String? = ""
	@objc dynamic var pickupDate: String? = ""
    @objc dynamic var bookingId: String? = ""
    @objc dynamic var confirmationNumber: String? = ""
    @objc dynamic var internalConfirmationNumber: String? = ""
    @objc dynamic var externalConfirmationNumber: String? = ""
    @objc dynamic var venderMessage: String? = ""
    @objc dynamic var memo: String? = ""
    @objc dynamic var purchasePrice: String? = ""
	@objc dynamic var vat: String? = ""
	@objc dynamic var munTax: String? = ""
    @objc dynamic var contactPersons: [VendorContactModel]?

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        id <- map["id"]
        orderId <- map["qb_po_id"]
        vendorId <- map["qb_vendor_id"]
        vendorName <- map["qb_vendor_name"]
        accountId <- map["qb_account_id"]
        accountName <- map["qb_account_name"]
        date <- map["qb_date"]
        checkinDate <- map["checkin_date"]
		pickupDate <- map["pickup_date"]
        checkoutDate <- map["checkout_date"]
        bookingId <- map["booking_id"]
        confirmationNumber <- map["confirmation_number"]
        internalConfirmationNumber <- map["internal_confirmation_number"]
        externalConfirmationNumber <- map["external_confirmation_number"]
        venderMessage <- map["qb_message_to_vendor"]
        memo <- map["qb_memo"]
        purchasePrice <- map["purchase_price"]
		vat <- map["vat"]
		munTax <- map["mun"]
        contactPersons <- map["vendor_contact_persons"]
    }
}
