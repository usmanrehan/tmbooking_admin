//
//  AssignedDriverModel.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 14/04/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class AssignedDriverModel: NSObject, Mappable {
    @objc dynamic var rideId: String? = ""
    @objc dynamic var status: String? = ""
    @objc dynamic var driverId: String? = ""
    @objc dynamic var driverName: String? = ""
    @objc dynamic var updatedAt: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        rideId <- map["rideid"]
        status <- map["status"]
        driverId <- map["driverid"]
        driverName <- map["name"]
        updatedAt <- map["updated_at"]
    }
}
