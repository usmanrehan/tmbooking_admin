//
//  VisaTypeModel.swift
//
//  Created by Hamza Hasan on 29/08/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class VisaTypeModelOld: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let visatype = "visatype"
    static let apply = "apply"
    static let arrival = "arrival"
    static let id = "id"
    static let image = "image"
    static let descriptionValue = "description"
    static let entries = "entries"
    static let title = "title"
    static let restriction = "restriction"
    static let duration = "duration"
    static let price = "price"
  }

  // MARK: Properties
  @objc dynamic var visatype: String? = ""
  @objc dynamic var apply: String? = ""
  @objc dynamic var arrival: String? = ""
  @objc dynamic var id: String? = ""
  @objc dynamic var image: String? = ""
  @objc dynamic var descriptionValue: String? = ""
  @objc dynamic var entries: String? = ""
  @objc dynamic var title: String? = ""
  @objc dynamic var restriction: String? = ""
  @objc dynamic var duration: String? = ""
  @objc dynamic var price: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    visatype <- map[SerializationKeys.visatype]
    apply <- map[SerializationKeys.apply]
    arrival <- map[SerializationKeys.arrival]
    id <- map[SerializationKeys.id]
    image <- map[SerializationKeys.image]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    entries <- map[SerializationKeys.entries]
    title <- map[SerializationKeys.title]
    restriction <- map[SerializationKeys.restriction]
    duration <- map[SerializationKeys.duration]
    price <- map[SerializationKeys.price]
  }


}
