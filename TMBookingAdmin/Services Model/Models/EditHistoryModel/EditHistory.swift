//
//  EditHistory.swift
//
//  Created by Hamza Hasan on 12/12/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class EditHistory: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let dateTime = "date_time"
    static let value = "new_value"
    static let user = "user"
    static let oldValue = "old_value"
  }

  // MARK: Properties
    @objc dynamic var dateTime: String? = ""
    @objc dynamic var value: String? = ""
    @objc dynamic var user: String? = ""
    @objc dynamic var oldValue: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "oldValue"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    dateTime <- map[SerializationKeys.dateTime]
    value <- map[SerializationKeys.value]
    user <- map[SerializationKeys.user]
    oldValue <- map[SerializationKeys.oldValue]
  }


}
