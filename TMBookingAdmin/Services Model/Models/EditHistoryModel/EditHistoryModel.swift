//
//  PassportNumber.swift
//
//  Created by Hamza Hasan on 12/12/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class EditHistoryModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let editHistory = "edit_history"
        static let currentValue = "current_value"
    }
    
    // MARK: Properties
    var editHistory = List<EditHistory>()
    @objc dynamic var currentValue: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "currentValue"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        editHistory <- (map[SerializationKeys.editHistory], ListTransform<EditHistory>())
        currentValue <- map[SerializationKeys.currentValue]
    }
    
    
}
