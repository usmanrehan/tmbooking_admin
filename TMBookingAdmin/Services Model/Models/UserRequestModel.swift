//
//  UserRequestModel.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 18/06/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit
import ObjectMapper

class UserRequestModel: NSObject, Mappable {
    // MARK: Properties
    @objc dynamic var id: Int = 0
    @objc dynamic var username: String? = ""
    @objc dynamic var firstName: String? = ""
    @objc dynamic var lastName: String? = ""
    @objc dynamic var email: String? = ""
    @objc dynamic var profileImage: String? = ""
    @objc dynamic var statusId: String? = ""
    @objc dynamic var statusName: String? = ""
    @objc dynamic var roleIdentifier: String? = ""
    @objc dynamic var roleName: String? = ""
    @objc dynamic var subUsers: [UserRequestModel]?
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required public convenience init?(map : Map){
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        id <- map["id"]
        username <- map["username"]
        firstName <- map["firstname"]
        lastName <- map["lastname"]
        email <- map["email"]
        profileImage <- map["profile_image"]
        statusId <- map["status_id"]
        statusName <- map["status_name"]
        roleIdentifier <- map["role_indentifier"]
        roleName <- map["role_name"]
        subUsers <- map["subusers"]
    }
}
