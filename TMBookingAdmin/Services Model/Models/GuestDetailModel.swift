//
//  GuestDetailModel.swift
//
//  Created by Hamza Hasan on 07/12/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class GuestDetailModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let name = "name"
    static let orderId = "order_id"
    static let passportExpiry = "passport_expiry"
    static let email = "email"
    static let passportNumber = "passport_number"
    static let guestId = "guest_id"
    static let localContactNumber = "local_contact_number"
    static let visaNumber = "visa_number"
    static let visaSponser = "visa_sponser"
    static let saudiContactNumber = "saudi_contact_number"
    static let dateOfBirth = "date_of_birth"
  }

  // MARK: Properties
  @objc dynamic var name: String? = ""
  @objc dynamic var orderId: String? = ""
  @objc dynamic var passportExpiry: String? = ""
  @objc dynamic var email: String? = ""
  @objc dynamic var passportNumber: String? = ""
  @objc dynamic var guestId: String? = ""
  @objc dynamic var localContactNumber: String? = ""
  @objc dynamic var visaNumber: String? = ""
  @objc dynamic var visaSponser: String? = ""
  @objc dynamic var saudiContactNumber: String? = ""
  @objc dynamic var dateOfBirth: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "guestId"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    name <- map[SerializationKeys.name]
    orderId <- map[SerializationKeys.orderId]
    passportExpiry <- map[SerializationKeys.passportExpiry]
    email <- map[SerializationKeys.email]
    passportNumber <- map[SerializationKeys.passportNumber]
    guestId <- map[SerializationKeys.guestId]
    localContactNumber <- map[SerializationKeys.localContactNumber]
    visaNumber <- map[SerializationKeys.visaNumber]
    visaSponser <- map[SerializationKeys.visaSponser]
    saudiContactNumber <- map[SerializationKeys.saudiContactNumber]
    dateOfBirth <- map[SerializationKeys.dateOfBirth]
  }


}
