//
//  All.swift
//
//  Created by Hamza Hasan on 08/12/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class BookingsCountModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let selectedData = "selectedData"
        static let count = "count"
    }
    
    // MARK: Properties
    @objc dynamic var selectedData: String? = ""
    @objc dynamic var count = 0
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "count"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        selectedData <- map[SerializationKeys.selectedData]
        count <- map[SerializationKeys.count]
    }
    
    
}
