//
//  MeetGreetModel.swift
//
//  Created by Hamza Hasan on 07/12/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class MeetGreetModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let bookingId = "booking_id"
    static let meetId = "meet_id"
    static let isMandoobMeet = "is_mandoob_meet"
    static let meetComments = "meet_comments"
    static let meetPicture = "meet_picture"
    static let meetDateTime = "meet_date_time"
  }

  // MARK: Properties
  @objc dynamic var bookingId: String? = ""
  @objc dynamic var meetId: String? = ""
  @objc dynamic var isMandoobMeet: String? = ""
  @objc dynamic var meetComments: String? = ""
  var meetPicture = List<String>()
  @objc dynamic var meetDateTime: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "bookingId"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    bookingId <- map[SerializationKeys.bookingId]
    meetId <- map[SerializationKeys.meetId]
    isMandoobMeet <- map[SerializationKeys.isMandoobMeet]
    meetComments <- map[SerializationKeys.meetComments]
    meetPicture <- map[SerializationKeys.meetPicture]
    meetDateTime <- map[SerializationKeys.meetDateTime]
  }


}
