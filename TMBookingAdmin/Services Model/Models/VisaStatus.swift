//
//  SingleVisaModel.swift
//
//  Created by Hamza Hasan on 06/01/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class VisaStatus: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "id"
        static let status = "status"
        static let reason = "reason"
        static let image = "image"
        static let okToBoardImage = "okb_image"
    }
    
    // MARK: Properties
    @objc dynamic var id: String? = ""
    @objc dynamic var status: String? = ""
    @objc dynamic var reason: String? = ""
    @objc dynamic var image: String? = ""
    @objc dynamic var okToBoardImage: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required public convenience init?(map : Map){
        self.init()
    }
    
    override class public func primaryKey() -> String? {
        return "id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        id <- map[SerializationKeys.id]
        status <- map[SerializationKeys.status]
        reason <- map[SerializationKeys.reason]
        image <- map[SerializationKeys.image]
        okToBoardImage <- map[SerializationKeys.okToBoardImage]
    }
}
