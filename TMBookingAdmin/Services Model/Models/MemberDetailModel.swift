//
//  MemberDetailModel.swift
//
//  Created by Hamza Hasan on 20/01/2019
//  Copyright (c) . All rights reserved.
//


import ObjectMapper

public class MemberDetailModel: NSObject, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let dob = "dob"
        static let countryofbirth = "countryofbirth"
        static let resident = "resident"
        static let aqama = "aqama"
        static let id = "id"
        static let visatypeid = "visatypeid"
        static let passport = "passport"
        static let photo = "photo"
        static let fullname = "fullname"
        static let passportexpiry = "passportexpiry"
        static let userid = "userid"
        static let education = "education"
        static let healthstatus = "healthstatus"
        static let formid = "formid"
        static let email = "email"
        static let mobile = "mobile"
        static let date = "date"
        static let gender = "gender"
        static let sirname = "sirname"
        static let martialstatus = "martialstatus"
        static let passportdateofissue = "passportdateofissue"
        static let issuecity = "issuecity"
        static let occupation = "occupation"
        static let socialsecuritynumber = "socialsecuritynumber"
        static let cnic = "cnic"
        static let issuecountry = "issuecountry"
        static let referalcode = "referalcode"
        static let nationality = "nationality"
        static let livingin = "livingin"
        static let cnicphoto = "cnicphoto"
        static let passportNo = "passport_no"
    }
    
    // MARK: Properties
    @objc dynamic var dob: String? = ""
    @objc dynamic var countryofbirth: String? = ""
    @objc dynamic var resident: String? = ""
    @objc dynamic var aqama: String? = ""
    @objc dynamic var id: String? = ""
    @objc dynamic var visatypeid: String? = ""
    @objc dynamic var photo: String? = ""
    @objc dynamic var fullname: String? = ""
    @objc dynamic var passportexpiry: String? = ""
    @objc dynamic var userid: String? = ""
    @objc dynamic var education: String? = ""
    @objc dynamic var healthstatus: String? = ""
    @objc dynamic var formid: String? = ""
    @objc dynamic var email: String? = ""
    @objc dynamic var mobile: String? = ""
    @objc dynamic var date: String? = ""
    @objc dynamic var gender: String? = ""
    @objc dynamic var sirname: String? = ""
    @objc dynamic var martialstatus: String? = ""
    @objc dynamic var passportdateofissue: String? = ""
    @objc dynamic var issuecity: String? = ""
    @objc dynamic var occupation: String? = ""
    @objc dynamic var socialsecuritynumber: String? = ""
    @objc dynamic var cnic: String? = ""
    @objc dynamic var issuecountry: String? = ""
    @objc dynamic var referalcode: String? = ""
    @objc dynamic var nationality: String? = ""
    @objc dynamic var livingin: String? = ""
    @objc dynamic var cnicphoto: String? = ""
    @objc dynamic var passport: [String]?
    @objc dynamic var passportNo: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map) {
        self.init()
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        dob <- map[SerializationKeys.dob]
        countryofbirth <- map[SerializationKeys.countryofbirth]
        resident <- map[SerializationKeys.resident]
        aqama <- map[SerializationKeys.aqama]
        id <- map[SerializationKeys.id]
        visatypeid <- map[SerializationKeys.visatypeid]
        passport <- map[SerializationKeys.passport]
        photo <- map[SerializationKeys.photo]
        fullname <- map[SerializationKeys.fullname]
        passportexpiry <- map[SerializationKeys.passportexpiry]
        userid <- map[SerializationKeys.userid]
        education <- map[SerializationKeys.education]
        healthstatus <- map[SerializationKeys.healthstatus]
        formid <- map[SerializationKeys.formid]
        email <- map[SerializationKeys.email]
        mobile <- map[SerializationKeys.mobile]
        date <- map[SerializationKeys.date]
        gender <- map[SerializationKeys.gender]
        sirname <- map[SerializationKeys.sirname]
        martialstatus <- map[SerializationKeys.martialstatus]
        passportdateofissue <- map[SerializationKeys.passportdateofissue]
        issuecity <- map[SerializationKeys.issuecity]
        occupation <- map[SerializationKeys.occupation]
        socialsecuritynumber <- map[SerializationKeys.socialsecuritynumber]
        cnic <- map[SerializationKeys.cnic]
        issuecountry <- map[SerializationKeys.issuecountry]
        referalcode <- map[SerializationKeys.referalcode]
        nationality <- map[SerializationKeys.nationality]
        livingin <- map[SerializationKeys.livingin]
        cnicphoto <- map[SerializationKeys.cnicphoto]
        passportNo <- map[SerializationKeys.passportNo]
    }
}
