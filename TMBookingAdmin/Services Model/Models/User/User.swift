//
//  User.swift
//
//  Created by Hamza Hasan on 16/12/2018
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class User: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let canUseStorecredit = "can_use_storecredit"
    static let userRole = "_user_role"
    static let links = "_links"
    static let avatarUrls = "avatar_urls"
    static let registeredDate = "registered_date"
    static let billingAddress1 = "billing_address_1"
    static let nickname = "nickname"
    static let userNationality = "user_nationality"
    static let billingAddress2 = "billing_address_2"
    static let refrence2ContactPerson = "refrence_2_contact_person"
    static let parentUser = "parent_user"
    static let entity = "entity"
    static let billingFirstName = "billing_first_name"
    static let lastName = "last_name"
    static let id = "id"
    static let agentCompanyName = "agent_company_name"
    static let url = "url"
    static let refrence2Address = "refrence_2_address"
    static let refrence1ContactPerson = "refrence_1_contact_person"
    static let descriptionValue = "description"
    static let officeAddress = "office_address"
    static let username = "username"
    static let locale = "locale"
    static let officeNumber = "office_number"
    static let extraCapabilities = "extra_capabilities"
    static let billingPhone = "billing_phone"
    static let firstName = "first_name"
    static let billingCompany = "billing_company"
    static let iataNumber = "iata_number"
    static let refrence1CompanyName = "refrence_1_company_name"
    static let visitingCard = "visiting_card"
    static let billingEmail = "billing_email"
    static let meta = "meta"
    static let agentBankTitle = "agent_bank_title"
    static let capabilities = "capabilities"
    static let dtsNumber = "dts_number"
    static let billingState = "billing_state"
    static let agentBankName = "agent_bank_name"
    static let slug = "slug"
    static let authorizedAgent = "authorized_agent"
    static let agentBankIban = "agent_bank_iban"
    static let refrence2CompanyName = "refrence_2_company_name"
    static let agentFirstName = "agent_first_name"
    static let authorisedAccountantMobileNumber = "authorised_accountant_mobile_number"
    static let billingPostcode = "billing_postcode"
    static let name = "name"
    static let authorisedAccountantName = "authorised_accountant_name"
    static let email = "email"
    static let refrence1Address = "refrence_1_address"
    static let billingCity = "billing_city"
    static let authorisedAccountantDesignation = "authorised_accountant_designation"
    static let agentBankBranchName = "agent_bank_branch_name"
    static let billingCountry = "billing_country"
    static let billingLastName = "billing_last_name"
    static let authorisedAccountantEmail = "authorised_accountant_email"
    static let agentBankAccountNumber = "agent_bank_account_number"
    static let link = "link"
    static let transportationSales = "transportation_sales"
    static let featuredImage = "_featured_image"
    static let hotelSales = "hotel_sales"
    static let roles = "roles"
    static let officeMobile = "office_mobile"
    static let agentLastName = "agent_last_name"
    static let officeFax = "office_fax"
  }

  // MARK: Properties
  @objc dynamic var canUseStorecredit: String? = ""
  @objc dynamic var userRole: String? = ""
  @objc dynamic var registeredDate: String? = ""
  @objc dynamic var billingAddress1: String? = ""
  @objc dynamic var nickname: String? = ""
  @objc dynamic var userNationality: String? = ""
  @objc dynamic var billingAddress2: String? = ""
  @objc dynamic var refrence2ContactPerson: String? = ""
  @objc dynamic var parentUser: String? = ""
  @objc dynamic var entity: String? = ""
  @objc dynamic var billingFirstName: String? = ""
  @objc dynamic var lastName: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var agentCompanyName: String? = ""
  @objc dynamic var url: String? = ""
  @objc dynamic var refrence2Address: String? = ""
  @objc dynamic var refrence1ContactPerson: String? = ""
  @objc dynamic var descriptionValue: String? = ""
  @objc dynamic var officeAddress: String? = ""
  @objc dynamic var username: String? = ""
  @objc dynamic var locale: String? = ""
  @objc dynamic var officeNumber: String? = ""
  @objc dynamic var billingPhone: String? = ""
  @objc dynamic var firstName: String? = ""
  @objc dynamic var billingCompany: String? = ""
  @objc dynamic var iataNumber: String? = ""
  @objc dynamic var refrence1CompanyName: String? = ""
  @objc dynamic var visitingCard: String? = ""
  @objc dynamic var billingEmail: String? = ""
  @objc dynamic var agentBankTitle: String? = ""
  @objc dynamic var dtsNumber: String? = ""
  @objc dynamic var billingState: String? = ""
  @objc dynamic var agentBankName: String? = ""
  @objc dynamic var slug: String? = ""
  @objc dynamic var authorizedAgent: String? = ""
  @objc dynamic var agentBankIban: String? = ""
  @objc dynamic var refrence2CompanyName: String? = ""
  @objc dynamic var agentFirstName: String? = ""
  @objc dynamic var authorisedAccountantMobileNumber: String? = ""
  @objc dynamic var billingPostcode: String? = ""
  @objc dynamic var name: String? = ""
  @objc dynamic var authorisedAccountantName: String? = ""
  @objc dynamic var email: String? = ""
  @objc dynamic var refrence1Address: String? = ""
  @objc dynamic var billingCity: String? = ""
  @objc dynamic var authorisedAccountantDesignation: String? = ""
  @objc dynamic var agentBankBranchName: String? = ""
  @objc dynamic var billingCountry: String? = ""
  @objc dynamic var billingLastName: String? = ""
  @objc dynamic var authorisedAccountantEmail: String? = ""
  @objc dynamic var agentBankAccountNumber: String? = ""
  @objc dynamic var link: String? = ""
  @objc dynamic var transportationSales: String? = ""
  @objc dynamic var featuredImage: String? = ""
  @objc dynamic var hotelSales: String? = ""
  var roles = List<String>()
  @objc dynamic var officeMobile: String? = ""
  @objc dynamic var agentLastName: String? = ""
  @objc dynamic var officeFax: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    canUseStorecredit <- map[SerializationKeys.canUseStorecredit]
    userRole <- map[SerializationKeys.userRole]
    registeredDate <- map[SerializationKeys.registeredDate]
    billingAddress1 <- map[SerializationKeys.billingAddress1]
    nickname <- map[SerializationKeys.nickname]
    userNationality <- map[SerializationKeys.userNationality]
    billingAddress2 <- map[SerializationKeys.billingAddress2]
    refrence2ContactPerson <- map[SerializationKeys.refrence2ContactPerson]
    parentUser <- map[SerializationKeys.parentUser]
    entity <- map[SerializationKeys.entity]
    billingFirstName <- map[SerializationKeys.billingFirstName]
    lastName <- map[SerializationKeys.lastName]
    id <- map[SerializationKeys.id]
    agentCompanyName <- map[SerializationKeys.agentCompanyName]
    url <- map[SerializationKeys.url]
    refrence2Address <- map[SerializationKeys.refrence2Address]
    refrence1ContactPerson <- map[SerializationKeys.refrence1ContactPerson]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    officeAddress <- map[SerializationKeys.officeAddress]
    username <- map[SerializationKeys.username]
    locale <- map[SerializationKeys.locale]
    officeNumber <- map[SerializationKeys.officeNumber]
    billingPhone <- map[SerializationKeys.billingPhone]
    firstName <- map[SerializationKeys.firstName]
    billingCompany <- map[SerializationKeys.billingCompany]
    iataNumber <- map[SerializationKeys.iataNumber]
    refrence1CompanyName <- map[SerializationKeys.refrence1CompanyName]
    visitingCard <- map[SerializationKeys.visitingCard]
    billingEmail <- map[SerializationKeys.billingEmail]
    agentBankTitle <- map[SerializationKeys.agentBankTitle]
    dtsNumber <- map[SerializationKeys.dtsNumber]
    billingState <- map[SerializationKeys.billingState]
    agentBankName <- map[SerializationKeys.agentBankName]
    slug <- map[SerializationKeys.slug]
    authorizedAgent <- map[SerializationKeys.authorizedAgent]
    agentBankIban <- map[SerializationKeys.agentBankIban]
    refrence2CompanyName <- map[SerializationKeys.refrence2CompanyName]
    agentFirstName <- map[SerializationKeys.agentFirstName]
    authorisedAccountantMobileNumber <- map[SerializationKeys.authorisedAccountantMobileNumber]
    billingPostcode <- map[SerializationKeys.billingPostcode]
    name <- map[SerializationKeys.name]
    authorisedAccountantName <- map[SerializationKeys.authorisedAccountantName]
    email <- map[SerializationKeys.email]
    refrence1Address <- map[SerializationKeys.refrence1Address]
    billingCity <- map[SerializationKeys.billingCity]
    authorisedAccountantDesignation <- map[SerializationKeys.authorisedAccountantDesignation]
    agentBankBranchName <- map[SerializationKeys.agentBankBranchName]
    billingCountry <- map[SerializationKeys.billingCountry]
    billingLastName <- map[SerializationKeys.billingLastName]
    authorisedAccountantEmail <- map[SerializationKeys.authorisedAccountantEmail]
    agentBankAccountNumber <- map[SerializationKeys.agentBankAccountNumber]
    link <- map[SerializationKeys.link]
    transportationSales <- map[SerializationKeys.transportationSales]
    featuredImage <- map[SerializationKeys.featuredImage]
    hotelSales <- map[SerializationKeys.hotelSales]
    roles <- map[SerializationKeys.roles]
    officeMobile <- map[SerializationKeys.officeMobile]
    agentLastName <- map[SerializationKeys.agentLastName]
    officeFax <- map[SerializationKeys.officeFax]
  }


}
