//
//  UserAPIManager.swift
//  The Court Lawyer
//
//  Created by Ahmed Shahid on 5/3/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class UsersAPIManager: APIManagerBase {
    //MARK:- GET Drivers List
    func getDriverList(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route:Route.DriverList.rawValue, params: params)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Update driver
    func UpdateDriver(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.UpdateDriver.rawValue)! as URL
        print(route)
        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    //MARK:- Driver Registration
    func DriverRegistration(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.DriverRegistration.rawValue)! as URL
        print(route)
        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    //MARK:- Remove Driver
    func RemoveDriver(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.RemoveDriver.rawValue)! as URL
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Booking Rides
    func getBookingRides(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.GetBookingRides.rawValue)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Pending Rides
    func PendingRides(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.PendingRides.rawValue)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Confirmation Booking
    func ConfirmationBooking(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.ConfirmationBooking.rawValue)! as URL
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- UnbookDriver
    func UnbookDriver(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.UnbookDriver.rawValue)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Assign Driver
    func AssignDriver(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.AssignDriver.rawValue)! as URL
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Active Rides
    func ActiveRides(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.ActiveRides.rawValue)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Cancel Rides List
    func CancelRidesList(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.CancelRidesList.rawValue)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Cancel Booking
    func CancelBooking(params:Parameters, success: @escaping DefaultIntResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.CancelBooking.rawValue)! as URL
        print(route)
        self.postRequestForIntWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Ride assigned drivers
    func GetAssignedDrivers(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.AssignedDrivers.rawValue)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }

    //MARK:- Booking History
    func BookingHistory(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.BookingHistory.rawValue)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Visa Application List
    func VisaApplicationList(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.VisaApplicationList.rawValue)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }

    // MARK: - GetVisaCoutriesList
    func getVisaCountriesList(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.VisaCountryList.rawValue)! as URL
        self.getRequestForArray(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    
    // MARK: - Add Visa Country
    func addVisaCountry(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.AddVisaCountry.rawValue)! as URL
        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }

    // MARK: - Update Visa Country
    func updateVisaCountry(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.UpdateCountry.rawValue)! as URL
        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }

    // MARK: - Remove Visa Country
    func removeVisaCountry(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.RemoveVisaCountry.rawValue)! as URL
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }

    // MARK: - Remove Visa Type
    func removeVisaType(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Route.RemoveVisaType.rawValue)! as URL
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }

    //MARK:- Visa Type List
    func VisaTypeList(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.VisaTypeList.rawValue)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Remove Visa Type
    func RemoveVisaType(params:Parameters, success: @escaping DefaultBoolResultAPISuccesClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.RemoveVisaType.rawValue)! as URL
        print(route)
        self.postRequestForBoolWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Update Visa Type
    func UpdateVisaType(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.UpdateVisaType.rawValue)! as URL
        print(route)
        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    //MARK:- Add Visa Type
    func AddVisaType(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.AddVisaType.rawValue)! as URL
        print(route)
        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    //MARK:- Block Unblock Driver
    func Block_UnblockDriver(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.Block_UnblockDriver.rawValue)! as URL
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- UpdateVisaForm
    func UpdateVisaForm(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.UpdateVisaForm.rawValue)! as URL
        print(route)
        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    //MARK:- UpdateParentVisaStatus
    func UpdateParentVisaStatus(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.UpdateParentVisaStatus.rawValue)! as URL
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- UpdateMemberVisaStatus
    func UpdateMemberVisaStatus(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.UpdateMemberVisaStatus.rawValue)! as URL
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Get Members
    func GetMembers(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.GetMembers.rawValue)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- list_bookings
    func GetListBookings(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route : URL = WPURLforRoute(route:Route.ListBookings.rawValue, params: params)! as URL
        self.getWPResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- DeliverGift
    func DeliverGift(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route : URL = WPURLforRoute(route:Route.DeliverGift.rawValue, params: params)! as URL
        self.postWPRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- CheckIn
    func CheckIn(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route : URL = WPURLforRoute(route:Route.CheckIn.rawValue, params: params)! as URL
        self.postWPRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- CheckOut
    func CheckOut(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route : URL = WPURLforRoute(route:Route.CheckOut.rawValue, params: params)! as URL
        self.postWPRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- list_bookings
    func GetAllGuests(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route : URL = WPURLforRoute(route:Route.AllGuests.rawValue, params: params)! as URL
        self.getWPArrayResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- UpdateGuest
    func UpdateGuest(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route : URL = WPURLforRoute(route:Route.UpdateGuest.rawValue, params: params)! as URL
        self.postWPRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- MeetGreet
    func MeetGreet(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route : URL = WPURLforRoute(route:Route.MeetGreet.rawValue, params: params)! as URL
        self.getWPResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- MeetGreet
    func PostMeetGreet(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route : URL = WPPOSTURLforRoute(route: Route.MeetGreet.rawValue)! as URL
        self.postWPRequestWithMultipart(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    //MARK:- MeetGreet
    func ListBookingsCount(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route : URL = WPURLforRoute(route:Route.ListBookingsCount.rawValue, params: params)! as URL
        self.getWPResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- GetGiftsAndAssignee
    func GetGiftsAndAssignee(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route : URL = WPURLforRoute(route:Route.GiftsAndAssignee.rawValue, params: params)! as URL
        self.getWPResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- UpdateGuest
    func AddDelivery(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route : URL = WPURLforRoute(route:Route.AddDelivery.rawValue, params: params)! as URL
        self.postWPRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- GetGiftsAndAssignee
    func GetEditHistory(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure){
        let route : URL = WPURLforRoute(route:Route.EditHistory.rawValue, params: params)! as URL
        self.getWPResponseWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- GetUserNotifications
    func getUserNotifications(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = WPURLforRoute(route: Route.getUserNotifications.rawValue, params: params)! as URL
        self.getWPAnyResponseWith(route: route, success: success, failure: failure)
    }
    //MARK:- GetAllGroup
    func getAllGroup(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route:Route.GetAllGroup.rawValue, params: params)! as URL
        print(route)
        self.getRequestForArray(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    //MARK:- GetAllVisas
    func getAllVisas(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route:Route.GetAllVisas.rawValue, params: params)! as URL
        print(route)
        self.getRequestForArray(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    // MARK: - Get all Saudi Visas
    func getAllSaudiVisas(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route:Route.GetAllSaudiVisas.rawValue, params: params)! as URL
        print(route)
        self.getRequestForArray(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    //MARK:- GetListSingleVisa
    func getListSingleVisa(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route:Route.ListSingleVisa.rawValue, params: params)! as URL
        print(route)
        self.getRequestForArray(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    //MARK:- GetGroupMembers
    func getGroupMembers(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route:Route.GetGroupMembers.rawValue, params: params)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- Get Member
    func GetMember(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.GetMember.rawValue)! as URL
        print(route)
        self.postRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- GetListSingleVisa
    func getStatus(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route:Route.GetStatus.rawValue, params: params)! as URL
        print(route)
        self.getRequestForArray(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    //MARK:- UpdateVisaStatus
    func UpdateVisaStatus(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.UpdateVisaStatus.rawValue)! as URL
        print(route)
        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    // MARK: - Update Saudi Visa Status
    func UpdateSaudiVisaStatus(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = URLforRoute(route: Route.UpdateSaudiVisaStatus.rawValue, params: params)! as URL
        print(route)
        self.getRequestForDictionaryWith(route: route, success: success, failure: failure, withHeader: false)
    }
    //MARK:- UpdateVisaStatus
    func UpdateVisaMemberDetail(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.UpdateMember.rawValue)! as URL
        print(route)
        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    //MARK:- GetVisatStatus
    func GetVisatStatus(params:Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = POSTURLforRoute(route: Route.GetVisatStatus.rawValue)! as URL
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    
    
    // MARK: - OkToBoard Visa APIs
    
    func getOkToBoardVisas(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route:Route.GetOkToBoardVisas.rawValue, params: params)! as URL
        self.getRequestForArray(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }

    func getOkToBoardStamps(params:Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route:Route.GetOkToBoardStamps.rawValue, params: params)! as URL
        self.getRequestForArray(route: route, parameters: params, success: success, failure: failure, withHeader: false)
    }
    
    func approveOkToBoardVisa(params: Parameters, queryParams: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route : URL = URLforRoute(route:Route.OkToBoardVisaApproval.rawValue, params: queryParams)! as URL
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
}
