//
//  APIManagerServicesWordPress.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 08/12/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import Alamofire

class APIManagerServicesWordPress: APIManagerWordPressBase {
    
    /// Default Failure Handler
    ///
    /// - Parameter error: NSError
    func apiFailureHandler(error: NSError){
        Utility.hideLoader()
        if let responseError = error.userInfo["NSLocalizedFailureReason"] as? String {
            Utility.showAlert(title: "Alert", message: responseError.html2String)
        } else {
            Utility.showAlert(title: "Alert", message: error.localizedDescription.html2String)
        }
    }

    /// AUTH TOKEN
    ///
    /// - Parameters:
    ///   - email: String
    ///   - pwd: String
    ///   - success: Closure
    func authToken(email: String, pwd: String, success :@escaping DefaultAPIAnyObject) {
        Utility.showLoader()
        let parameters: Parameters = ["username": email, "password": pwd]
        let route: URL = POSTURLfor(route: Route.auth.rawValue)!
        self.postRequestObjectWithToken(route: route, parameters: parameters, needToken: false, success: success, failure: {
            (errorResponse) in
            self.apiFailureHandler(error: errorResponse)
        })
    }

    func signInWith(success :@escaping DefaultAPIAnyObject) {
        Utility.showLoader()
        let route: URL = POSTURLfor(route: Route.login.rawValue)!
        self.postRequestObjectWithToken(route: route, parameters: [:], needToken: true, success: success, failure: {
            (errorResponse) in
            self.apiFailureHandler(error: errorResponse)
        })
    }
    
    func signOutWith(parameters: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Route.Logout.rawValue, params: parameters)!
        self.getRequestObjectWith(route: route, success: success, failure: failure)
    }

    func sendVisaEmail(parameters: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLfor(route: Route.SendVisaEmail.rawValue)!
        self.postRequestObjectWithToken(route: route, parameters: parameters, needToken: false, success: success, failure: failure)
    }
    
    func getVisaFields(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Route.getVisaFields.rawValue, params: params)!
        self.getRequestObjectWith(route: route, success: success, failure: failure)
    }
    
    func getPurchaseOrders(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Route.getPurchaseOrders.rawValue, params: params)!
        self.getRequestObjectWith(route: route, success: success, failure: failure)
    }

	func getTransportPurchaseOrders(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
		let route: URL = GETURLfor(route: Route.getTransportPurchaseOrders.rawValue, params: params)!
		self.getRequestObjectWith(route: route, success: success, failure: failure)
	}

    func getPurchaseOrderFields(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Route.getPurchaseOrderFields.rawValue, params: params)!
        self.getRequestObjectWith(route: route, success: success, failure: failure)
    }

	func getTransportPurchaseOrderFields(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
		let route: URL = GETURLfor(route: Route.getTransportPurchaseOrderFields.rawValue, params: params)!
		self.getRequestObjectWith(route: route, success: success, failure: failure)
	}

    func addPurchaseOrder(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLfor(route: Route.addPurchaseOrders.rawValue)!
        self.postRequestObjectWithToken(route: route, parameters: params, needToken: false, success: success, failure: failure)
    }
	
	func addTransportPurchaseOrder(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
		let route: URL = POSTURLfor(route: Route.addTransportPurchaseOrders.rawValue)!
		self.postRequestObjectWithToken(route: route, parameters: params, needToken: false, success: success, failure: failure)
	}
	
    func getOkToBoardStamp(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Route.getOkToBoardStamp.rawValue, params: params)!
        self.getRequestObjectWith(route: route, success: success, failure: failure)
    }

    func getPendingUsers(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Route.getPendingUsers.rawValue, params: params)!
        self.getRequestObjectWith(route: route, success: success, failure: failure)
    }

    func updateUserStatus(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Route.updateUserStatus.rawValue, params: params)!
        self.getRequestObjectWith(route: route, success: success, failure: failure)
    }
    
    func getTransportVendors(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Route.GetTransportVendors.rawValue)!
        self.getRequestObjectWith(route: route, success: success, failure: failure)
    }
    
    func getAllOrderStatus(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Route.getAllOrderStatus.rawValue)!
        self.getRequestObjectWith(route: route, success: success, failure: failure)
    }
    
    func updateOrderStatus(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Route.updateOrderStatus.rawValue, params: params)!
        self.postRequestObjectWithToken(route: route, parameters: [:], needToken: false, success: success, failure: failure)
    }
    
    func getOrderPaymentSlips(params: Parameters, success: @escaping DefaultAPIAnyObject, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Route.getOrderPaymentSlips.rawValue, params: params)!
        self.getRequestObjectWith(route: route, success: success, failure: failure)
    }
}
