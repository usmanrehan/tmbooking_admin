//
//  AppDelegate.swift
//  AdminTMBooking
//
//  Created by Akber Sayni on 30/07/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit
import LGSideMenuController
import GoogleMaps
import GooglePlacePicker
import IQKeyboardManagerSwift
import RealmSwift
import SwiftyUserDefaults
import UserNotifications
import Firebase
import Fabric
import Crashlytics
import ObjectMapper

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let apiKey = "AIzaSyD11DleGwZcdHl5sb29YNOFbaVCEf8UGq4"
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GMSServices.provideAPIKey(apiKey)
        GMSPlacesClient.provideAPIKey(apiKey)
        IQKeyboardManager.shared.enable = true
        
        //MARK: - FCM Integration
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        Messaging.messaging().delegate = self

        // For iOS 10 display notification (sent via APNS)
        UNUserNotificationCenter.current().delegate = self
        
        // Configure Fabric crashlytics
        Fabric.with([Crashlytics.self])
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        
        application.registerForRemoteNotifications()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }

    // [END receive_message]
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        Messaging.messaging().apnsToken = deviceToken
        print("APNs token retrieved: \(deviceToken)")
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
}

//MARK: - Public Methods
extension AppDelegate {
    public func changeRootViewController(updateDeviceTokenToServer: Bool = false) {
		if let loginData = Defaults[.loginData]?.toString() {
			let user = Mapper<User>().map(JSONObject: Utility.convertToDictionary(text: loginData))
			if let role = user?.userRole, role == "administrator" {
				self.loadHomeViewController()
			} else {
				self.loadBookingCalendar()
			}
			
			// Update device token
			if updateDeviceTokenToServer {
				let deviceToken = Defaults[.deviceToken]
				let userId = Defaults[.loginUserId]
				if !deviceToken.isEmpty, !userId.isEmpty {
					self.registerDeviceToken()
				}
			}
		} else {
			self.loadLoginViewController()
		}
    }
	
	public func setRootBookingCalendarController(updateDeviceTokenToServer: Bool = false) {
		if Defaults[.loginStatus] {
			self.loadBookingCalendar()
			if updateDeviceTokenToServer {
				let deviceToken = Defaults[.deviceToken]
				let userId = Defaults[.loginUserId]
				if !deviceToken.isEmpty, !userId.isEmpty {
					self.registerDeviceToken()
				}
			}
		} else {
			self.loadLoginViewController()
		}
	}
    
    public func loadLoginViewController() {
        let storyboard = AppStoryboard.Main.instance
        let controller = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
        let navController = BaseNavigationController(rootViewController: controller)
        if let window = self.window {
            window.rootViewController = nil
            UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromLeft, animations: {
                window.rootViewController = navController
            }, completion: nil)
        }
    }
    
    public func loadHomeViewController() {
        let storyboard = AppStoryboard.Main.instance
        let leftMenuController = storyboard.instantiateViewController(withIdentifier: "LeftSideMenuViewController")
        let controller = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
        let navController = BaseNavigationController(rootViewController: controller)
		
        let sideMenuController = LGSideMenuController(rootViewController: navController, leftViewController: leftMenuController, rightViewController: nil)
		
        if let window = self.window {
            window.rootViewController = nil
            UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromLeft, animations: {
                window.rootViewController = sideMenuController
            }, completion: nil)
        }
    }
	
	public func loadBookingCalendar() {
		let leftMenuController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: "LeftSideMenuViewController")
		let controller = AppStoryboard.Booking.instance.instantiateViewController(withIdentifier: "BookingsCalendar")
		let navigationController = BaseNavigationController(rootViewController: controller)
		
		let sideMenuController = LGSideMenuController(rootViewController: navigationController, leftViewController: leftMenuController, rightViewController: nil)
		
		if let window = self.window {
			window.rootViewController = nil
			UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromLeft, animations: {
				window.rootViewController = sideMenuController
			}, completion: nil)
		}
	}
}

//MARK:- Realm Migration
extension AppDelegate{
    func processRealmMigration() {
        var config = Realm.Configuration()
        config.deleteRealmIfMigrationNeeded = true
        Realm.Configuration.defaultConfiguration = config
    }
}

//MARK:- Web API Methods
extension AppDelegate {
    private func registerDeviceToken(_ token: String = Defaults[.deviceToken], userId: String = Defaults[.loginUserId]) {
        if !Defaults[.loginStatus], userId.isEmpty {
            return//user not logged in
        }
        
        let route = APIManager.sharedInstance.usersAPIManager.POSTURLforRoute(route: Route.AddToken.rawValue)!
        let params: [String: Any] = [
            "userid": userId,
            "token": token,
            "user_type": "admin"
        ]
        APIManager.sharedInstance.usersAPIManager.postRequestWith(route: route, parameters: params, success: { (response) in
            print("Device token registered successfully")
        }, failure: { (error) in
            print("Failed to register device token: \(error.localizedDescription)")
        }, withHeaders: false)
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
}

// [END ios_10_message_handling]
extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Defaults[.deviceToken] = fcmToken
        self.registerDeviceToken(fcmToken)
        //        let fcmToken : [String: String] = ["Token": fcmToken]
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        //        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil, userInfo: fcmToken)
        
    }
    // [END refresh_token]
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

