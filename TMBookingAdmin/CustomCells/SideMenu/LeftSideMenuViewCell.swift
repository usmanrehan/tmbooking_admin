//
//  LeftSideMenuViewCell.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 04/08/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class LeftSideMenuViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgDropDown: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
