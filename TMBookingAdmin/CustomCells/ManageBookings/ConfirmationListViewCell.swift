//
//  ConfirmationListViewCell.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 17/06/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit

class ConfirmationListViewCell: UITableViewCell {
    @IBOutlet weak var vendorLabel: UILabel!
    @IBOutlet weak var checkinDateLabel: UILabel!
    @IBOutlet weak var checkoutDateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func setData(_ data: PurchaseOrderModel) {
        self.vendorLabel.text = data.vendorName
        self.checkinDateLabel.text = data.checkinDate ?? "-"
        self.checkoutDateLabel.text = data.checkoutDate ?? "-"
    }

	public func setTransportData(_ data: PurchaseOrderModel) {
		self.vendorLabel.text = data.vendorName
		self.checkinDateLabel.text = data.pickupDate ?? "-"
	}
}
