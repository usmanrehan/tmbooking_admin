//
//  OrderHistoryCell.swift
//  TMBookingTemp
//
//  Created by Ahmed Shahid on 4/16/18.
//  Copyright © 2018 Ahmed Shahid. All rights reserved.
//

import UIKit
import SwiftDate

class OrderHistoryCell: UITableViewCell {

    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    internal func updateUI(data: Order, status: String) {
        self.lblStatus.text = status
        self.lblOrderNumber.text = String(format: "Order #%d", data.id ?? 0)
        self.lblTotal.text = String(format: "Total: SAR %@", data.total ?? 0.0)
        if let createdDate = data.createdAt {
            let date = DateInRegion(createdDate, format: DateUtil.FORMAT_WOOCOMMERCE)
            self.lblOrderDate.text = date?.toString(.custom("MMM dd, yyyy"))
        }        
    }
}
