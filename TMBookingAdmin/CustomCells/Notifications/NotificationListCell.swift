//
//  NotificationListCell.swift
//  Template
//
//  Created by Akber Sayni on 07/11/2018.
//  Copyright © 2018 Akber Sayani. All rights reserved.
//

import UIKit

class NotificationListCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblGuestName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
