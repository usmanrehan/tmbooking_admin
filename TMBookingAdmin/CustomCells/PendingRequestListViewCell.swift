//
//  PendingRequestListViewCell.swift
//  TMBookingAdmin
//
//  Created by Akber Sayni on 18/06/2019.
//  Copyright © 2019 Akber Sayani. All rights reserved.
//

import UIKit

class PendingRequestListViewCell: UITableViewCell {
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
